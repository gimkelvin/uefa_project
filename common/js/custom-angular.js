var app = angular.module('uefaApp', []);

app.controller('signUpController', function($scope,$http) {
    $scope.isLoading = undefined;
    $scope.areaCodes = areaCodes;
    $scope.signUp = function() {
        $scope.isLoading = true;

            var url="/prc/sign_up.php";
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.formData),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data,status) {
            $scope.isLoading = false;
            alert(data.message);
            if(data.result == 1) {
                document.location="/";
            }
        });
    };
});


app.controller('DepositCtrl', ['$scope', function($scope) {
    $scope.submitted = false;
    $scope.submit = function() {
        if ($scope.user_form.$valid) {
            //Submit as normal
        } else {
            //don't submit
        }
    };
}]);

app.controller('WithdrawCtrl', ['$scope', function($scope) {
    $scope.submitted = false;
    $scope.submit = function() {
        if ($scope.user_form.$valid) {
            //Submit as normal
        } else {
            //don't submit
        }
    };
}]);


app.directive('tqValidateAfter', [function() {
    var validate_class = "tq-validate";
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {
            ctrl.validate = false;

            element.bind('focus', function(evt) {
                if(ctrl.validate && ctrl.$invalid) // if we focus and the field was invalid, keep the validation
                {
                    element.addClass(validate_class);
                    scope.$apply(function() {ctrl.validate = true;});
                }
                else
                {
                    element.removeClass(validate_class);
                    scope.$apply(function() {ctrl.validate = false;});
                }

            }).bind('blur', function(evt) {
                element.addClass(validate_class);
                scope.$apply(function() {ctrl.validate = true;});
            });
        }
    };
}]);

//Signup
app.directive('validPasswordC', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            var original;
            ctrl.$formatters.unshift(function (modelValue) {
                original = modelValue;
                return modelValue;
            });

            ctrl.$parsers.push(function(viewValue){
                var noMatch = viewValue != scope.signUpForm.MemberPwd.$viewValue;
                console.log(noMatch);
                ctrl.$setValidity('noMatch', !noMatch);

                return viewValue;
            });
        }
    }
});

//Tabs
//Tab - Notice
app.controller("TabController", function() {
    this.tab = 1;

    this.isSet = function(checkTab,slot) {

        return this.tab === checkTab;
    };

    this.setTab = function(setTab) {
        this.tab = setTab;

    };
});

app.controller("DepositTabController", function() {
    this.tab = 2;

    this.isSet = function(checkTab) {
        return this.tab === checkTab;
    };

    this.setTab = function(setTab) {
        this.tab = setTab;
    };
});

app.controller('noticeController', function($scope,$http) {
    $scope.noticeDatas = {};
    $scope.eventDatas = {};

    $scope.getNotice = function() {
        var responsePromise = $http.get("/prc/get_board_detail.php?type=1&page=1");
        responsePromise.success(function(data, status, headers, config) {
            $scope.noticeDatas = data.list;

            for (index in $scope.noticeDatas){
                if($scope.noticeDatas[index].PopUp == "y" && $scope.noticeDatas[index].Type == 1){
                    var url= '/prc/get_board_description.php?type=1&code='+$scope.noticeDatas[index].BoardCode;

                    var total = $('.notice-content').length;
                    $('.notice-content').each(function(index) {
                        if (index === total - 1) {
                            //$('.notice-subject').html($scope.noticeDatas[index].Subject); //Get the last popup
                            $('.notice-content').html($scope.noticeDatas[index].Subject); //Get the last popup
                        }
                    });

                    var responsePromise = $http.get(url);
                    responsePromise.success(function(data, status, headers, config){
                       //alert(createCookie('popCookie'));
                        if(!getCookie('popCookie')) {

                            $('.notice-container').show();
                            $('.notice-tabs').show();
                        }

                    });

                }

            }

        });

        responsePromise.error(function(data, status, headers, config) {/*alert("AJAX failed!");*/});
    };

    $scope.getEvent = function() {
        var responsePromise = $http.get("/prc/get_board_detail.php?type=3&page=1");
        responsePromise.success(function(data, status, headers, config) {
            $scope.eventDatas = data.list;
        });
        responsePromise.error(function(data, status, headers, config) {/*alert("AJAX failed!");*/});
    };

    $scope.getFaq = function() {
        var responsePromise = $http.get("/prc/get_board_detail.php?type=2&page=1");
        responsePromise.success(function(data, status, headers, config) {
            $scope.faqDatas = data.list;
        });
        responsePromise.error(function(data, status, headers, config) {/*alert("AJAX failed!");*/});
    };


    $scope.getCustomer = function() {
        var responsePromise = $http.get("/prc/get_board_detail.php?type=4&page=1");
        responsePromise.success(function(data, status, headers, config) {
            $scope.customerDatas = data.list;
        });
        responsePromise.error(function(data, status, headers, config) {/*alert("AJAX failed!");*/});
    };
});

app.controller('transactionController', function($scope,$http) {
    $scope.realTimeTransactions = {};
    $scope.realTimeWithdrawal = {};

    $scope.getTransaction = function() {
        var responsePromise = $http.get("/prc/display_transaction.php?count=9");
        responsePromise.success(function(data, status, headers, config) {
            $scope.realTimeTransactions = data;
        });
        responsePromise.error(function(data, status, headers, config) {/*alert("AJAX failed!");*/});
    };
});


app.controller('withdrawController', function() {
    this.products = withdrawData;
});

var withdrawData = [{
    withdrawDate: '2013.12.31 18:00',
    withdrawBalance: 360000,
    withdrawName: 'Deposit****'
}, {
    withdrawDate: '2013.12.31 18:00',
    withdrawBalance: 360000,
    withdrawName: 'Sample****'
}, {
    withdrawDate: '2013.12.31 18:00',
    withdrawBalance: 360000,
    withdrawName: 'Sample****'
}, {
    withdrawDate: '2013.12.31 18:00',
    withdrawBalance: 360000,
    withdrawName: 'Sample****'
}, {
    withdrawDate: '2013.12.31 18:00',
    withdrawBalance: 360000,
    withdrawName: 'Sample****'
}, {
    withdrawDate: '2013.12.31 18:00',
    withdrawBalance: 360000,
    withdrawName: 'Sample****'
}, {
    withdrawDate: '2013.12.31 18:00',
    withdrawBalance: 360000,
    withdrawName: 'Sample****'
}, {
    withdrawDate: '2013.12.31 18:00',
    withdrawBalance: 360000,
    withdrawName: 'Sample****'
}];

app.controller('depositController', function() {
    this.products = depositData;
});

var depositData = [{
    depositDate: '2013.12.31 18:00',
    depositBalance: 360000,
    depositName: 'Withdraw****'
}, {
    depositDate: '2013.12.31 18:00',
    depositBalance: 360000,
    depositName: 'Sample****'
}, {
    depositDate: '2013.12.31 18:00',
    depositBalance: 360000,
    depositName: 'Sample****'
}, {
    depositDate: '2013.12.31 18:00',
    depositBalance: 360000,
    depositName: 'Sample****'
}, {
    depositDate: '2013.12.31 18:00',
    depositBalance: 360000,
    depositName: 'Sample****'
}, {
    depositDate: '2013.12.31 18:00',
    depositBalance: 360000,
    depositName: 'Sample****'
}, {
    depositDate: '2013.12.31 18:00',
    depositBalance: 360000,
    depositName: 'Sample****'
}, {
    depositDate: '2013.12.31 18:00',
    depositBalance: 360000,
    depositName: 'Sample****'
}];

app.controller('walletController', function($scope) {
    $scope.games = games;
});

var games = {1005:"Microgaming",1012:"Asia Gaming",1000:"GAMEPLAY",1002:"Gold Deluxe",1014:"Ezugi",1004: "Betsoft",1015:"WFT Sports",1016:"ASC Sports"};

var reviews = [{
    body: "Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.",
    createdOn: 1397490980837
}];

app.controller('messageController', function () {
    this.message = {};
    this.reviews = reviews;

    this.addChatMsg = function (reviews) {
        this.message.createdOn = Date.now();
        this.reviews.push(this.message);
        this.message = {};
    };
});