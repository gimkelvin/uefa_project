

function addComma(Name) {
    var src;
    var i;
    var factor;
    var su;
    var SpaceSize = 0;
    var chkValue;

    chkValue = "";
    su = Name.value.length;
    for (i = 0; i < su; i++) {
        src = Name.value.substring(i, i + 1);
        if (src != ",") {
            factor = parseInt(src);
            if (isNaN(factor)) // < 0 || src > 9)^M
            {
                alert("Please enter numeric values only.");
                Name.focus();
                //return false;
            } else {
                chkValue += src;
            }
        }
    }
    Name.value = chkValue;

    factor = Name.value.length % 3;
    su = (Name.value.length - factor) / 3;
    src = Name.value.substring(0, factor);

    for (i = 0; i < su; i++) {
        if ((factor == 0) && (i == 0)) // "XXX" 인경우
        {
            src += Name.value.substring(factor + (3 * i), factor + 3 + (3 * i));
        } else {
            src += ",";
            src += Name.value.substring(factor + (3 * i), factor + 3 + (3 * i));
        }
    }
    Name.value = src;

    return true;
}

function comma_add_return(n) {
    var reg = /(^[+-]?\d+)(\d{3})/;
    n += '';
    while (reg.test(n))
        n = n.replace(reg, '$1' + ',' + '$2');
    return n;
}

function toInt(str) {
    str = str.replace(/\,/g, '');

    if (str == "") {
        str = 0
    } else {
        str = parseInt(str);
    }
    return str;
}


function addAmount(amount,self) {
    var amountInput = $(self).parent().parent().find('.amount');

    if (amount == '') {
        $(amountInput).val(amount);
    } else {
        $(amountInput).val(comma_add_return(toInt($(amountInput).val()) + toInt(amount)));
    }
}

function resetForm(){
    $('select').find('option:first').attr('selected', 'selected');
    $('textarea').val('');
    $(".game-balance").text("Please select game.");
    $("input").not('.btn-form,:input[type=submit],:input[type=button]').val('');
}


function checkid(obj) {
    var pattern = /(^[ㄱ-ㅎ가-힣]*$)/;
    if(pattern.test($(obj).val()) && $(obj).val().length >= 1){
        alert('Please enter alphabets only.');
        $(obj).val('');
        return 0;
    }

    if ($(obj).val().length > 3) {

        if(window.location.href.indexOf("english") >= 30){
            var url = "/prc/check_client.php?lang=english";
        }else{
            var url = "/prc/check_client.php";
        }

        $.post(url,$('#sign-up-form').serialize(),function(data){
            var result = data.result;
            var message = data.message;
            var msg_id = data.message_id;
            if (result == "1") {
                $("#msg_id").css("color", "green");
            }else if(msg_id=="alert"){
                alert(data.message);
            }else{
                $("#msg_id").css("color", "red");
            }
            $("#msg_id").html(message);
        },"json");

    }

}

function forgot_password(){
    var url = "/prc/forgot_password.php";
    $.post(url,$('#forgot-pass-form').serialize(),function(data){
        var result = data.result;
        var message = data.message;
        if(result == "1") {
            alert(message);
            resetForm();
            $(".b-close").click();
        }else{
            alert(message);
        }
    },"json");
}

var process_signUp=false;
function signUp(){

    if(!process_signUp){
        process_signUp=true;

        if(window.location.href.indexOf("english") >= 30) {
            var url = "/prc/sign_up.php?lang=english";
        }else{
            var url = "/prc/sign_up.php";
        }

        $.post(url,$('#sign-up-form').serialize(),function(data){
            var message = data.message;
            var message_id = data.message_id;
            if(data.result == 1){
                alert(data.message);

                if(window.location.href.indexOf("english") >= 30) {
                    document.location="/index.php?lang=english";
                }else{
                    document.location="/";
                }

            }else{
                alert(data.message);
/*                $(".msg").text('')
                $("#" + message_id).css({color:"red"}).html(message);*/
            }

        },"json").done(function(){
            process_signUp=false;
        });
    }else{
        if(window.location.href.indexOf("english") >= 30) {
             alert("Process pending");
        }else{
            alert("ที่รอดำเนินการขั้นตอน");
        }
    }
}


$('.id,.pw').keypress(function (e) {
    if (e.which == 13) {
        $(this).blur();
        login();
    }
});

var try_login = false;


function login() {
    var beforeText = $("#hiddenLogin").val();

    if(beforeText=="LOGIN") {
        if (!try_login) {

            if(window.location.href.indexOf("english") >= 30){
                $(".btn-login").text("Processing..");
                try_login = true;
                var url = "/prc/login.php?lang=english";
            }else{
                $(".btn-login").text("การประมวลผล..");
                try_login = true;
                var url = "/prc/login.php";
            }


            if ($('#popup-login').is(':visible')) {
                login_form = "#popup-login-form";
            }else{
                login_form = "#login-form";
            }

            $.post(url, $(login_form).serialize(), function (data) {

                if (data.result == 1) {
                    try_login = false;
                    alert(data.message);

                    if(window.location.href.indexOf("english") >= 30){
                        document.location = "/index.php?lang=english";
                    }else{
                        document.location = "/";
                    }

                } else {
                    try_login = false;
                    alert(data.message);

                }
            }, "json").done(function(){

                if(window.location.href.indexOf("english") >= 30){
                    $(".btn-login").text("Login");
                }else{
                    $(".btn-login").text("เข้าสู่ระบบ");
                }

            });
        }
    }else{
        alert ("Process pending");
    }
}


function logout(){
    var url="/prc/logout.php";
    $.post(url,function(data){
        if(data.result == 1){
            location.href="/";
        }else{
            alert(data.message);
        }
    },"json");
}

function getBalance(game,self) {
    var gameBalance = $(self).parent().find('.game-balance');

    if(window.location.href.indexOf("english") >= 30){
        $(gameBalance).html("Loading balance..");
    }else{
        $(gameBalance).html("สมดุลการโหลด..");
    }

    var url = "/prc/check_member_balance.php?Wallet="+game;
    $.getJSON(url,function(data){
        if(data.result == 1){

            if(window.location.href.indexOf("english") >= 30){
                $(gameBalance).html("Available balance for transfer ฿ <strong>" + comma_add_return(data.balance) + ".</strong>");
            }else{
                $(gameBalance).html("ยอดเงินคงเหลือสำหรับการถ่ายโอน ฿ <strong>" + comma_add_return(data.balance) + ".</strong>");
            }

        }else{
            if(window.location.href.indexOf("english") >= 30){
                $(gameBalance).html("Failed to retrieve available balance.");
            }else{
                $(gameBalance).html("ล้มเหลวในการดึงยอดเงินคงเหลือ.");
            }

        }
    });
}

function indexNotice(type,self){
    var code = $(self).parent().find('input').val();
    popupOpenCustomer();
    if(type==1){
        $("#customerNotice").click();
        read_board('1',code,'');
    }else if(type==3){
        $("#customerEvent").click();
        read_board('3',code,'');
    }

    //$("#noticeBoardCode").attr("onclick","read_board('1','"+code+"');").click();
}

function getAllGameBalance(){
    $(".opus-balance").text("Loading..");
    $(".gd-balance").text("Loading..");
    $(".ag-balance").text("Loading..");
    $(".mg-balance").text("Loading..");
    $(".bet-balance").text("Loading..");
    $(".moon-balance").text("Loading..");
    $(".ho-balance").text("Loading..");
    $(".ezugi-balance").text("Loading..");
    $(".asc-balance").text("Loading..");
    $(".wft-balance").text("Loading..");
    $(".xtd-balance").text("Loading..");
    $(".all-balance").text("Loading..");

    $.getJSON('/prc/check_member_balance_parall.php', {
        returnformat: 'json'
    }, function(data) {

        $(".mg-balance").text(data.list[1005].Balance);
        $(".opus-balance").text(data.list[1000].Balance);
        $(".gd-balance").text(data.list[1002].Balance);
        $(".ag-balance").text(data.list[1012].Balance);
        $(".bet-balance").text(data.list[1004].Balance);
        $(".ezugi-balance").text(data.list[1014].Balance);
        $(".asc-balance").text(data.list[1016].Balance);
        $(".wft-balance").text(data.list[1015].Balance);
//        $(".xtd-balance").text(data.list[1009].Balance);
//        $(".moon-balance").text(data.list[1003].Balance);
//        $(".ho-balance").text(data.list[1013].Balance);
        $(".all-balance").text(data.list['All'].Balance);

    });
}

function check_session(){
    $.getJSON('/prc/get_member_info.php', {
        returnformat: 'json'
    },function(data){
        if(data.result != 1){
            if(data.result == 207){
                var url="/prc/logout.php";
                $.post(url,function(data){
                    if(data.result == 1){
                        alert("Your account has been logged out.");
                        window.location.href="/";
                    }else{
                        alert(data.message);
                    }
                },"json");
            }else{
                alert(data.message);
                window.location.href="/";
            }
        }
    });
}

function load_comp(){
    $.getJSON('/prc/get_member_info.php', {
        returnformat: 'json'
    },function(data){
        //console.log(data);
        var ccomp = data.bonus.CurrentComp;
        var ucomp = data.bonus.UsedComp;
        var tcomp = data.bonus.TotalComp;

        $(".current-comp").text(ccomp);
        $(".used-comp").text(ucomp);
        $(".total-comp").text(tcomp);
    });
}



function load_comp_history(type,page){
    $.getJSON('/prc/comp_history.php?type='+type+'&page='+page, {
        returnformat: 'json'
    },function(data){
        //console.log(data);
        var table_data="";
        var page_data="";
        if(data.list.length){
            for(var i = 0 ;i<data.list.length;i++){
                if(i%2 == 0){
                    table_data += "<tr class='row-box-table'>";
                }else{
                    table_data += "<tr class='row-box-table highlight'>";
                }
                table_data += "<td class=\"row-col width10 text-center\">"+data.list[i].num + "</td>";
                table_data += "<td class=\"row-col width15 text-center\">"+data.list[i].BetDate.substr(0,10) + "</td>";
                table_data += "<td class=\"row-col width20 text-center\">"+data.list[i].GameCode +"</td>";
                table_data += "<td class=\"row-col width30 text-center\">"+data.list[i].GameName +"</td>";
                table_data += "<td class=\"row-col width10 text-center\">"+data.list[i].BetCount +"</td>";
                table_data += "<td class=\"row-col width15 text-center\">"+data.list[i].Comp +"</td>";
                table_data += "</tr>";
            }
        }else{
            table_data += "<tr><td colspan='7' class=\"text-center\" style='height:100px;padding-top:100px;'>Non existing data.</td></tr>";
        }

        if(data.pages){
            for(var i=1;i<=data.pages;i++){
                if(i==data.page){
                    page_data += "<li class='active'>"+i+"</li>";
                }else {
                    page_data += "<li class=\"pointer link\"><a onclick=\"load_comp_history('"+type+"',"+i+")\">"+i+"</a></li>";
                }
            }
        }
        if(type=="mComp"){
            $(".mcomp-list").html(table_data);
            $(".pagination-mcomp").html(page_data);
        }else if(type=="fComp"){
            $(".fcomp-list").html(table_data);
            $(".pagination-fcomp").html(page_data);
        }
    });
}


function load_friend_list(page){
    $.getJSON('/prc/get_friend_list.php?page='+page, {
        returnformat: 'json'
    },function(data){
//        console.log(data);
        var table_data="";
        var page_data="";
        if(data.list.length){
            //console.log(data);
            for(var i = 0 ;i<data.list.length;i++){
                if(i%2 == 0){
                    table_data += "<tr class='row-box-table'>";
                }else{
                    table_data += "<tr class='row-box-table highlight'>";
                }
                table_data += "<td class=\"row-col width33 text-center\">"+data.list[i].num+ "</td>";
                table_data += "<td class=\"row-col width33 text-center\">"+data.list[i].RefererID+"</td>";
                table_data += "<td class=\"row-col width33 text-center\">"+data.list[i].RegisterDate+"</td>";
                table_data += "</tr>";
            }
        }else{
            table_data += "<tr><td colspan='7' class=\"text-center\" style='height:100px;padding-top:160px;'>Non existing data.</td></tr>";
        }

        if(data.pages){
            for(var i=1;i<=data.pages;i++){
                if(i==data.page){
                    page_data += "<li class='active'>"+i+"</li>";
                }else {
                    page_data += "<li class=\"pointer link\"><a onclick=\"load_friend_list("+i+")\">"+i+"</a></li>";
                }
            }
        }

        $(".friend-list").html(table_data);
        $(".pagination-friend").html(page_data);
    });
}


function flash()
{
    var color = '255,0,0';
    var duration = 1000;
    var coupon = '.coupon-count';
    var current = $(coupon).css( 'color' );
    $(coupon).animate( { color:  'rgb(' + color + ')' }, duration / 2 );
    $(coupon).animate( { color: current }, duration / 2 );
}

function change_password(form,self){
    var url="/prc/change_password.php";
    $.post(url,$(form).serialize(),function(data){
        var result = data.result;
        var message = data.message;

        if (result == "1") {
            alert("Password has been updated successfully.");
        } else {
            alert(message);
        }
    },"json");
}

function count_copuon(){
    $.getJSON('/prc/member_coupon.php', {
        returnformat: 'json'
    },function(data){
//        console.log(data);
        var coupon = '.coupon-count';
        var total = data.total;
//        console.log(data.total);
        if(total>0){
            $(coupon).css({color:'#ffd800'});
            setInterval(flash,1500);
        }
        $(coupon).text(total);
    });
}

function use_coupon(self,code){
    var game=$(self).parent().find('.GameCode').val();

    var url="/prc/use_coupon.php";
    var beforeValueName;
    var message="";

    if($(self).val() == "Processing"){
        alert("Process pending");
        return 0;
    }

    beforeValueName = $(self).val();
    $(self).val("Processing");

    $.post(url,{ CouponCode: code, GameCode: game },function(data){
        message = data.message;
        if(data.result == 1){
            getAllGameBalance();
            alert(message);
            load_coupon(1);
        }else{
            alert(message);
        }
    },"json").done(function(){
        $(self).val(beforeValueName);
    });

}


function load_history(page){


    $.getJSON('/prc/member_transaction_list.php?page='+page, {
        returnformat: 'json'
    }, function(data) {
        var table_data="";
        var page_data="";
        if(data.list.length){
            for(var i = 0 ;i<data.list.length;i++){
                if(i%2 == 0){
                    table_data += "<tr class='row-box-table'>";
                }else{
                    table_data += "<tr class='row-box-table highlight'>";
                }
                table_data += "<td class=\"row-col width08 text-center\">"+data.list[i].TransactionType + "</td>";
                table_data += "<td class=\"row-col width26 text-center\">"+data.list[i].Wallet +"</td>";
                table_data += "<td class=\"row-col width12 text-center\">"+data.list[i].Amount +"</td>";
                table_data += "<td class=\"row-col width10 text-center\">"+data.list[i].ServiceDescription +"</td>";
                table_data += "<td class=\"row-col width14 text-center\">"+data.list[i].ServiceAmount +"</td>";
                table_data += "<td class=\"row-col width10 text-center\">"+data.list[i].Status +"</td>";
                table_data += "<td class=\"row-col width20 text-center\">"+data.list[i].Date +"</td>";
                table_data += "</tr>";
            }
        }else{
            table_data += "<tr><td colspan='7' class=\"text-center\" style='height:20px;padding-top:20px;'>Non existing data.</td></tr>";
        }

        if(data.pages){
            for(var i=1;i<=data.pages;i++){
                if(i==data.page){
                    page_data += "<li class='active'>"+i+"</li>";
                }else {
                    page_data += "<li class=\"pointer link\"><a onclick=\"load_history("+i+")\">"+i+"</a></li>";
                }
            }
        }
        $(".history-list").html(table_data);
        $(".pagination-history").html(page_data);

    });
}


function load_board(page,type,tab){
    if(type == 4 || type ==5) {
        var isMember = check_member("customer");
        if(!isMember) {
            return 0;
        }
    }
    $(".board-list").html("");
    $(".pagination-board").html("");
    $('.message-box').hide().html("");
    $(".board-read").hide(); 
    $(".read-content").html("");

    $.getJSON('/prc/get_board_detail.php?page='+page+'&type='+type, {
        returnformat: 'json'
    }, function(data) {
        var table_data="";
        var page_data="";
        var class_data="";
        if(data.list.length){
            for(var i = 0 ;i<data.list.length;i++){
                if(i%2 == 0){
                    table_data += "<tr>";
                }else{
                    table_data += "<tr class='highlight'>";
                }
                table_data += "<td class=\"row-col width10 text-center\">"+data.list[i].num + "</td>";
                if((data.list[i].Type == 4 || data.list[i].Type == 5) && data.list[i].CommentCount > 0){
                    table_data += "<td class=\"row-col width69 pop-notice-title text-left\" width=\"70%\" onclick=\"read_board('"+data.list[i].Type+"','"+data.list[i].BoardCode+"','"+data.list[i].CommentCount+"');\">"+data.list[i].Subject+" <span class=\"comment\"> ["+data.list[i].CommentCount+"]</span></td>";
                }else{
                    table_data += "<td class=\"row-col width69 pop-notice-title text-left\" width=\"70%\" onclick=\"read_board('"+data.list[i].Type+"','"+data.list[i].BoardCode+"','');\">"+data.list[i].Subject + "</td>";
                }

                table_data += "<td class=\"row-col width10 text-center\">"+data.list[i].WriteDate.substring(0,10) +"</td>";
                table_data += "<td class=\"row-col width10 text-center\">"+data.list[i].ViewCount +"</td>";
                table_data += "</tr>";
            }
        }else{
            table_data += "<tr><td colspan='7' class=\"text-center\" style='height:20px;padding-top:20px;'>Non existing data.</td></tr>";
        }

        if(data.pages){
            for(var i=1;i<=data.pages;i++){
                if(i==data.page){
                    page_data += "<li class=\"active\">"+i+"</li>";
                }else {
                    page_data += "<li class=\"pointer link\" onclick=\"load_board("+i+","+type+",'"+tab+"')\">"+i+"</li>";
                }
            }
        }

        $(".board-list").html(table_data);
        $(".pagination-board").html(page_data);
    });
}


function write_comment(form,type,code,comment,self){
    var beforeValueName;
    if($(self).val() == "Processing"){
        alert("Process pending");
        return 0;
    }

    beforeValueName = $(self).val();
    $(self).val("Processing");

    var url="/prc/write_comment.php?type="+type+"&code="+code;
    $.post(url,$(form).serialize(),function(data) {
        var message = data.message;
        if (data.result == 1) {
            alert(message);
            read_board(type,code,comment);
            load_board('1','4','#1on1support');
            $(form+" textarea").val("");
        }else{
            alert(message);
        }
    },"json").done(function(){
        $(self).val(beforeValueName);
    });
}


function write_board(form,type,self) {
    if(type === 4)
    {
        $(".board-write").show();
        $(".read-subject").html("");
        $(".read-date").html("");
        $(".read-content").html("");
        $('.message-box').html("");
    }

    var beforeValueName;
    if($(self).val() == "Processing"){
        alert('처리중입니다.');
        return 0;
    }

    beforeValueName = $(self).val();
    $(self).val("Processing");

    var url="/prc/write_board.php?type="+type+"&ServerType=C";
    $.post(url,$(form).serialize(),function(data) {
//        console.log(data.message);
        var message = data.message;
        if (data.result == 1) {
            alert("Inquiry has been noted. Customer support will reply shortly.");
            resetForm();
            if(type==4){
                $(".board-write").hide();
            }
            $("#customer1on1").trigger('click');
        } else {
            alert(message);
        }
    },"json").done(function(){
        $(self).val(beforeValueName);
    });
}



function read_board(type,code,comment){
    $(".board-read").show();
    $(".board-write").hide();
    $(".read-subject").html("");
    $(".read-date").html("");
    $(".read-content").html("");
    $('.message-box').html("");


    $.getJSON('/prc/get_board_description.php?type='+type+'&code='+code,{
        returnformat: 'json'
    }, function(data) {
        if(comment >= 1){
            var comment_data="";
            $.getJSON('/prc/get_board_comment.php?type='+type+'&code='+code,{
                returnformat: 'json'
            },function(data){
                for(var i = 0 ;i<data.content.length;i++){
                    if(data.content[i]. WriteLevel == "C"){
                        comment_data += "<div class=\"user-message\">" +
                        "<div class=\"message-name text-left\"><strong>"+data.content[i].MemberID+"</strong></div>" +
                        "<div class=\"message-time text-right\"><em>"+data.content[i].WriteDate.substring(5,16)+"</em></div>"+
                        "<div class=\"clear\"></div>" +
                        "<div class=\"user-message-box\"><p>"+data.content[i].Comment+"</p></div>"+
                        "</div><div class=\"clear\"></div>";
                    }else{
                        comment_data += "<div class=\"admin-message\">" +
                        "<div class=\"message-name text-left\"><strong>"+data.content[i].MemberID+"</strong></div>" +
                        "<div class=\"message-time text-right\"><em>"+data.content[i].WriteDate.substring(5,16)+"</em></div>"+
                        "<div class=\"clear\"></div>" +
                        "<div class=\"admin-message-box\"><p>"+data.content[i].Comment+"</p></div>"+
                        "</div><div class=\"clear\"></div>";
                    }
                }
                $(".message-box").show().html(comment_data);
            });
        }else{
            $(".message-box").hide();
        }
        $(".btn-send").html("<input type=\"button\" value=\"Write\" class=\"btn-application\" onclick=\"write_comment('#write-comment-form','"+type+"','"+code+"','"+comment+"',this)\">");
        $(".read-subject").html(data.content.Subject);
        $(".read-date").html(data.content.WriteDate.substring(0,10));
        $(".read-content").html(data.content.Contents).find('img').css({width:"100%"});
    });
}


function loadSlot(game){
    $(".tab-content-1").html("");
    var slot_list ="";
    var i=0;
    slot_list+="<div id=\"casinoItems\">";
    for(i = 0 ;i<slot[game].length;i++) {
        if(game==1000){
            slot_list+=	"<a class=\"casinoItemBox\" onclick=\"playGame('"+game+"','slot','"+slot[game][i].GameCode+"','Mobile')\">"+
            "<img src='"+slot[game][i].image+"'  alt='"+slot[game][i].GameName+"'>";
        }else if(game==1005){
            slot_list+=	"<a class=\"casinoItemBox\" onclick=\"playGame('"+game+"','slot','"+slot[game][i].GameCode+"','Mobile')\">"+
            "<img src='"+slot[game][i].image+"' style='width:200%' alt='"+slot[game][i].GameName+"'>";
        }else if(game==1004){
            slot_list+=	"<a class=\"casinoItemBox\" style='height: 150px;' onclick=\"playGame('"+game+"','slot','"+slot[game][i].GameCode+"','2d')\">"+
            "<img src='"+slot[game][i].image+"' alt='"+slot[game][i].GameName+"'>";
        }
        slot_list+="<span class=\"casinoTitle\">"+slot[game][i].GameName+"</span>"+
        "</a>";
    }
    slot_list+="<div class='clear'></div>";
    slot_list+="</div>";

    $(".tab-content-1").html(slot_list);

}



function moneyProcess(form,self){

    var url="";
    var beforeValueName;
    var message="";
    //alert($(self).val());
    if($(self).val() == "Processing"){
        alert("Process pending");
        return 0;
    }

    beforeValueName = $(self).val();
    $(self).val("Processing");


    if(form == "#transfer-form" || form == "#transfer-form-sidebar"){
        url="/prc/transfer.php";
    }else if(form == "#deposit-form" || form == "#deposit-form-help2pay"){
        url="/prc/deposit.php";
    }else if(form == "#withdrawal-form"){
        url="/prc/withdrawal.php";
    }else if(form == "#comp-form"){
        url="/prc/use_comp.php";
    }else if(form == "#coupon-form"){
        url="/prc/use_coupon.php";
    }else if(form == "#settings-form"){
    url="/prc/change_password.php";
}

    var money_result=0;
    $.post(url,$(form).serialize(),function(data){
        message = data.message;
        money_result = data.result;
        if(data.result == 1){
            resetForm();
            alert(message);
        }else{
            alert(message);
        }
    },"json").done(function(){
        $(self).val(beforeValueName);
        if(money_result){
            getAllGameBalance();
            load_comp();
        }
    });
}