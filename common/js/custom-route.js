$(".main-container").show();
$(".sports-container").hide();
$(".slots-container").hide();
$(".signup-container").hide();

$('.logo').click(function(){openMain()});
$('.btn-signup, .link-signup').click(function(){openSignup()});
$('.gamebutton-slots').click(function(){openSlot()});

$(".container").removeClass('container-sports');

$('.gamebutton-slots1, .jackpot').click(function(){
    $('.slot-microgaming').fadeIn("slow");
    $('.slot-gameplay').hide();
    $('.slot-playtech').hide();
    $('.slot-betsoft').hide();
});
$('.gamebutton-slots2').click(function(){
    $('.slot-microgaming').hide();
    $('.slot-gameplay').fadeIn("slow");
    $('.slot-playtech').hide();
    $('.slot-betsoft').hide();
});
$('.gamebutton-slots3').click(function(){
    $('.slot-microgaming').hide();
    $('.slot-gameplay').hide();
    $('.slot-playtech').fadeIn("slow");
    $('.slot-betsoft').hide();
});
$('.gamebutton-slots4').click(function(){
    $('.slot-microgaming').hide();
    $('.slot-gameplay').hide();
    $('.slot-playtech').hide();
    $('.slot-betsoft').fadeIn("slow");
});

$('.gamebutton-sports1').click(function(){
    $("body").addClass("bg-sports1");
    $("body").removeClass("bg-sports2");
    $("body").removeClass("bg-sports3");
    $("body").removeClass("bg-sports4");
});
$('.gamebutton-sports2').click(function(){
    $("body").removeClass("bg-sports1");
    $("body").addClass("bg-sports2");
    $("body").removeClass("bg-sports3");
    $("body").removeClass("bg-sports4");
});
$('.gamebutton-sports3').click(function(){
    $("body").removeClass("bg-sports1");
    $("body").removeClass("bg-sports2");
    $("body").addClass("bg-sports3");
    $("body").removeClass("bg-sports4");
});
$('.gamebutton-sports4').click(function(){
    $("body").removeClass("bg-sports1");
    $("body").removeClass("bg-sports2");
    $("body").removeClass("bg-sports3");
    $("body").addClass("bg-sports4");
});

function openMain() {
    $(".masthead").fadeIn("fast");
    $("body").removeClass("bg-sports1");
    $("body").removeClass("bg-sports2");
    $("body").removeClass("bg-sports3");
    $("body").removeClass("bg-sports4");

    $(".container").removeClass('container-sports');

    $("#nav_mobile, #nav_sports, #nav_casino, #nav_slots, #nav_othergames").removeClass('active');
    $('.nav_hover_wrapper').hide();

    $(".content-container").addClass('main-wrapper');
    $(".content-container").removeClass('slots-wrapper');
    $(".content-container").removeClass('sports-wrapper');
    $(".content-container").removeClass('signup-wrapper');
    $(".main-container").fadeIn("slow");
    $(".sports-container").fadeOut("slow");
    $(".casino-container").fadeOut("slow");
    $(".slots-container").fadeOut("slow");
    $(".signup-container").fadeOut("slow");
}

function popupOpenRestricted(){
    if($("#popupRestricted").is(":visible")){

    }else{
        popupClose();
        $('#popupRestricted').slideDown(300);
        $('.popup-wrapper').fadeIn(300);
    }
}

//var alreadyOpen=false;
//var slot="";
function openSlotMicro() {
    $(".masthead").fadeOut("fast");
    $("body").removeClass("bg-sports1");
    $("body").removeClass("bg-sports2");
    $("body").removeClass("bg-sports3");
    $("body").removeClass("bg-sports4");
    $("body").removeClass("bg-casino");
    $("body").addClass("bg-slots");
    $("body").removeClass("bg-signup");

    $(".container").removeClass('container-sports');

    $("#nav_mobile, #nav_sports, #nav_casino, #nav_othergames").removeClass('active');
    $("#nav_slots").addClass('active');
    $('.nav_hover_wrapper').hide();

    $('.slot-microgaming').show();
    $('.slot-gameplay').hide();
    $('.slot-betsoft').hide();

    $(".content-container").removeClass('main-wrapper');
    $(".content-container").addClass('slots-wrapper');
    $(".content-container").removeClass('sports-wrapper');
    $(".content-container").removeClass('signup-wrapper');
    $(".main-container").hide();
    $(".sports-container").hide();
    $(".casino-container").hide();
    $(".slots-container").show();
    $(".signup-container").hide();
    $("#1005-slider-nav-ul").hide();
    $("#main-slider-nav-ul").show();

    $.get("/prc/slot.php", function(data) {
        slot = $.parseJSON(data);
    }).done(function(){
            $(".tab-slot1").trigger('click');
    })

}

function openSlotGamePlay() {
    //Slot Banner Animation
    $(".masthead").fadeOut("fast");
    $("body").removeClass("bg-sports1");
    $("body").removeClass("bg-sports2");
    $("body").removeClass("bg-sports3");
    $("body").removeClass("bg-sports4");
    $("body").removeClass("bg-casino");
    $("body").addClass("bg-slots");
    $("body").removeClass("bg-signup");

    $(".container").removeClass('container-sports');

    $("#nav_mobile, #nav_sports, #nav_casino, #nav_othergames").removeClass('active');
    $("#nav_slots").addClass('active');
    $('.nav_hover_wrapper').hide();

    $('.slot-microgaming').hide();
    $('.slot-gameplay').show();
    $('.slot-betsoft').hide();

    $(".content-container").removeClass('main-wrapper');
    $(".content-container").addClass('slots-wrapper');
    $(".content-container").removeClass('sports-wrapper');
    $(".content-container").removeClass('signup-wrapper');
    $(".main-container").hide();
    $(".sports-container").hide();
    $(".casino-container").hide();
    $(".slots-container").show();
    $(".signup-container").hide();
    $("#1000-slider-nav-ul").hide();
    $("#main-slider-nav-ul").show();

    $.get("/prc/slot.php", function(data) {
        slot = $.parseJSON(data);
    }).done(function(){
        $(".tab-slot2").trigger('click');
    })


}

function openSlotBet() {
    $(".masthead").fadeOut("fast");
    $("body").removeClass("bg-sports1");
    $("body").removeClass("bg-sports2");
    $("body").removeClass("bg-sports3");
    $("body").removeClass("bg-sports4");
    $("body").removeClass("bg-casino");
    $("body").addClass("bg-slots");
    $("body").removeClass("bg-signup");

    $(".container").removeClass('container-sports');

    $("#nav_mobile, #nav_sports, #nav_casino, #nav_othergames").removeClass('active');
    $("#nav_slots").addClass('active');
    $('.nav_hover_wrapper').hide();

    $('.slot-microgaming').hide();
    $('.slot-gameplay').hide();
    $('.slot-betsoft').show();

    $(".content-container").removeClass('main-wrapper');
    $(".content-container").addClass('slots-wrapper');
    $(".content-container").removeClass('sports-wrapper');
    $(".content-container").removeClass('signup-wrapper');
    $(".main-container").hide();
    $(".sports-container").hide();
    $(".casino-container").hide();
    $(".slots-container").show();
    $(".signup-container").hide();
    $("#main-slider-nav-ul").show();

    $.get("/prc/slot.php", function(data) {
        slot = $.parseJSON(data);
    }).done(function(){
            $(".tab-slot3").trigger('click');
    })

}

$(".tab-slot1").click(function(e){
    //console.log(e);
    $(".tab-slot").removeClass("active");
    $(this).addClass("active");
    $(".slot-tabs-content").hide();
    $(".slot1").show();
    //slotList('1005','TOP25','',true);
    loadSlot('1005','Advanced_Slot','',true);

});

$(".tab-slot2").click(function(){
    $(".tab-slot").removeClass("active");
    $(this).addClass("active");
    $(".slot-tabs-content").hide();
    $(".slot2").show();
    loadSlot('1000','Slots_3d','',true);
    //slotList('1000','TOP25','',true);
});

$(".tab-slot3").click(function(){
    $(".tab-slot").removeClass("active");
    $(this).addClass("active");
    $(".slot-tabs-content").hide();
    $(".slot3").show();
    loadSlot('1004','Slots','',true);
    //slotList('1004','TOP25','',true);
});

function openSignup() {
    $('#popup-login .btn-popup-close').click();

    $(".masthead").fadeOut("fast");
    $("body").removeClass("bg-sports1");
    $("body").removeClass("bg-sports2");
    $("body").removeClass("bg-sports3");
    $("body").removeClass("bg-sports4");
    $("body").removeClass("bg-casino");
    $("body").removeClass("bg-slots");
    $("body").addClass("bg-signup");

    $(".container").removeClass('container-sports');

    $("#nav_mobile, #nav_sports, #nav_casino, #nav_slots, #nav_othergames").removeClass('active');
    $('.nav_hover_wrapper').hide();

    $(".content-container").removeClass('main-wrapper');
    $(".content-container").removeClass('slots-wrapper');
    $(".content-container").removeClass('sports-wrapper');
    $(".content-container").addClass('signup-wrapper');
    $(".main-container").hide();
    $(".sports-container").hide();
    $(".casino-container").hide();
    $(".slots-container").hide();
    $(".signup-container").show();

    $('.banner-signup').show();
}

//Notice Tabs Delay
function showNotice() {$('.notice-container p').animate({opacity: 10},100);}
setTimeout(showNotice, 300);

function popupClose(){
    $('#popup-notice').slideUp(300);
    $('.b-modal.__b-popup1__').fadeOut(300);
}

function slotList(game,type,self,first){


    if(first){
        $(".tab-slots-sub ul li:first-child").addClass("active");
    }

/*    if($('#main-slider-wrapper').is(':visible')){
        $('#main-slider-wrapper').remove();
    }*/
    //$(".items-container").html("<div id=\"main-slider\" class=\"liquid-slider\"></div>");
    //$(".tab-slots-sub li").removeClass("active");
    $(self).addClass('active');
    $.getJSON('/prc/slot.php?game='+game+"&type="+type, {
        returnformat: 'json'
    }, function(data) {
//            console.log(data);
        var image,image_hover;
        var game_list="<div><h2 class=\"title\"></h2><div class=\"slot-list\">";
        for(var i = 0 ;i<data.length;i++) {
            if (i % 25 == 0 && i != 0) {
                game_list += "</div></div>";
                game_list += "<div><h2 class=\"title\"></h2><div class=\"slot-list\">";
            }

            if (game == 1013) {
                image = "style='background:url(http://fc75501cdbf4ec5fc292-8add6c8fd53476d9e2a6e3f3e2a0c602.r38.cf6.rackcdn.com/" + data[i].GameCode + ".png) top center no-repeat;'";
                image_hover = "style='background:url(http://fc75501cdbf4ec5fc292-8add6c8fd53476d9e2a6e3f3e2a0c602.r38.cf6.rackcdn.com/" + data[i].GameCode + ".png) top center no-repeat #0f0f0f;'";
            }else if( game == 1000){
                image = "style='background:url(http://8f2a6a1c077d250f6b95-0c3da9623b8f8a60af02d01462be7ec3.r55.cf6.rackcdn.com/" + data[i].GameCode + ".png) top center no-repeat;'";
                image_hover = "style='background:url(http://8f2a6a1c077d250f6b95-0c3da9623b8f8a60af02d01462be7ec3.r55.cf6.rackcdn.com/" + data[i].GameCode + ".png) top center no-repeat #0f0f0f;'";
            } else if (game == 1004) {
                image = "style='background:url(http://6daf46d71e0ee6c47cb1-d168421f8748e6c749e7bbee1aeac0bc.r40.cf6.rackcdn.com/game_" + data[i].GameCode + ".png) center center no-repeat;'";
                image_hover = "style='background:url(http://6daf46d71e0ee6c47cb1-d168421f8748e6c749e7bbee1aeac0bc.r40.cf6.rackcdn.com/game_" + data[i].GameCode + ".png) center center no-repeat #0f0f0f;'";
            }else if (type == "Mobile" && game == "1005") {
                image = "style='background:url(http://dc5309f6b5c0545a91a2-f3765441c4d51292a84de4d37db3c178.r24.cf6.rackcdn.com/mobile_" + data[i].GameCode + ".png) 0 0 no-repeat;'";
                image_hover = "style='background:url(http://dc5309f6b5c0545a91a2-f3765441c4d51292a84de4d37db3c178.r24.cf6.rackcdn.com/mobile_" + data[i].GameCode + ".png) 100% 0 no-repeat #0f0f0f;background-size: 200%;'";
            } else if (game == 1005){
                image = "style='background:url(http://dc5309f6b5c0545a91a2-f3765441c4d51292a84de4d37db3c178.r24.cf6.rackcdn.com/" + data[i].GameCode + ".png) 0 0 no-repeat;'";
                image_hover = "style='background:url(http://dc5309f6b5c0545a91a2-f3765441c4d51292a84de4d37db3c178.r24.cf6.rackcdn.com/" + data[i].GameCode + ".png) 100% 0 no-repeat #0f0f0f;background-size: 200%;'";
            }

            game_list += "<div class=\"slot-box\">";
            if(type=="TOP25" && game=="1013"){
                game_list +=
                    "<div class=\"slot-item-hover\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','"+data[i].AmayaType+"','"+data[i].AmayaName+"');\""+image_hover+"><div class=\"slot-new"+data[i].GameNew+"\"></div><div class=\"slot-top"+data[i].GameTop25+"\"></div><p>"+data[i].GameName+"</p></div>"+
                    "<div class=\"slot-item\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','"+data[i].AmayaType+"','"+data[i].AmayaName+"');\""+image+"><div class=\"slot-new"+data[i].GameNew+"\"></div><div class=\"slot-top"+data[i].GameTop25+"\"></div><p>"+data[i].GameName+"</p></div>";
            }else if(type=="TOP25"){
                game_list +=
                    "<div class=\"slot-item-hover\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','3d');\""+image_hover+"><div class=\"slot-new"+data[i].GameNew+"\"></div><div class=\"slot-top"+data[i].GameTop25+"\"></div><p>"+data[i].GameName+"</p></div>"+
                    "<div class=\"slot-item\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','3d');\""+image+"><div class=\"slot-new"+data[i].GameNew+"\"></div><div class=\"slot-top"+data[i].GameTop25+"\"></div><p>"+data[i].GameName+"</p></div>";
            }else if(type=="Slots_3d"){
                game_list +=
                    "<div class=\"slot-item-hover\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','3d');\""+image_hover+"><div class=\"slot-new"+data[i].GameNew+"\"></div><p>"+data[i].GameName+"</p></div>"+
                    "<div class=\"slot-item\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','3d');\""+image+"><div class=\"slot-new"+data[i].GameNew+"\"></div><p>"+data[i].GameName+"</p></div>";
            }else if(type=="Mobile" && game == "1005"){
                game_list +=
                    "<div class=\"slot-item-hover\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','Mobile');\""+image_hover+"><div class=\"slot-new"+data[i].GameNew+"\"></div><p>"+data[i].GameName+"</p></div>"+
                    "<div class=\"slot-item\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','Mobile');\""+image+"><div class=\"slot-new"+data[i].GameNew+"\"></div><p>"+data[i].GameName+"</p></div>";
            }else if(type=="Mobile" && game == "1004"){
                game_list +=
                    "<div class=\"slot-item-hover\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','2d');\""+image_hover+"><div class=\"slot-new"+data[i].GameNew+"\"></div><p>"+data[i].GameName+"</p></div>"+
                    "<div class=\"slot-item\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','2d');\""+image+"><div class=\"slot-new"+data[i].GameNew+"\"></div><p>"+data[i].GameName+"</p></div>";
            }else if(type=="Mobile" && game == "1000"){
                game_list +=
                    "<div class=\"slot-item-hover\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','Mobile');\""+image_hover+"><div class=\"slot-new"+data[i].GameNew+"\"></div><p>"+data[i].GameName+"</p></div>"+
                    "<div class=\"slot-item\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','Mobile');\""+image+"><div class=\"slot-new"+data[i].GameNew+"\"></div><p>"+data[i].GameName+"</p></div>";
            }else if(game=="1013"){
                game_list +=
                    "<div class=\"slot-item-hover\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','"+data[i].AmayaType+"','"+data[i].AmayaName+"');\""+image_hover+"><div class=\"slot-new"+data[i].GameNew+"\"></div><p>"+data[i].GameName+"</p></div>"+
                    "<div class=\"slot-item\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','"+data[i].AmayaType+"','"+data[i].AmayaName+"');\""+image+"><div class=\"slot-new"+data[i].GameNew+"\"></div><p>"+data[i].GameName+"</p></div>";
            }else{
                game_list +=
                    "<div class=\"slot-item-hover\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','2d');\""+image_hover+"><div class=\"slot-new"+data[i].GameNew+"\"></div><p>"+data[i].GameName+"</p></div>"+
                    "<div class=\"slot-item\" onclick=\"playGame('"+game+"','slot','"+data[i].GameCode+"','2d');\""+image+"><div class=\"slot-new"+data[i].GameNew+"\"></div><p>"+data[i].GameName+"</p></div>";
            }

            if(data[i].GameTop25 <=25){
                var html_top25 = "<div class=\"slot-top"+data[i].GameTop25+"\"></div>";
                $('.slot-item-hover').prepend(html_top25);
                $('.slot-item').prepend(html_top25);
            }
            game_list += "</div>";
        }
        game_list+="</div></div>";
        $("#main-slider").html(game_list);
    }).done(function(){
        $('#main-slider').liquidSlider({
            dynamicTabs: true,
            dynamicTabsHtml: true,
            autoHeight:true,
            slideEaseDuration:500,
            autoSlideInterval:-13000,
            dynamicTabsPosition: "top"
        });
        if(game!="1000" || game!="1004"){
            $(".slot-item").hover(function(){
//                    $(this).css({"backgroundPosition": "-146px 0"});
                $(this).css({"backgroundPosition": "-146px 0"});
            },function(){
                $(this).css({backgroundPosition:"0 0"})
            })
        }
    });
}