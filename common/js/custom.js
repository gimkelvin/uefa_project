//Delay
function showNotice() {$('.notice-tabs').animate({opacity: 10},100);}
setTimeout(showNotice, 300);

function showCustomerPanel() {$('#customer-panel').animate({opacity: 10},100);}
setTimeout(showCustomerPanel, 800);

function showWalletPanel() {$('#wallet-panel').animate({opacity: 10},100);}
setTimeout(showWalletPanel, 800);

function showCategories() {$('.gamebuttons-main').animate({opacity: 10},100);}
setTimeout(showCategories, 800);

//function showBannerSlot() {$('.banner-slot').animate({opacity: 10},100);}
//setTimeout(showBannerSlot, 800);

//Panel
$(window).scroll(function() {
    var hdr = $('.header-container').height();
    var scroll_y = $(this).scrollTop();

    if( scroll_y > hdr ) {
        $('.panel-content').animate({paddingTop: 0}, 10);
    } else {
        $('.panel-content').animate({paddingTop: 102}, 10);
    }
});

$(document).ready(function(){
    if(!getCookie('popCookie')) {
        $('#popup-notice').bPopup({easing: 'easeOutBack', speed: 400, positionStyle: 'fixed'});
    }

    //Progressive Jackpot
    $('.super-jackpot').jOdometer({
        increment: 0.01,
        counterStart:'8052535.00',
        counterEnd:'8102535.00',
        numbersImage: 'common/images/jodometer-numbers-gold.png',
        spaceNumbers: -1,
        formatNumber: true,
        widthNumber: 12,
        heightNumber: 31
    });

    //Year Select
/*    var myselect = document.getElementById("ageInYear");
    for (i=2000; i>=1915;i--){
        myselect.add(new Option(i),null);
    }
*/


/*        if($('#MemberID').val() != "" && $('#MemberPwd').val() != "" && $('#MemberValidPwd').val() != "" && $('#MemberName').val() != ""
            || $('#MemberPhone').val() != "" && $('#MemberEmail').val() != "" && $('#MemberCountry').val() != "" && $('#MemberAge').val() != ""){

            $('#terms').hide();
            $('#submitSignup').removeAttr('disabled','disabled');
            $('#submitSignup').addClass('btn-orange');
            $('#submitSignup').attr('onclick','signUp()');

        }*/


    //Signup Form Terms
    $('.btn-terms').click(function(){
        $(this).toggleClass('btn-green');
        $('input[type=checkbox]#agreedToTerms').click();
        //$('input[type=checkbox]#agreedToTerms').attr('checked', 'checked');
    });
    /*$('.btn-terms').click(function(){
     $(this).toggleClass('btn-green');

         if($('.btn-terms').hasClass('btn-green')){
             if($('#MemberID').val() != "" && $('#MemberPwd').val() != "" && $('#MemberValidPwd').val() != "" && $('#MemberName').val() != ""
             || $('#MemberPhone').val() != "" && $('#MemberEmail').val() != "" && $('#MemberCountry option:selected').val() != "" && $('#MemberAge option:selected').val() != "") {

                 $('#terms').hide();
                 $('#submitSignup').removeAttr('disabled','disabled');
                 $('#submitSignup').addClass('btn-orange');
                 $('#submitSignup').attr('onclick','signUp()');
             }
             else {
                 $('#terms').show();
                 $('#submitSignup').attr('disabled','disabled');
                 $('#submitSignup').removeClass('btn-orange');
                 $('#submitSignup').removeAttr('onclick','signUp()');
             }
             $('#terms').hide();
             $('#submitSignup').removeAttr('disabled','disabled');
             $('#submitSignup').addClass('btn-orange');
             $('#submitSignup').attr('onclick','signUp()');
         }
         else {
             $('#terms').show();
             $('#submitSignup').attr('disabled','disabled');
             $('#submitSignup').removeClass('btn-orange');
             $('#submitSignup').removeAttr('onclick','signUp()');
        }

     });*/

    /*$("#agreedTerms").change(function(){
        $('#agreedTerms').addClass('btn-green');
    });*/

    $("#MemberAge").change(function(){
        if ($("#MemberAge option:selected").text() <= 1997) {
            $('.btn-over18').addClass('btn-green');
        }else{
            $('.btn-over18').removeClass('btn-green');
        }
    });



    //Language Option
    $("#lang-active").click(function(){
        $("#lang-list").slideToggle('fast');
        $(this).toggleClass('lang-active-selected');
        $(".rotate-triangle2").toggleClass('rotate-triangle2-selected');
    });

    //Login
/*    $('#guest').show();
    $('#user').hide();
    $('.btn-login').click(function(){
        $('#guest').css('display','none');
        $('#user').css('display','inline-block');
        $('#buttonreplacement').show();
        $(".txtLoading").delay(3900).fadeOut(20);
        $(".txtAmount").delay(4000).fadeIn(20);
    });
    $('.btn-logout').click(function(){
        $('#guest').css('display','inline-block');
        $('#user').css('display','none');
    });*/

    //Slot Page
    $('#main-slider').liquidSlider({
        dynamicTabs: true,
        dynamicTabsHtml: true,
        autoHeight:true,
        slideEaseDuration:500,
        autoSlideInterval:-13000,
        dynamicTabsPosition: "top"
    });

    $(".slot-item-hover").hide();
    $(".slot-box").hover(function(){
        $(this).find(".slot-item-hover").show().fadeIn(100);
        $(this).find(".slot-item").fadeOut(100);
        $(this).find('.slot-item-hover').removeClass('opacity0');
        $(this).find('.slot-item-hover p').removeClass('opacity0');
    }, function() {
        $(this).find(".slot-item-hover").fadeOut(100);
        $(this).find(".slot-item").fadeIn(100);
    });

    //Slider
    $('#slides').superslides({
        hashchange: false,
        play: 8000,
        animation: 'fade', //slide or fade
        animation_speed: 1800,
        animation_easing: 'linear',
        inherit_width_from: '#slides',
        inherit_height_from: '#slides',
        pagination: false
    });

    //Slide Navigation
    $("#slides").hover(
        function () {$(".slides-navigation").animate({opacity: 100},800)},
        function () {$(".slides-navigation").animate({opacity: 0},200)}
    );

    //Outline Strokes
    $(".nav_hover_wrapper li h2").outlineLetters({color: '#000000', size: 2});
    $("#slides h2").outlineLetters({color: '#000000', size: 2});

    //Pagination
    $('#pagination-container').simplePagination({
        pagination_container: 'div.list-items',
        items_per_page: 5,
        number_of_visible_page_numbers: 10
    });

    //Text Count
    var text_max = 99;
    $('#textarea_feedback').html(text_max);
    $('#textarea').keyup(function() {
        var text_length = $('#textarea').val().length;
        var text_remaining = text_max - text_length;

        $('#textarea_feedback').html(text_remaining);
    });

    //Scrollbar Custom
    $(".notice-content, .terms-content").mCustomScrollbar({scrollInertia:200});
    $("#popup-wallet div.popup-content").mCustomScrollbar({scrollInertia:200});
    $("#popup-customer div.popup-content").mCustomScrollbar({scrollInertia:200});



    //Promo Banners
    $("div.promo_group_button").click(function () {
        $(this).closest('div.promo_group_body').find('div.promo_detail').slideToggle();
        $(this).find('i').toggle();
    });

    $("div.promo_group_body .promo_group_body_inner img:first-child").click(function () {
        $(this).closest('div.promo_group_body').find('div.promo_detail').slideToggle();
        $(this).closest('div.promo_group_body').find('i').toggle();
    });

    $('.signup-promo').click(function(){popupOpenCustomer()});

    //Popup
    $('.link-forgotpassword').click(function(){popupOpenForgotPassword()});

    $('#nav_promo, .panel-promo-item').click(function(){popupOpenCustomer()});
/*    $('#nav_deposit, .btn-deposit, .btn-withdraw').click(function(){popupOpenWallet()});*/
    $('#nav_withdraw, .btn-wallet, #nav_deposit, .btn-deposit, .btn-withdraw').click(function(){popupOpenWallet()});
    $('#nav_customer, .btn-more-notice').click(function(){popupOpenCustomer()});
    $('.gamebutton-casino').click(function(){popupOpenLogin()});

    $('.icon-terms').click(function(){popupTerms()});
    $('.btn-demo').click(function(){popupDemo()});

    $('#slides-demo-register, #slides-demo-deposit, #slides-demo-withdraw, #slides-demo-transfer').superslides({
        hashchange: false,
        play: false,
        animation: 'slide',
        animation_speed: 200,
        animation_easing: 'linear',
        inherit_width_from: '#slides-demo',
        inherit_height_from: '#slides-demo',
        pagination: true
    });
});

//Popup Functions
function popupOpenLogin(){
    $('#popup-login').bPopup({easing: 'easeOutBack', speed: 400, positionStyle: 'fixed'});
}
function popupOpenForgotPassword(){
    $('#popup-login .btn-popup-close').click();
    $('#popup-forgotpass').bPopup({easing: 'easeOutBack', speed: 400, positionStyle: 'fixed'});
}
function popupOpenWallet(){
    $('#popup-wallet').bPopup({easing: 'easeOutBack', speed: 400, positionStyle: 'fixed'});
}
function popupOpenCustomer(){
    $('#popup-customer').bPopup({easing: 'easeOutBack', speed: 400, positionStyle: 'fixed'});
}
function popupTerms(){
    $('#popup-terms').bPopup({easing: 'easeOutBack', speed: 400, positionStyle: 'fixed'});
}

function insertBoardCode(){
    BoardCode = $('#BoardCodeID').val();

    alert(BoardCode);
    //read_board('1',BoardCode,'');
}

function popupDemo(){
    $('#popup-demo').bPopup({easing: 'easeOutBack', speed: 400, positionStyle: 'fixed'});
}