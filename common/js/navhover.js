﻿$(function () {
	//Navigation Dropdown
	dropdownNavEvent();

	//Left & Right Panels
	$("#customer-panel a").click(function(){
		var id = $(this).attr("href").substring(1);
		$("html, body").animate({ scrollTop: $("#"+id).offset().top }, 1000, function(){
			$("#customer-panel").slideReveal("hide");
		});
	});

	var slider = $("#wallet-panel").slideReveal({
		push: false,
		position: "left",
		width: 220,
		trigger: $(".wallet-panel-handle"),
		shown: function(obj){
			obj.find(".wallet-panel-handle").html('<p class="customer-panel-arrow-left"></p>');
		},
		hidden: function(obj){
			obj.find(".wallet-panel-handle").html('<p class="wallet-panel-arrow-right"></p>');
		}
	});

	$("#wallet-panel a").click(function(){
		var id = $(this).attr("href").substring(1);
		$("html, body").animate({ scrollTop: $("#"+id).offset().top }, 1000, function(){
			$("#wallet-panel").slideReveal("hide");
		});
	});

	var slider2 = $("#customer-panel").slideReveal({
		push: false,
		position: "right",
		width: 220,
		trigger: $(".customer-panel-handle"),
		shown: function(obj){
			obj.find(".customer-panel-handle").html('<p class="wallet-panel-arrow-right"></p>');
		},
		hidden: function(obj){
			obj.find(".customer-panel-handle").html('<p class="customer-panel-arrow-left"></p>');
		}
	});
});


var mobileTimeout = null;
var mobileTimeoutB = null;
var casinoTimeout = null;
var casinoTimeoutB = null;
var sportsTimeout = null;
var sportsTimeoutB = null;
var slotsTimeout = null;
var slotsTimeoutB = null;
var othergamesTimeout = null;
var othergamesTimeoutB = null;

function dropdownNavEvent(){
	//Mobile
	$("#nav_mobile").mouseenter(function(){
		clearTimeout(mobileTimeout);
		mobileTimeout = null;
		if(mobileTimeoutB == null){
			mobileTimeoutB = setTimeout(function(){
				showMobile();
			},200);
		}
	});
	$("#nav_mobile").mouseleave(function(){
		clearTimeout(mobileTimeoutB);
		mobileTimeoutB=null;
		hideMobile();
	});
	$(".nav_hover_mobile").mouseenter(function(){
		clearTimeout(mobileTimeout);
		mobileTimeout = null;
		if(mobileTimeoutB == null){
			mobileTimeoutB = setTimeout(function(){
				showMobile();
			},200);
		}
	});
	$(".nav_hover_mobile").mouseleave(function(){
		clearTimeout(mobileTimeoutB);
		mobileTimeoutB=null;
		hideMobile();
	});

	//Casino
	$("#nav_casino").mouseenter(function(){
		clearTimeout(casinoTimeout);
		casinoTimeout = null;
		if(casinoTimeoutB == null){
			casinoTimeoutB = setTimeout(function(){
				showCasino();
			},200);
		}
	});
	$("#nav_casino").mouseleave(function(){
		clearTimeout(casinoTimeoutB);
		casinoTimeoutB=null;
		hideCasino();
	});
	$(".nav_hover_casino").mouseenter(function(){
		clearTimeout(casinoTimeout);
		casinoTimeout = null;
		if(casinoTimeoutB == null){
			casinoTimeoutB = setTimeout(function(){
				showCasino();
			},200);
		}
	});
	$(".nav_hover_casino").mouseleave(function(){
		clearTimeout(casinoTimeoutB);
		casinoTimeoutB=null;
		hideCasino();
	});

	//Sports
	$("#nav_sports").mouseenter(function(){
		clearTimeout(sportsTimeout);
		sportsTimeout = null;
		if(sportsTimeoutB == null){
			sportsTimeoutB = setTimeout(function(){
				showSports();
			},200);
		}
	});
	$("#nav_sports").mouseleave(function(){
		clearTimeout(sportsTimeoutB);
		sportsTimeoutB=null;
		hideSports();
	});
	$(".nav_hover_sports").mouseenter(function(){
		clearTimeout(sportsTimeout);
		sportsTimeout = null;
		if(sportsTimeoutB == null){
			sportsTimeoutB = setTimeout(function(){
				showSports();
			},200);
		}
	});
	$(".nav_hover_sports").mouseleave(function(){
		clearTimeout(sportsTimeoutB);
		sportsTimeoutB=null;
		hideSports();
	});

	//Slots
	$("#nav_slots").mouseenter(function(){
		clearTimeout(slotsTimeout);
		slotsTimeout = null;
		if(slotsTimeoutB == null){
			slotsTimeoutB = setTimeout(function(){
				showSlots();
			},200);
		}
	});
	$("#nav_slots").mouseleave(function(){
		clearTimeout(slotsTimeoutB);
		slotsTimeoutB=null;
		hideSlots();
	});
	$(".nav_hover_slots").mouseenter(function(){
		clearTimeout(slotsTimeout);
		slotsTimeout = null;
		if(slotsTimeoutB == null){
			slotsTimeoutB = setTimeout(function(){
				showSlots();
			},200);
		}
	});
	$(".nav_hover_slots").mouseleave(function(){
		clearTimeout(slotsTimeoutB);
		slotsTimeoutB=null;
		hideSlots();
	});

	//Other Games
	$("#nav_othergames").mouseenter(function(){
		clearTimeout(othergamesTimeout);
		othergamesTimeout = null;
		if(othergamesTimeoutB == null){
			othergamesTimeoutB = setTimeout(function(){
				showOthergames();
			},200);
		}
	});
	$("#nav_othergames").mouseleave(function(){
		clearTimeout(othergamesTimeoutB);
		othergamesTimeoutB=null;
		hideOthergames();
	});
	$(".nav_hover_othergames").mouseenter(function(){
		clearTimeout(othergamesTimeout);
		othergamesTimeout = null;
		if(othergamesTimeoutB == null){
			othergamesTimeoutB = setTimeout(function(){
				showOthergames();
			},200);
		}
	});
	$(".nav_hover_othergames").mouseleave(function(){
		clearTimeout(othergamesTimeoutB);
		othergamesTimeoutB=null;
		hideOthergames();
	});
}

function showMobile(){
	$(".item_mobile").addClass("over");
	$('.nav_hover_wrapper').show();
	$(".nav_hover_mobile").css({"z-index":100});

	$(".nav_hover_mobile").stop(true).animate({"height":"173px"},200,"easeInQuart",function(){
		if(check()){
			$("#nav_hover_cnt_mobile").stop(true).animate({"opacity":1},200,"easeInQuart",function(){});
		}
	});
}

function hideMobile(){
	$(".nav_hover_mobile").css({"z-index":99});
	mobileTimeout = setTimeout(function(){
		mobileTimeout = null;
		$(".item_mobile").removeClass("over");
		if(check()){
			$("#nav_hover_cnt_mobile").stop(true).animate({"opacity":0},100,"easeInQuart",function(){
				$(".nav_hover_mobile").stop(true).animate({"height":"0px"},100,"easeInQuart",function(){
				});
			});
		}else{
			$("#nav_hover_cnt_mobile").stop(true).animate({"top":"0px"},100,"easeInQuart",function(){
				$(".nav_hover_mobile").stop(true).animate({"height":"0px"},100,"easeInQuart",function(){
				});
			});
		}
	},250);
}

function showCasino(){
	$(".item_casino").addClass("over");
	$('.nav_hover_wrapper').show();
	$(".nav_hover_casino").css({"z-index":100});

	$(".nav_hover_casino").stop(true).animate({"height":"173px"},200,"easeInQuart",function(){
		if(check()){
			$("#nav_hover_cnt_casino").stop(true).animate({"opacity":1},200,"easeInQuart",function(){});
		}
	});
}

function hideCasino(){
	$(".nav_hover_casino").css({"z-index":99});
	casinoTimeout = setTimeout(function(){
		casinoTimeout = null;
		$(".item_casino").removeClass("over");
		if(check()){
			$("#nav_hover_cnt_casino").stop(true).animate({"opacity":0},100,"easeInQuart",function(){
				$(".nav_hover_casino").stop(true).animate({"height":"0px"},100,"easeInQuart",function(){
				});
			});
		}else{
			$("#nav_hover_cnt_casino").stop(true).animate({"top":"0px"},100,"easeInQuart",function(){
				$(".nav_hover_casino").stop(true).animate({"height":"0px"},100,"easeInQuart",function(){
				});
			});
		}
	},250);
}

function showSports(){
	$(".item_sports").addClass("over");
	$('.nav_hover_wrapper').show();
	$(".nav_hover_sports").css({"z-index":100});

	$(".nav_hover_sports").stop(true).animate({"height":"173px"},200,"easeInQuart",function(){
		if(check()){
			$("#nav_hover_cnt_sports").stop(true).animate({"opacity":1},200,"easeInQuart",function(){});
		}
	});
}

function hideSports(){
	$(".nav_hover_sports").css({"z-index":99});
	sportsTimeout = setTimeout(function(){
		sportsTimeout = null;
		$(".item_sports").removeClass("over");
		if(check()){
			$("#nav_hover_cnt_sports").stop(true).animate({"opacity":0},100,"easeInQuart",function(){
				$(".nav_hover_sports").stop(true).animate({"height":"0px"},100,"easeInQuart",function(){
				});
			});
		}else{
			$("#nav_hover_cnt_sports").stop(true).animate({"top":"0px"},100,"easeInQuart",function(){
				$(".nav_hover_sports").stop(true).animate({"height":"0px"},100,"easeInQuart",function(){
				});
			});
		}
	},250);
}

function showSlots(){
	$(".item_slots").addClass("over");
	$('.nav_hover_wrapper').show();
	$(".nav_hover_slots").css({"z-index":100});

	$(".nav_hover_slots").stop(true).animate({"height":"173px"},200,"easeInQuart",function(){
		if(check()){
			$("#nav_hover_cnt_slots").stop(true).animate({"opacity":1},200,"easeInQuart",function(){});
		}
	});
}

function hideSlots(){
	$(".nav_hover_slots").css({"z-index":99});
	slotsTimeout = setTimeout(function(){
		slotsTimeout = null;
		$(".item_slots").removeClass("over");
		if(check()){
			$("#nav_hover_cnt_slots").stop(true).animate({"opacity":0},100,"easeInQuart",function(){
				$(".nav_hover_slots").stop(true).animate({"height":"0px"},100,"easeInQuart",function(){
				});
			});
		}else{
			$("#nav_hover_cnt_slots").stop(true).animate({"top":"0px"},100,"easeInQuart",function(){
				$(".nav_hover_slots").stop(true).animate({"height":"0px"},100,"easeInQuart",function(){
				});
			});
		}
	},250);
}

function showOthergames(){
	$(".item_othergames").addClass("over");
	$('.nav_hover_wrapper').show();
	$(".nav_hover_othergames").css({"z-index":100});

	$(".nav_hover_othergames").stop(true).animate({"height":"173px"},200,"easeInQuart",function(){
		if(check()){
			$("#nav_hover_cnt_othergames").stop(true).animate({"opacity":1},200,"easeInQuart",function(){});
		}
	});
}

function hideOthergames(){
	$(".nav_hover_othergames").css({"z-index":99});
	othergamesTimeout = setTimeout(function(){
		othergamesTimeout = null;
		$(".item_othergames").removeClass("over");
		if(check()){
			$("#nav_hover_cnt_othergames").stop(true).animate({"opacity":0},100,"easeInQuart",function(){
				$(".nav_hover_othergames").stop(true).animate({"height":"0px"},100,"easeInQuart",function(){
				});
			});
		}else{
			$("#nav_hover_cnt_othergames").stop(true).animate({"top":"0px"},100,"easeInQuart",function(){
				$(".nav_hover_othergames").stop(true).animate({"height":"0px"},100,"easeInQuart",function(){
				});
			});
		}
	},250);
}

function check(){
	if ($.support.leadingWhitespace){return true;}
	else {return false;}
}