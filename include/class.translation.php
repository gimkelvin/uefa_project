<?php
class Translator {

    private $language	= 'thai';
    private $lang 		= array();

    public function __construct($language){
        $this->language = $language;
    }

    private function findString($str) {
        //$str = "findString= ".strtolower($str);
        if (array_key_exists($str, $this->lang[$this->language])) {
            //default thailand
            echo $this->lang[$this->language][$str];
            return;
        }
        //change lang thai
        echo $str;
    }

    private function splitStrings($str) {
        //$str = "splitStrings= ".strtolower($str);
        return explode('=',trim($str));
    }

    public function __($str) {
        //$str = "lastF= ".strtolower($str);
        if (!array_key_exists($this->language, $this->lang)) {
            if (file_exists('lang/'.$this->language.'.txt')) {
                $strings = array_map(array($this,'splitStrings'),file('lang/'.$this->language.'.txt'));
                foreach ($strings as $k => $v) {
                    $this->lang[$this->language][$v[0]] = $v[1];
                }
                return $this->findString($str);
            }
            else {
                //default show english
                echo $str;
            }
        }
        else {
            return $this->findString($str);
        }
    }
}
?>