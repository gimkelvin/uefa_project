<?php
/**
 * Created by PhpStorm.
 * User: jim
 * Date: 2015-06-11
 * Time: 오후 4:27
 */
session_start();
?>

<script type="text/javascript">
    var __lc = {};
    //__lc.license = 5524611;
    __lc.license = 5082911;

    <? if(isset($_SESSION['MemberID'])){?>
    __lc.params = [
        { name: 'MemberInfo', value: '<?=$_SESSION['MemberID']?>' }
    ];

    __lc.visitor = {
        name: '<?=$_SESSION['MemberID']?>',
        email: ''
    };
    <?}else{?>
    __lc.visitor = {
        name: 'Customer',
        email: ''
    };
    <?}?>
    (function() {
        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
</script>
<style>
html {overflow:hidden;}
</style>

<html>
<link rel="stylesheet" href="../common/css/style.css" type="text/css" media="screen" />
<script type="text/javascript">

function open_chat(){

    LC_API.open_chat_window();
    document.getElementById('livechat-full').style.right = 0;
    document.getElementById('livechat-full').style.position = '';
    //setTimeout(reLoad, 1000);
}
/*
function reLoad(){
    alert(document.getElementById('livechat-full').style.right = 0);
}*/
</script>
<body onload="open_chat();">
</body>
</html>
