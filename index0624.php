<?include_once $_SERVER["DOCUMENT_ROOT"] . "/include/class.translation.php";
if(isset($_GET['lang'])) {

    $translate = new Translator($_GET['lang']);
    include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php?lang=english";

}else {

    $translate = new Translator('thai');
    include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php";
}

include_once $_SERVER["DOCUMENT_ROOT"] . "/prc/check_server.php";
if (isset($_SESSION['MemberID'])) {
    include_once $_SERVER["DOCUMENT_ROOT"] . "/prc/session_extension.php";
}

if($detect->isMobile()){
    header("Location: http://uefa.isltest.net/mobile/index.php");
    die();
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="telephone=no">
    <!--[if lte IE 8]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/json2/20140204/json2.min.js"></script>
    <![endif]-->
    <!--[if lte IE 8]>
    <script>
        document.createElement('ng-include');
        document.createElement('ng-pluralize');
        document.createElement('ng-view');
        document.createElement('ng:include');
        document.createElement('ng:pluralize');
        document.createElement('ng:view');
    </script>
    <![endif]-->
    <!--[if lt IE 8]>
<!--    <script src="//cdnjs.cloudflare.com/ajax/libs/json2/20140204/json2.js"></script>-->
    <![endif]-->
    <!--[if lte IE 8]>
    <script>
        document.createElement('my-editor');
    </script>
    <![endif]-->
    <title>UEFA168.com</title>
    <link rel="shortcut icon" href="common/images/favicon.ico" type="image/gif">
    <link rel="stylesheet" href="common/css/sukhumvit-font.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="common/css/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="common/css/jquery.mCustomScrollbar.css" type="text/css" />
    <link rel="stylesheet" href="common/css/datetimepicker.css" type="text/css" />

</head>

<body ng-app="uefaApp">

<div id="popup-demo">
    <span class="btn-popup-close b-close"></span>

    <div class="demo-links" ng-controller="TabController as tab">
        <ul>
            <li id="demoRegister" ng-class="{ active:tab.isSet(1) }" ng-click="tab.setTab(1)">How to Register?</li>
            <li id="demoDeposit"  ng-class="{ active:tab.isSet(2) }" ng-click="tab.setTab(2)">How to Deposit?</li>
            <li id="demoWithdraw" ng-class="{ active:tab.isSet(3) }" ng-click="tab.setTab(3)">How to Withdraw?</li>
            <li id="demoTransfer" ng-class="{ active:tab.isSet(4) }" ng-click="tab.setTab(4)">How to Transfer?</li>
        </ul>
        <div class="clear"></div>

        <div ng-show="tab.isSet(1)">
            <div class="popup-content demo-content demo-register">
                <div class="demo-slide">
                    <div id="slides-demo-register">
                        <div class="slides-container">
                            <div class="demo-img">
                                <img src="common/images/demo/register-1.png" />

                                <h2 class="step1 text-left">
                                    <span class="step-number">1</span>
                                    To Sign Up as a member, please click on the "Sign Up Here" button.
                                </h2>
                            </div>
                            <div class="demo-img">
                                <img src="common/images/demo/register-2.png" />
                                <h2 class="step2 text-left">
                                    <span class="step-number">2</span>
                                    Complete the form and Click on the Terms & Conditions and
                                    Over 18 Years Old buttons to verify.
                                </h2>
                            </div>
                            <div class="demo-img">
                                <img src="common/images/demo/register-3.png" />
                                <h2 class="step3 text-left">
                                    <span class="step-number">3</span>
                                    Click Sign Up button to proceed. Once the form is submitted,
                                    please check your email to activate your account.<br /><br />
                                    Thank you for joining UEFA168.com!
                                </h2>
                            </div>
                        </div>
                        <nav class="slides-navigation">
                            <div class="containerArrow">
                                <a href="#" class="next">NEXT <span class="icon-arrownext"></span></a>
                                <a href="#" class="prev"><span class="icon-arrowprev"></span> PREVIOUS</a>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div ng-show="tab.isSet(2)">
            <div class="popup-content demo-content demo-deposit">
                <div class="demo-slide">
                    <div id="slides-demo-deposit">
                        <div class="slides-container">
                            <div class="demo-img">
                                <img src="common/images/demo/deposit-1.png" />

                                <h2 class="step1 text-left">
                                    <span class="step-number">1</span>
                                    To Deposit fund to your account, please login first.
                                </h2>
                            </div>
                            <div class="demo-img">
                                <img src="common/images/demo/deposit-2.png" />
                                <h2 class="step2 text-left">
                                    <span class="step-number">2</span>
                                    Click the Deposit button on the main navigation.
                                </h2>
                            </div>
                            <div class="demo-img">
                                <img src="common/images/demo/deposit-3.png" />
                                <h2 class="step3 text-left">
                                    <span class="step-number">3</span>
                                    Or, Click the Deposit button from the left wallet panel to
                                    proceed to Deposit Page.
                                </h2>
                            </div>
                            <div class="demo-img">
                                <img src="common/images/demo/deposit-4.png" />
                                <h2 class="step4 text-left">
                                    <span class="step-number">4</span>
                                    Choose your desired type of deposit payment option.
                                </h2>
                            </div>
                            <div class="demo-img">
                                <img src="common/images/demo/deposit-5.png" />
                                <h2 class="step5 text-left">
                                    <span class="step-number">5</span>
                                    Enter valid deposit details, and click on Deposit button to
                                    complete the deposit process. Thank you!
                                </h2>
                            </div>
                        </div>
                        <nav class="slides-navigation">
                            <div class="containerArrow">
                                <a href="#" class="next">NEXT <span class="icon-arrownext"></span></a>
                                <a href="#" class="prev"><span class="icon-arrowprev"></span> PREVIOUS</a>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div ng-show="tab.isSet(3)">
            <div class="popup-content demo-content demo-withdraw">
                <div class="demo-slide">
                    <div id="slides-demo-withdraw">
                        <div class="slides-container">
                            <div class="demo-img">
                                <img src="common/images/demo/withdraw-1.png" />

                                <h2 class="step1 text-left">
                                    <span class="step-number">1</span>
                                    To Withdraw funds from your account, please login first.
                                </h2>
                            </div>
                            <div class="demo-img">
                                <img src="common/images/demo/withdraw-2.png" />
                                <h2 class="step2 text-left">
                                    <span class="step-number">2</span>
                                    Click the Withdraw button on the main navigation.
                                </h2>
                            </div>
                            <div class="demo-img">
                                <img src="common/images/demo/withdraw-3.png" />
                                <h2 class="step3 text-left">
                                    <span class="step-number">3</span>
                                    Or, Click the Withdraw button from the left wallet panel to
                                    proceed to Withdraw Page.
                                </h2>
                            </div>
                            <div class="demo-img">
                                <img src="common/images/demo/withdraw-4.png" />
                                <h2 class="step4 text-left">
                                    <span class="step-number">4</span>
                                    Enter withdrawal details, and click on the Withdraw button to
                                    complete the withdrawal process. Thank you!
                                </h2>
                            </div>
                        </div>
                        <nav class="slides-navigation">
                            <div class="containerArrow">
                                <a href="#" class="next">NEXT <span class="icon-arrownext"></span></a>
                                <a href="#" class="prev"><span class="icon-arrowprev"></span> PREVIOUS</a>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div ng-show="tab.isSet(4)">
            <div class="popup-content demo-content demo-transfer">
                <div class="demo-slide">
                    <div id="slides-demo-transfer">
                        <div class="slides-container">
                            <div class="demo-img">
                                <img src="common/images/demo/transfer-1.png" />

                                <h2 class="step1 text-left">
                                    <span class="step-number">1</span>
                                    To transfer funds, please login first.
                                </h2>
                            </div>
                            <div class="demo-img">
                                <img src="common/images/demo/transfer-2.png" />
                                <h2 class="step2 text-left">
                                    <span class="step-number">2</span>
                                    Click the "My Wallet" button to proceed to the Wallet page.
                                </h2>
                            </div>
                            <div class="demo-img">
                                <img src="common/images/demo/transfer-3.png" />
                                <h2 class="step3 text-left">
                                    <span class="step-number">3</span>
                                    In the Money Transfer area, please select the game that you want
                                    to transfer from and to, and the amount you wish to transfer.
                                    <br /><br />
                                    Click the "Transfer" button to complete the transfer process.
                                </h2>
                            </div>
                            <div class="demo-img">
                                <img src="common/images/demo/transfer-4.png" />
                                <h2 class="step4 text-left">
                                    <span class="step-number">4</span>
                                    You can also use the left wallet panel for faster transfer process:
                                    Just select the game that you want to transfer from and to,
                                    and the transfer amount.
                                    <br /><br />
                                    Click the "Transfer" button to complete the transfer process.
                                </h2>
                            </div>
                        </div>
                        <nav class="slides-navigation">
                            <div class="containerArrow">
                                <a href="#" class="next">NEXT <span class="icon-arrownext"></span></a>
                                <a href="#" class="prev"><span class="icon-arrowprev"></span> PREVIOUS</a>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="popup-notice">
    <span class="btn-popup-close b-close"></span>
    <h1 class="logo"></h1>
<!--            <div class="notice-subject"></div>-->
            <div class="notice-content"></div>
    <p class="notice-close">
        <span class="b-close oneDay" ><?$translate->__('ไม่ต้องแสดงข้อความนี้สำหรับวันนี้');?>!</span> | <span class="txt-gray b-close"><?$translate->__('ปิด');?></span>
    </p>

</div>

<?if(!isset($_SESSION['MemberID'])){?>
<div id="popup-login">
    <span class="btn-popup-close b-close"></span>
    <h1 class="logo"></h1>

    <form id="popup-login-form">
        <input type="text" name="MemberID" class="login-input-user" placeholder="<?$translate->__('รหัสผู้ใช้');?>" />
        <input type="password" name="MemberPwd" class="login-input-pass" placeholder="<?$translate->__('รหัสผ่าน');?>" />
        <button class="btn-popup-login" onclick="login();" ><?$translate->__('เข้าสู่ระบบ');?></button>
        <input type="hidden" id="hiddenLogin" value="LOGIN">
        <input type="hidden" id="hiddenLang" value="<?php echo $_GET['lang']; ?>">
    </form>
    <p>
        <span class="txt-gold link-signup"><?$translate->__('ลงทะเบียนที่นี่!');?></span>   | <span class="txt-gray link-forgotpassword"><?$translate->__('ลืมรหัสผ่าน');?>?</span>
    </p>
</div>
<?}?>

<div id="popup-forgotpass">
    <span class="btn-popup-close b-close"></span>
    <h1 class="logo"></h1>

    <h2><?$translate->__('ลืมรหัสผ่าน');?>?</h2>
    <h4><?$translate->__('กรุณาใส่ชื่อและที่อยู่อีเมลของคุณด้านล่างและเราจะส่งคำแนะนำเกี่ยวกับวิธีการตั้งค่ารหัสผ่านของคุณ');?>.</h4>

    <form id="forgot-pass-form" >
        <input type="text" name="MemberID" class="login-input-user" placeholder="<?$translate->__('รหัสผู้ใช้');?>" />
        <input type="email" name="MemberEmail" class="login-input-email" placeholder="<?$translate->__('อีเมล');?>" />
        <button class="btn-popup-forgotpass" onclick="forgot_password();"  ><?$translate->__('ตกลง');?></button>
    </form>
</div>


<div id="popup-wallet">
    <span class="btn-popup-close b-close"></span>

    <div class="popup-links" ng-controller="TabController as tab">
        <ul>
            <li id="walletBalance"  ng-class="{ active:tab.isSet(1) }" ng-click="tab.setTab(1)"><span><?$translate->__('ความสมดุลและการถ่ายโอน');?></span></li>
            <li id="walletDeposit"  ng-class="{ active:tab.isSet(2) }" ng-click="tab.setTab(2)"><span><?$translate->__('ฝาก');?></span></li>
            <li id="walletWithdraw" ng-class="{ active:tab.isSet(3) }" ng-click="tab.setTab(3)"><span><?$translate->__('ถอน');?></span></li>
            <li id="walletBonus"    ng-class="{ active:tab.isSet(4) }" ng-click="tab.setTab(4)" onclick="load_comp();load_comp_history('mComp',1);load_comp_history('fComp',1);"><span><?$translate->__('ประวัติ โบนัส');?></span></li>
            <li id="walletFriends"  ng-class="{ active:tab.isSet(5) }" ng-click="tab.setTab(5)" onclick="load_friend_list(1);"><span><?$translate->__('รายชื่อเพื่อน');?></span></li>
            <li id="walletCoupon"   ng-class="{ active:tab.isSet(6) }" ng-click="tab.setTab(6)" onclick="load_coupon(1);"><span><?$translate->__('รายการคูปอง');?></span></li>
            <li id="walletCash"     ng-class="{ active:tab.isSet(7) }" ng-click="tab.setTab(7)" onclick="load_history(1);"><span><?$translate->__('ประวัติการเงิน');?></span></li>
            <li id="walletPassword" ng-class="{ active:tab.isSet(8) }" ng-click="tab.setTab(8)"><span><?$translate->__('เปลี่ยนรหัสผ่าน');?></span></li>
        </ul>
        <div class="clear"></div>

        <div ng-show="tab.isSet(1)">
            <div class="popup-content">
                <div class="balance-content">
                    <div class="header-box"><?$translate->__('คงเหลือ');?> <button class="btn btn-refresh float-right"><?$translate->__('ทำให้สดชื่น');?></button></div>
<!--                    <div class="row-box-bal highlight">
                        <div class="row-box-logo text-left"><img src="common/images/logo_1.png" /></div>
                        <div class="row-box-name text-left">AFB Sports</div>
                        <div class="row-box-icon text-center">
                            <div class="icon-casino"></div>
                            <div class="icon-sport on"></div>
                            <div class="icon-slot"></div>
                        </div>
                        <div class="row-box-balance text-left">฿ <strong>1,425,233</strong></div>
                        <div class="clear"></div>
                    </div>-->

                    <div class="row-box-bal">
                        <div class="row-box-logo text-left"><img src="common/images/logo_2.png" /></div>
                        <div class="row-box-name text-left">WFT Sports</div>
                        <div class="row-box-icon text-center">
                            <div class="icon-casino"></div>
                            <div class="icon-sport on"></div>
                            <div class="icon-slot"></div>
                        </div>
                        <div>฿ <strong class="wft-balance balance text-left"></strong></div>
                        <div class="clear"></div>
                    </div>
                    <div class="row-box-bal highlight">
                        <div class="row-box-logo text-left"><img src="common/images/logo_3.png" /></div>
                        <div class="row-box-name text-left">ASC Sports</div>
                        <div class="row-box-icon text-center">
                            <div class="icon-casino"></div>
                            <div class="icon-sport on"></div>
                            <div class="icon-slot"></div>
                        </div>
                        <div>฿ <strong class="asc-balance balance text-left"></strong></div>
                        <div class="clear"></div>
                    </div>
<!--                    <div class="row-box-bal">
                        <div class="row-box-logo text-left"><img src="common/images/logo_4.png" /></div>
                        <div class="row-box-name text-left"><?/*$translate->__('ผลการแข่งขัน');*/?></div>
                        <div class="row-box-icon text-center">
                            <div class="icon-casino"></div>
                            <div class="icon-sport on"></div>
                            <div class="icon-slot"></div>
                        </div>
                        <div>฿ <strong class="bet-balance balance text-left"></strong></div>
                        <div class="clear"></div>
                    </div>-->
                    <div class="row-box-bal highlight">
                        <div class="row-box-logo text-left"><img src="common/images/logo_5.png" /></div>
                        <div class="row-box-name text-left">Microgaming</div>
                        <div class="row-box-icon text-center">
                            <div class="icon-casino on"></div>
                            <div class="icon-sport"></div>
                            <div class="icon-slot on"></div>
                        </div>
                        <div>฿ <strong class="mg-balance balance text-left"></strong></div>
                        <div class="clear"></div>
                    </div>
                    <div class="row-box-bal">
                        <div class="row-box-logo text-left"><img src="common/images/logo_6.png" /></div>
                        <div class="row-box-name text-left">Gameplay</div>
                        <div class="row-box-icon text-center">
                            <div class="icon-casino on"></div>
                            <div class="icon-sport"></div>
                            <div class="icon-slot on"></div>
                        </div>
                        <div>฿ <strong class="opus-balance balance text-left"></strong></div>
                        <div class="clear"></div>
                    </div>
                    <div class="row-box-bal highlight">
                        <div class="row-box-logo text-left"><img src="common/images/logo_7.png" /></div>
                        <div class="row-box-name text-left">Asia Gaming</div>
                        <div class="row-box-icon text-center">
                            <div class="icon-casino"></div>
                            <div class="icon-sport"></div>
                            <div class="icon-slot on"></div>
                        </div>
                        <div>฿ <strong class="ag-balance balance text-left"></strong></div>
                        <div class="clear"></div>
                    </div>
                    <div class="row-box-bal">
                        <div class="row-box-logo text-left"><img src="common/images/logo_8.png" /></div>
                        <div class="row-box-name text-left">Gold Deluxe</div>
                        <div class="row-box-icon text-center">
                            <div class="icon-casino"></div>
                            <div class="icon-sport"></div>
                            <div class="icon-slot on"></div>
                        </div>
                        <div>฿ <strong class="gd-balance balance text-left"></strong></div>
                        <div class="clear"></div>
                    </div>
                    <div class="row-box-bal highlight">
                        <div class="row-box-logo text-left"><img src="common/images/logo_9.png" /></div>
                        <div class="row-box-name text-left">Ezugi</div>
                        <div class="row-box-icon text-center">
                            <div class="icon-casino"></div>
                            <div class="icon-sport"></div>
                            <div class="icon-slot on"></div>
                        </div>
                        <div>฿ <strong class="ezugi-balance balance text-left"></strong></div>
                        <div class="clear"></div>
                    </div>
<!--                    <div class="row-box-bal">
                        <div class="row-box-logo text-left"><img src="common/images/logo_10.png" /></div>
                        <div class="row-box-name text-left">Playtech</div>
                        <div class="row-box-icon text-center">
                            <div class="icon-casino"></div>
                            <div class="icon-sport"></div>
                            <div class="icon-slot on"></div>
                        </div>
                        <div class="row-box-balance text-left">฿ <strong>1,425,233</strong></div>
                        <div class="clear"></div>
                    </div>-->
                    <div class="row-box-bal highlight">
                        <div class="row-box-logo text-left"><img src="common/images/logo_11.png" /></div>
                        <div class="row-box-name text-left">Betsoft</div>
                        <div class="row-box-icon text-center">
                            <div class="icon-casino"></div>
                            <div class="icon-sport"></div>
                            <div class="icon-slot on"></div>
                        </div>
                        <div>฿ <strong class="bet-balance balance text-left"></strong></div>
                        <div class="clear"></div>
                    </div>

                    <div class="row-box-bal no-border-bottom">
                        <div class="row-box-total text-left"><?$translate->__('ยอดรวม');?></div>
                        <div class="row-box-totalbal text-left">฿ <span class="all-balance balance"></span></div>
                        <div class="clear"></div>
                    </div>
                </div>

              <form id="transfer-form" role="form">
                <div class="moneytransfer-content">
                    <div class="header-box"><?$translate->__('การโอนเงิน');?></div>
                    <div class="row-box-transfer highlight">
                        <label><?$translate->__('จาก');?></label>
                        <p>
                            <select name="FromWallet" onchange="getBalance(this.value,this);" >
                                <option value="" default><?$translate->__('กรุณาเลือกเกม');?></option>
                                <? foreach ($variables['gameText'] as $k => $v) { ?>
                                    <option value="<?= $k ?>"><?= $v ?></option>
                                <? } ?>
                            </select>
                            <span class="text-right game-balance"></span>
                        </p>

                        <div class="clear"></div>
                    </div>
                    <div class="row-box-transfer">
                        <label><?$translate->__('จำนวน');?></label>
                        <p>฿ &nbsp; <input type="text" name="Amount" value="0" class="text-left amount" style="width: 239px;"/></p>
                        <div class="clear"></div>
                                <span>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('100',this);">฿ 100</button>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('500',this);">฿ 500</button>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('1,000',this);">฿ 1,000</button>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('5,000',this);">฿ 5,000</button>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('10,000',this);">฿ 10,000</button>
                                    <button class="btn btn-gray btn-option"><?$translate->__('ทั้งหมด');?></button>
                                </span>
                        <div class="clear"></div>
                    </div>
                    <div class="row-box-transfer highlight">
                        <label><?$translate->__('ถึง');?></label>
                        <p>
                            <select name="ToWallet">
                                <option value="" default><?$translate->__('กรุณาเลือกเกม');?></option>
 <!--                                <option value="">AFB Sports</option>
                                    <option value="">Playtech</option>
                                    <option value=""><?/*$translate->__('ผลการแข่งขัน');*/?></option>-->
                                <? foreach ($variables['gameText'] as $k => $v) { ?>
                                    <option value="<?= $k ?>"><?= $v ?></option>
                                <? } ?>
                            </select>
                        </p>
                        <div class="clear"></div>
                    </div>
                    <div class="row-box-transfer-last text-center">
                        <p><button class="btn btn-orange btn-transfer" onclick="moneyProcess('#transfer-form',this);" ><?$translate->__('การโอนเงิน');?></button></p>
                        <div class="clear"></div>
                    </div>
                </div>
              </form>




            </div>
            <div class="clear"></div>
        </div>

        <!--Start Deposit-->
        <div ng-show="tab.isSet(2)">
            <div class="popup-content">
                <div class="header-box"><?$translate->__('ฝาก');?></div>

                <div ng-controller="DepositTabController as tab" class="ng-scope">
                    <ul class="depositTabs">
                        <li id="depositOption1" ng-class="{ active:tab.isSet(1) }" ng-click="tab.setTab(1)">HELP2PAY</li>
                        <li id="depositOption2" ng-class="{ active:tab.isSet(2) }" ng-click="tab.setTab(2)"><?$translate->__('ฝากธรรมดา');?></li>
                    </ul>
                    <div class="clear"></div>

                    <div ng-show="tab.isSet(1)">
                    <form id="deposit-form-help2pay">
                        <div class="row-box">
                            <label><?$translate->__('เลือกเกม');?></label>
                            <p>
                                <select name="Wallet">
                                <option value=""><?$translate->__('กรุณาเลือกเกม');?></option>
<!--                                <option value="">AFB Sports</option>
                                    <option value="">Playtech</option>
                                    <option value=""><?/*$translate->__('ผลการแข่งขัน');*/?></option>-->
                                    <? foreach ($variables['gameText'] as $k => $v) { ?>
                                        <option value="<?= $k ?>"><?= $v ?></option>
                                    <? } ?>
                                </select>
                            </p>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box">
                            <label><?$translate->__('หมายเลขโทรศัพท์');?></label>
                            <p>
                                <input type="text" placeholder="000-000-000" name="MemberPhone"/>

                            </p>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box">
                            <label><?$translate->__('จำนวนเงินฝาก');?></label>
                            <p>฿ &nbsp;
                                <input type="text" placeholder="0" class="text-left amount" style="width: 170px;"
                                       onkeyup="addComma(this);"
                                       name="Amount"/>
                                <em>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('100',this);">฿ 100</button>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('500',this);">฿ 500</button>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('1,000',this);">฿ 1,000</button>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('5,000',this);">฿ 5,000</button>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('10,000',this);">฿ 10,000</button>
                                    <button class="btn btn-gray btn-option"><?$translate->__('ทั้งหมด');?></button>
                                </em>
                        </p>
                            <div class="clear"></div>
                    </div>
                        <div class="row-box">
                            <label><?$translate->__('ชื่อของผู้ฝากเงิน');?></label>
                            <p>
                                <input type="text" placeholder="<?$translate->__('2 ถึง 10 อักษร');?>"
                                       name="Depositor"/>
                            </p>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box-last text-center">
                            <button class="btn btn-orange btn-deposit" onclick="moneyProcess('#deposit-form-help2pay',this);" ><?$translate->__('ฝาก');?></button>
                        </div>
                    </form>
                    </div>

                    <div ng-show="tab.isSet(2)">
                        <form id="deposit-form" >
                        <div class="row-box">
                            <label><?$translate->__('เลือกเกม');?></label>
                            <p>
                                <select name="Wallet">
                                    <option value=""><?$translate->__('กรุณาเลือกเกม');?></option>
<!--                                <option value="">AFB Sports</option>
                                    <option value="">Playtech</option>
                                    <option value=""><?/*$translate->__('ผลการแข่งขัน');*/?></option>-->
                                    <? foreach ($variables['gameText'] as $k => $v) { ?>
                                        <option value="<?= $k ?>"><?= $v ?></option>
                                    <? } ?>
                                </select>
                            </p>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box">
                            <label><?$translate->__('หมายเลขโทรศัพท์');?></label>
                            <p>
                                <input type="text" placeholder="000-000-000" name="MemberPhone"/>
                            </p>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box">
                            <label><?$translate->__('ยอดฝาก');?></label>
                            <p>฿ &nbsp;
                                <input type="text" name="Amount" placeholder="0" class="text-left addtxtamount amount" style="width: 170px;"
                                       onkeyup="addComma(this);"/>
                                <em>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('100',this);">฿ 100</button>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('500',this);">฿ 500</button>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('1,000',this);">฿ 1,000</button>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('5,000',this);">฿ 5,000</button>
                                    <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('10,000',this);">฿ 10,000</button>
                                    <button class="btn btn-gray btn-option"><?$translate->__('ทั้งหมด');?></button>
                                </em>
                            </p>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box">
                            <label><?$translate->__('ชื่อของผู้ฝากเงิน');?></label>
                            <p>
                                <input type="text" placeholder="<?$translate->__('2 ถึง 10 อักษร');?>" name="Depositor" />
                            </p>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box">
                            <label> <?$translate->__('วันที่ & เวลา');?></label>
                            <p>
                                <input type="text" name="DepositDate" id="datetimepicker" class="inputDate"/>
                            </p>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box">
                            <label><?$translate->__('ช่องทางการฝากเงิน');?></label>
                            <p>
                                <select name="DepositType">
                                    <option value=""><?$translate->__('ช่องทางการฝากเงิน');?></option>
                                    <option value="ATM"><?$translate->__('เอทีเอ็ม');?></option>
                                    <option value="Counter"><?$translate->__('เคาท์เตอร์');?></option>
                                </select>
                            </p>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box">
                            <label><?$translate->__('ความเห็น');?></label>
                            <p>
                                <textarea rows="6" cols="20" name="UserMemo" placeholder="<?$translate->__('ไม่เกิน 300 ตัวอักษร');?>"></textarea>
                            </p>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box-last text-center">
                            <button class="btn btn-orange btn-deposit" onclick="moneyProcess('#deposit-form',this);" ><?$translate->__('ฝาก');?></button>
                        </div>
                    </form>

                    </div>
                </div>
            </div>
        </div>

    <div ng-show="tab.isSet(3)">
            <form id="withdrawal-form" role="form">
            <div class="popup-content">
                <div class="header-box"><?$translate->__('ถอน');?></div>
                <div class="row-box">
                    <label><?$translate->__('เลือกเกม');?></label>
                    <p>
                        <select name="Wallet">
                            <option default><?$translate->__('กรุณาเลือกเกม');?></option>
 <!--                                <option value="">AFB Sports</option>
                                    <option value="">Playtech</option>
                                    <option value=""><?/*$translate->__('ผลการแข่งขัน');*/?></option>-->
                            <? foreach ($variables['gameText'] as $k => $v) { ?>
                                <option value="<?= $k ?>"><?= $v ?></option>
                            <? } ?>
                        </select>
                    </p>
                    <div class="clear"></div>
                </div>
                <div class="row-box">
                    <label><?$translate->__('จำนวนเงินที่ถอน');?></label>
                    <p>
                        ฿ &nbsp;
                        <input type="text" name="Amount" placeholder="0" class="text-left amount" style="width: 170px;" />
                        <em>
                            <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('100',this);">฿ 100</button>
                            <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('500',this);">฿ 500</button>
                            <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('1,000',this);">฿ 1,000</button>
                            <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('5,000',this);">฿ 5,000</button>
                            <button class="btn btn-gray btn-option"  onclick="javascript:addAmount('10,000',this);">฿ 10,000</button>
                            <button class="btn btn-gray btn-option"><?$translate->__('ทั้งหมด');?></button>
                        </em>
                    </p>
                    <div class="clear"></div>
                </div>
                <div class="row-box">
                    <label><?$translate->__('ธนาคาร');?></label>
                    <p>
                        <select name="BankCode">
                            <option value="" default><?$translate->__('กรุณาเลือกธนาคาร');?></option>
                            <? foreach ($variables['bank'] as $k => $v) { ?>
                                <option value="<?= $k ?>"><?= $v ?></option>
                            <? } ?>
                        </select>
                        <input type="text"  name="AccountNumber" placeholder="<?$translate->__('เลขที่บัญชี');?>" style="width: 141px;" />
                    </p>
                    <div class="clear"></div>
                </div>
                <div class="row-box">
                    <label><?$translate->__('ชื่อบัญชี');?></label>
                    <p>
                        <input type="text" name="BankAccount" placeholder="<?$translate->__('2 ถึง 10 อักษร');?>" />
                    </p>
                    <div class="clear"></div>
                </div>
                <div class="row-box">
                    <label><?$translate->__('หมายเลขโทรศัพท์');?></label>
                    <p>
                        <input type="text" name="MemberPhone" placeholder="000-000-000" />
                    </p>
                    <div class="clear"></div>
                </div>
                <div class="row-box">
                    <label><?$translate->__('ความเห็น');?></label>
                    <p>
                        <textarea rows="6" name="UserMemo" cols="20" placeholder="<?$translate->__('ไม่เกิน 300 ตัวอักษร');?>"></textarea>
                    </p>
                    <div class="clear"></div>
                </div>
                <div class="row-box-desc">
                    <p>
                        <?$translate->__('ในกรณีที่ใช้หลายชื่อในการทำรายการ จะต้องมีการตรวจสอบเพื่อความปลอดภัย');?>.
                    </p>
                    <div class="clear"></div>
                </div>
                <div class="row-box-last text-center">
                    <button class="btn btn-orange btn-withdraw" onclick="moneyProcess('#withdrawal-form',this);"><?$translate->__('ถอน');?></button>
                </div>
            </div>
          </form>

        </div>


        <div ng-show="tab.isSet(4)">
            <div id="bonus-history" class="popup-content">
                <div class="header-box"><?$translate->__('ประวัติ โบนัส');?></div>
                <div class="header-row-box">
                    <div class="header-title width33 text-center"><?$translate->__('โบนัสคงเหลือ');?></div>
                    <div class="header-title width34 text-center"><?$translate->__('โบนัส');?></div>
                    <div class="header-title width33 text-center"><?$translate->__('ยอดโบนัส');?></div>
                    <div class="clear"></div>
                </div>

                <div class="list-row-box">
                    <div class="row-col width33 text-center current-comp"></div>
                    <div class="row-col width34 text-center used-comp"></div>
                    <div class="row-col width33 text-center total-comp"></div>
                </div>

                <div class="clear"></div>

                <div class="row-option-box">
                    <form id="comp-form" role="form">
                    <p>
                        <select name="GameCode" onchange="getBalance(this.value,this);">
                            <option value="" default><?$translate->__('กรุณาเลือกเกม');?></option>
<!--                                <option value="">AFB Sports</option>
                                    <option value="">Playtech</option>
                                    <option value=""><?/*$translate->__('ผลการแข่งขัน');*/?></option>-->
                            <? foreach ($variables['gameText'] as $k => $v) { ?>
                                <option value="<?= $k ?>"><?= $v ?></option>
                            <? } ?>
                        </select>
                    </p>
                    <p><input type="text" name="CompAmount" class="text-right" placeholder="<?$translate->__('จำนวน');?>" onkeyup="addComma(this);"></p>
                    </form>
                    <p><button class="btn btn-orange" onclick="moneyProcess('#comp-form',this)"><?$translate->__('ดู');?></button></p>
                    <span class="text-right game-balance"></span>
                    <div class="clear"></div>
                </div>

                <div class="comp-content-table">
                    <div class="header-box"><?$translate->__('ประวัติ');?></div>
                    <div class="headerShadow">
                        <div class="header-row-box">
                            <div class="header-title width10 text-center"><?$translate->__('จำนวน');?></div>
                            <div class="header-title width15 text-center"><?$translate->__('วันที่');?></div>
                            <div class="header-title width20 text-center"><?$translate->__('เกมส์');?></div>
                            <div class="header-title width30 text-center"><?$translate->__('หมวดหมู่');?></div>
                            <div class="header-title width10 text-center"><?$translate->__('จำนวน เดิมพัน');?></div>
                            <div class="header-title width15 text-center">Comp</div>
                            <div class="clear"></div>
                        </div>
                        <div id="comp-container">
                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                <tbody class="mcomp-list"><tr><td colspan="7" class="text-center" style="height:100px;padding-top:100px;"><?$translate->__('โหลด');?>...</td></tr></tbody>
                            </table>
                            <div class="pagination-container">
                                <ul class="pagination pagination-mcomp">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div ng-show="tab.isSet(5)">
            <div class="popup-content">
                <div class="header-box"><?$translate->__('รายชื่อเพื่อน');?></div>

                <div class="header-row-box">
                    <div class="header-title width18 text-center"><?$translate->__('หมวดหมู่');?></div>
                    <div class="header-title width25 text-center"><?$translate->__('เกม');?></div>
                    <div class="header-title width25 text-center"><?$translate->__('ชนิด');?></div>
                    <div class="header-title width16 text-center"><?$translate->__('คงเหลือ');?></div>
                    <div class="header-title width16 text-center"><?$translate->__('วันที่');?></div>
                    <div class="clear"></div>
                </div>
                <div class="list-row-box">
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tbody class="friend-list"><tr><td colspan="7" class="text-center" style="height:100px;padding-top:100px;">Loading..</td></tr></tbody>
                    </table>
                </div>
                <div class="pagination-container">
                    <ul class="pagination pagination-friend">
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div ng-show="tab.isSet(6)">
            <div class="popup-content">
                <div class="header-box"><?$translate->__('รายการคูปอง');?></div>
                <div class="header-row-box">
                    <div class="header-title width18 text-center"><?$translate->__('หมวดหมู่');?></div>
                    <div class="header-title width25 text-center"><?$translate->__('เกม');?></div>
                    <div class="header-title width25 text-center"><?$translate->__('ชนิด');?></div>
                    <div class="header-title width16 text-center"><?$translate->__('คงเหลือ');?></div>
                    <div class="header-title width16 text-center"><?$translate->__('วันที่');?></div>
                    <div class="clear"></div>
                </div>
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tbody class="coupon-list"><tr><td colspan="7" class="text-center" style="height:100px;padding-top:100px;">Loading..</td></tr></tbody>
                </table>
                <div class="pagination-container">
                    <ul class="pagination pagination-coupon">
                    </ul>
                </div>
                <div class="clear"></div>
<!--

                <div class="list-row-box">
                    <div class="row-col width18 text-center">GD</div>
                    <div class="row-col width25 text-center">Baccarat</div>
                    <div class="row-col width25 text-center">3D Baccarat</div>
                    <div class="row-col width16 text-center">18</div>
                    <div class="row-col width16 text-center">2014-06-08</div>
                </div>
                <div class="list-row-box">
                    <div class="row-col width18 text-center">GD</div>
                    <div class="row-col width25 text-center">Baccarat</div>
                    <div class="row-col width25 text-center">3D Baccarat</div>
                    <div class="row-col width16 text-center">18</div>
                    <div class="row-col width16 text-center">2014-06-08</div>
                </div>

                <div class="clear"></div>-->
            </div>
        </div>
        <div ng-show="tab.isSet(7)">
            <div class="popup-content">
                <div class="header-box"><?$translate->__('ประวัติการเงิน');?></div>
                <div class="header-row-box">
                    <div class="header-title width10 text-center"><?$translate->__('ชนิด');?></div>
                    <div class="header-title width20 text-center"><?$translate->__('เกม');?></div>
                    <div class="header-title width15 text-center"><?$translate->__('จำนวน');?></div>
                    <div class="header-title width15 text-center"><?$translate->__('ประเภทโปรโมชั่น');?></div>
                    <div class="header-title width15 text-center"><?$translate->__('โปรโมชั่นจำนวน');?></div>
                    <div class="header-title width10 text-center"><?$translate->__('สถานะ');?></div>
                    <div class="header-title width15 text-center"><?$translate->__('วันที่');?></div>
                    <div class="clear"></div>
                </div>
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tbody class="history-list"><tr><td colspan="7" class="text-center" style="height:100px;padding-top:100px;">Loading..</td></tr></tbody>
                </table>
                <div class="pagination-container">
                    <ul class="pagination pagination-history">
                    </ul>
                </div>
                <div class="clear"></div>
                <!--


                                <div class="list-row-box">
                                    <div class="row-col width18 text-center">GD</div>
                                    <div class="row-col width25 text-center">Baccarat</div>
                                    <div class="row-col width25 text-center">3D Baccarat</div>
                                    <div class="row-col width16 text-center">18</div>
                                    <div class="row-col width16 text-center">2014-06-08</div>
                                </div>
                                <div class="list-row-box">
                                    <div class="row-col width18 text-center">GD</div>
                                    <div class="row-col width25 text-center">Baccarat</div>
                                    <div class="row-col width25 text-center">3D Baccarat</div>
                                    <div class="row-col width16 text-center">18</div>
                                    <div class="row-col width16 text-center">2014-06-08</div>
                                </div>

                                <div class="clear"></div>-->
            </div>
        </div>
        <div ng-show="tab.isSet(8)">
            <div class="popup-content content-config">
                <div class="header-box"><?$translate->__('เปลี่ยนรหัสผ่าน');?></div>
                <form id="settings-form" role="form">
                <div class="row-box">
                    <label><?$translate->__('รหัสผ่านปัจจุบัน');?></label><!--Current Password-->
                    <p><input type="password" name="CurrentPassword" class="text-left" /></p>
                    <div class="clear"></div>
                </div>
                <div class="row-box">
                    <label><?$translate->__('รหัสผ่านใหม่');?></label><!--New Password-->
                    <p><input type="password" name="ChangePassword" /></p>
                    <div class="clear"></div>
                </div>
                <div class="row-box">
                    <label><?$translate->__('ยืนยันรหัสผ่านใหม่');?></label><!--Confirm New Password-->
                    <p><input type="password" name="ValidChangePassword" /></p>
                    <div class="clear"></div>
                </div>
                <div class="row-box-last text-center">
                    <a href="#" class="btnForm" onclick="moneyProcess('#settings-form',this)"><button class="btn btn-orange btn-transfer"><?$translate->__('เปลี่ยนรหัสผ่าน');?></button></a>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="popup-customer">
    <span class="btn-popup-close b-close"></span>

    <div class="popup-links" ng-controller="TabController as tab">
        <ul>
            <li id="customerPromos" ng-class="{ active:tab.isSet(1) }" ng-click="tab.setTab(1)"><span><?$translate->__('โปรโมชั่น');?></span></li>
            <li id="customerNotice" ng-class="{ active:tab.isSet(2) }" ng-click="tab.setTab(2)" onclick="load_board(1,1,'#notice');"><span><?$translate->__('แจ้งเตือน');?></span></li>
            <li id="customerEvent" ng-class="{ active:tab.isSet(3) }" ng-click="tab.setTab(3)" onclick="load_board(1,3,'#notice');"><span><?$translate->__('กิจกรรม');?></span></li>
            <li id="customerFaq" ng-class="{ active:tab.isSet(4) }" ng-click="tab.setTab(4)" onclick="load_board(1,2,'#notice');"><span><?$translate->__('คำถามที่พบบ่อย');?></span></li>
            <li id="customer1on1" ng-class="{ active:tab.isSet(5) }" ng-click="tab.setTab(5)" onclick="$('#customer1on1').click();"><span><?$translate->__('บริการลูกค้า');?></span></li>
            <li id="customerPartnership" ng-class="{ active:tab.isSet(6) }" ng-click="tab.setTab(6)"><span><?$translate->__('พันธมิตร');?></span></li>
        </ul>
        <div class="clear"></div>

        <div ng-show="tab.isSet(1)">
            <div class="popup-content">
                <div class="header-box"><?$translate->__('โปรโมชั่น');?></div>
                <div class="promo-items">

                    <div class="promo_group_body" id="promo1">
                        <div class="promo_group_body_inner">
                            <img src="common/images/promo-banner1.png" />
                            <div class="promo_detail" style="display: none;">
                                <h1><?$translate->__('ฝากผ่านระบบออนไลน์หน้าเว็บ รับเพิ่ม 5% สูงสุด 500 บาท');?>!</h1>
                                <ol>
                                    <li><?$translate->__('สมาชิกที่มีการฝากเงินผ่านระบบออนไลน์ โบนัสที่ได้รับจะคิดให้จาก 5% ของยอดฝากโดยสมาชิกต้องฝากเงินขั้นต่ำ 750 บาท หรือมากกว่า สมาชิกสามารถขอรับโบนัสได้สูงสุดถึง 500 บาทต่อวัน ต่อ 1 ยูซเซอร์เท่านั้น');?>.</li>
                                    <li><?$translate->__('สมาชิกไม่สามารถแจ้งถอนโบนัสเป็นเงินสดได้ทันที จนกว่าจะมียอดเดิมพันหมุนเวียนถึง 2  เท่า ของยอดฝากรวมกับยอดโบนัส จึงจะขอแจ้งถอนได้');?>.</li>
                                    <li><?$translate->__('โบนัสนี้สามารถใช้สิทธิ์ได้เฉพาะเว็บไซด์เดิมพันกีฬา เท่านั้น');?>.</li>
                                    <li><?$translate->__('ทีมงานขอสงวนสิทธิในการเปลี่ยนแปลงกฎติกาและของรางวัลโดยไม่ต้องแจ้งให้ทราบล่วงหน้า สมาชิกท่านใด มีเจตนาส่อไปในทางทุจริต ทาง เว็บ ขอตัดสิทธิ์ทุกกรณี');?>.</li>
                                </ol>
                                <div class="promo_group_button">
                                    <button class="btn btn-orange">Hide Promo Info</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="promo_group_body" id="promo2">
                        <div class="promo_group_body_inner">
                            <img src="common/images/promo-banner2.png" />
                            <div class="promo_detail" style="display: none;">
                                <h1><?$translate->__('แทงบอลและกีฬา อื่นๆ คอมX3 & บอลสเต็ป');?>!</h1>
                                <p><?$translate->__('โปรโมชั่นนี้ เว็บไซด์ จะคำนวณค่าคอมมิชชั่น จากข้อมูลเว็บเดิมพัน ที่สมาชิกเดิมพันเข้ามา');?>.</p>
                                <ol>
                                    <li><?$translate->__('โปรโมชั่นนี้สามารถใช้สิทธิ์ได้เฉพาะเว็บไซด์เดิมพันกีฬา เท่านั้น');?>.</li>
                                    <li><?$translate->__('โปรโมชั่นนี้สำหรับสมาชิกที่มียอดเดิมพันหมุนเวียนเกิน 1 เท่า');?>.</li>
                                    <li><?$translate->__('โปรโมชั่นนี้จะคำนวณตั้งแต่วันจันทร์ถึงวันอาทิตย');?>์.</li>
                                    <li><?$translate->__('ทีมงานขอสงวนสิทธิในการเปลี่ยนแปลงกฎติกาและของรางวัลโดยไม่ต้องแจ้งให้ทราบล่วงหน้า สมาชิกท่านใด มีเจตนาส่อไปในทางทุจริต ทาง เว็บ ขอตัดสิทธิ์ทุกกรณี');?>.</li>
                                </ol>
                                <div class="promo_group_button">
                                    <button class="btn btn-orange">Hide Promo Info</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="promo_group_body" id="promo3">
                        <div class="promo_group_body_inner">
                            <img src="common/images/promo-banner3.png" />
                            <div class="promo_detail" style="display: none;">
                                <h1><?$translate->__('สมัครสมาชิกใหม่ รับทันที 15 % แต่ไม่เกิน 500 ต่อคน');?>!</h1>
                                <p><?$translate->__('สมัครสมาชิกใหม่กับเรา รับทันทีโบนัส 15% จากยอดฝากของท่าน โดยสมาชิกใหม่สามารถรับโบนัสสูงสุดได้ไม่เกิน 500 บาท');?>.</p>
                                <ol>
                                    <li><?$translate->__('สมาชิกที่ต้องการขอรับโปรโมชั่นนี้จะต้องทำการฝากเงินขั้นต่ำ 750 บาท หรือมากกว่า และยอดการฝากเงินของสมาชิกจะต้องมีสถานะฝากสำเร็จเรียบร้อยแล้วเท่านั้น สมาชิกจึงจะมีคุณสมบัติในการขอรับโปรโมชั่นนี้ได้ อนุญาตให้ทำการขอรับโบนัสได้ 1 ครั้ง ต่อ 1 ยูซเซอร์เท่านั้น');?>.</li>
                                    <li><?$translate->__('สมาชิกใหม่สามารถถอนโบนัสเป็นเงินสดได้ก็ต่อเมื่อ มียอดเดิมพันหมุนเวียน เกิน 4 เท่า ของยอดเงินฝากเริ่มต้นรวมกับยอดโบนัสแล้วเท่านั้น');?>.</li>
                                    <li><?$translate->__('การสมัครสมาชิกใหม่ สมาชิกจะต้องมีข้อมูลไม่ซ้ำกับฐานข้อมูลเดิมของเว็บเรา');?>.</li>
                                    <li><?$translate->__('ทีมงานขอสงวนสิทธิในการเปลี่ยนแปลงกฎติกาและของรางวัลโดยไม่ต้องแจ้งให้ทราบล่วงหน้า สมาชิกท่านใด มีเจตนาส่อไปในทางทุจริต ทาง เว็บ ขอตัดสิทธิ์ทุกกรณี');?>.</li>
                                </ol>
                                <div class="promo_group_button">
                                    <button class="btn btn-orange">Hide Promo Info</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="promo_group_body" id="promo4">
                        <div class="promo_group_body_inner">
                            <img src="common/images/promo-banner4.png" />
                            <div class="promo_detail" style="display: none;">
                                <h1><?$translate->__('โบนัสวันเกิด');?>!</h1>
                                <p><?$translate->__('ในการขอร่วมสนุกกับโปรโมชั่นนี้ สมาชิกจะต้องทำการแจ้งการขอรับโปรโมชั่นผ่านทางห้องสนทนาสด หรือติดต่อพนักงาน โดยใช้หัวข้อ “โปรโมชั่นสุขสันต์วันเกิด” และทำการระบุชื่อผู้ใช้งานพร้อมแนบสำเนาบัตรประชาชน หรือ บัตรใดๆ ก็ได้ที่แสดงวันเดือนปี เกิดของสมาชิกอย่างชัดเจนมาพร้อมกันด้วย');?>.</p>
                                <ol>
                                    <li><?$translate->__('สมาชิกสามารถแจ้งขอรับสิทธิ์คืนยอดได้เสียได้ทางเว็บไซด');?>์.</li>
                                    <li><?$translate->__('โปรโมชั่นนี้สำหรับสมาชิกที่ทำการลงทะเบียนกับทางเว็บไซด์ตั้งแต่ 1 เดือนขึ้นไป');?>.</li>
                                    <li><?$translate->__('สมาชิกที่ขอรับสิทธิ์โปรโมชั่นนี้ไม่สามารถขอแจ้งถอนเป็นเงินสดได้ทันที สมาชิกต้องมียอดหมุนเวียนเกิน 4 เท่า ของยอดโบนัสที่ได้รับก่อนจึงจะแจ้งถอนได้');?>.</li>
                                    <li><?$translate->__('สมาชิกที่ขอรับสิทธิ์จะต้องแสดงหลักฐานระบุตัวตนเพื่อขอรับโบนัส');?>.</li>
                                    <li><?$translate->__('ทีมงานขอสงวนสิทธิในการเปลี่ยนแปลงกฎติกาและของรางวัลโดยไม่ต้องแจ้งให้ทราบล่วงหน้า สมาชิกท่านใด มีเจตนาส่อไปในทางทุจริต ทาง เว็บ ขอตัดสิทธิ์ทุกกรณี');?>.</li>
                                </ol>
                                <div class="promo_group_button">
                                    <button class="btn btn-orange">Hide Promo Info</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="promo_group_body" id="promo5">
                        <div class="promo_group_body_inner">
                            <img src="common/images/promo-banner5.png" />
                            <div class="promo_detail" style="display: none;">
                                <h1><?$translate->__('โปรโมชั่น คาสิโน');?>!</h1>
                                <p><?$translate->__('เป็นสมาชิกกับทางเว็บไซด์รับทันที คอมมิชชั่น 0.8 % คิดให้ทุกยอดการเล่นที่มีการได้เสียในเกมส์นั้นๆ');?>.</p>
                                <ol>
                                    <li><?$translate->__('สมัครเป็นสมาชิกกับทางเว็บไซด์รับทันที คอมมิชชั่น 0.8 % คิดให้ทุกยอดการเล่นที่มีการได้เสียในเกมส์นั้นๆ');?>.</li>
                                    <li><?$translate->__('โปรโมชั่นนี้ไม่สามารถใช้ร่วมกับ โบนัสสมัครสมาชิกใหม่ได');?>้.</li>
                                    <li><?$translate->__('ระบบจะคำนวณคอมมิชชั่นให้ทุกๆการวางเดิมพัน');?> ้.</li>
                                    <li><?$translate->__('ทีมงานขอสงวนสิทธิในการเปลี่ยนแปลงกฎติกาและของรางวัลโดยไม่ต้องแจ้งให้ทราบล่วงหน้า สมาชิกท่านใด มีเจตนาส่อไปในทางทุจริต ทาง เว็บ ขอตัดสิทธิ์ทุกกรณี');?>.</li>
                                </ol>
                                <div class="promo_group_button">
                                    <button class="btn btn-orange">Hide Promo Info</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="promo_group_body" id="promo6">
                        <div class="promo_group_body_inner">
                            <img src="common/images/promo-banner6.png" />
                            <div class="promo_detail" style="display: none;">
                                <h1><?$translate->__('คืนโบนัสยอดเสีย คาสิโน 5%');?>!</h1>
                                <p><?$translate->__('สมาชิกสามารถขอรับสิทธิ์คืนยอดเสียได้ถึง 5 % จากยอดเดิมพันเสียทั้งหมด สูงสุดถึง 50,000 บาท');?>.</p>
                                <ol>
                                    <li><?$translate->__('สมาชิกสามารถแจ้งขอรับสิทธิ์คืนยอดได้เสียได้ทางเว็บไซด์');?>.</li>
                                    <li><?$translate->__('โบนัสนี้จะนับการเล่นตั้งแต่วันจันทร์จนถึงวันอาทิตย์ ถ้าทั้งอาทิตย์ลูกค้ามียอดการเล่นเป็นลบ สามารถแจ้งรับสิทธิ์จะได้ทันที');?>.</li>
                                    <li><?$translate->__('สมาชิกที่สามารถรับสิทธิ์โปรโมชั่นนี้ ต้องมียอดเดิมพันหมุนเวียน 4 เท่าของยอดเงินฝาก สามารถรับโบนัสได้สูงสุดถึง 50,000 บาท');?>.</li>
                                    <li><?$translate->__('ทีมงานขอสงวนสิทธิในการเปลี่ยนแปลงกฎติกาและของรางวัลโดยไม่ต้องแจ้งให้ทราบล่วงหน้า สมาชิกท่านใด มีเจตนาส่อไปในทางทุจริต ทาง เว็บ ขอตัดสิทธิ์ทุกกรณี');?>.</li>
                                </ol>
                                <div class="promo_group_button">
                                    <button class="btn btn-orange">Hide Promo Info</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div ng-show="tab.isSet(2)">
            <div class="popup-content">
                <div class="header-box"><?$translate->__('แจ้งเตือน');?></div>
                <div class="preview-box read-content">
                </div>
                <div class="header-row-box">
                    <div class="header-title width10 text-center"><?$translate->__('เบอร์');?></div>
                    <div class="header-title width64 text-center"><?$translate->__('หัวข้อ');?></div>
                    <div class="header-title width15 text-center"><?$translate->__('วันที่สร้าง');?></div>
                    <div class="header-title width10 text-center"><?$translate->__('ดู');?></div>
                    <div class="clear"></div>
                </div>


                <div id="notice-container">
                                        <table  cellpadding="0" cellspacing="0" width="100%" ng-controller="noticeController">
                                                                <tbody class="board-list" ng-init="getNotice()">
                                                                    <tr ng-repeat="noticeData in noticeDatas">
                                                                    <td class="row-col width10 text-center" ng-bind="noticeData.BoardCode"></td>
                                                                    <td class="row-col width64 text-left"  ng-bind="noticeData.Subject" ></td>
                                                                    <td class="row-col width15 text-center" ng-bind="noticeData.WriteDate"></td>
                                                                    <td class="row-col width10 text-center" ng-bind="noticeData.ViewCount"></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div ng-show="tab.isSet(3)">
            <div class="popup-content">
                <div class="header-box"><?$translate->__('กิจกรรม');?></div>
                <div class="preview-box read-content">
                </div>
                <div class="header-row-box">
                    <div class="header-title width10 text-center"><?$translate->__('เบอร์');?></div>
                    <div class="header-title width64 text-center"><?$translate->__('หัวข้อ');?></div>
                    <div class="header-title width15 text-center"><?$translate->__('วันที่สร้าง');?></div>
                    <div class="header-title width10 text-center"><?$translate->__('ดู');?></div>
                    <div class="clear"></div>
                </div>
<!--                <div id="event-container">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tbody class="board-list">
                        <tr class="">
                            <td class="row-col width10 text-center">4</td>
                            <td class="row-col width64 text-left" onclick="read_board('3','44','');"><?/*$translate->__('เหตุการณ์ตัวอย่าง');*/?> 1</td>
                            <td class="row-col width15 text-center">2014-09-08</td>
                            <td class="row-col width10 text-center">5</td>
                        </tr>
                        <tr>
                            <td class="row-col width10 text-center">3</td>
                            <td class="row-col width64 text-left" onclick="read_board('3','18','');"><?/*$translate->__('เหตุการณ์ตัวอย่าง');*/?> 2</td>
                            <td class="row-col width15 text-center">2014-07-23</td>
                            <td class="row-col width10 text-center">19</td>
                        </tr>
                        </tbody>
                    </table>
                </div>-->
                <div id="event-container">
                    <table cellpadding="0" cellspacing="0" width="100%" ng-controller="noticeController" >
                        <tbody class="board-list"  ng-init="getEvent()" >
                            <tr ng-repeat="eventData in eventDatas  | filter:{Type :3}" >
                            <td class="row-col width10 text-center" ng-bind="eventData.BoardCode"></td>
                            <td class="row-col width64 text-left"  ng-bind="eventData.Subject"></td>
                            <td class="row-col width15 text-center" ng-bind="eventData.WriteDate"></td>
                            <td class="row-col width10 text-center" ng-bind="eventData.ViewCount"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div ng-show="tab.isSet(4)">
            <div class="popup-content">
                <div class="header-box"><?$translate->__('คำถามที่พบบ่อย');?></div>
                <div class="preview-box read-content">
                </div>
                <div class="header-row-box">
                    <div class="header-title width10 text-center"><?$translate->__('เบอร์');?></div>
                    <div class="header-title width64 text-center"><?$translate->__('หัวข้อ');?></div>
                    <div class="header-title width15 text-center"><?$translate->__('วันที่สร้าง');?></div>
                    <div class="header-title width10 text-center"><?$translate->__('ดู');?></div>
                    <div class="clear"></div>
                </div>
                <div id="faq-container">
<!--                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tbody class="board-list">
                        <tr>
                            <td class="row-col width10 text-center">3</td>
                            <td class="row-col width64 text-left" onclick="read_board('2','40','');"><?/*$translate->__('ตัวอย่างคำถามที่พบบ่อย');*/?> 1</td>
                            <td class="row-col width15 text-center">2014-09-16</td>
                            <td class="row-col width10 text-center">18</td>
                        </tr>
                        <tr>
                            <td class="row-col width10 text-center">3</td>
                            <td class="row-col width64 text-left" onclick="read_board('2','40','');"><?/*$translate->__('ตัวอย่างคำถามที่พบบ่อย');*/?> 2</td>
                            <td class="row-col width15 text-center">2014-09-16</td>
                            <td class="row-col width10 text-center">18</td>
                        </tr>
                        </tbody>
                    </table>-->

                    <table cellpadding="0" cellspacing="0" width="100%" ng-controller="noticeController" >
                        <tbody class="board-list"  ng-init="getFaq()" >
                        <tr ng-repeat="faqData in faqDatas  | filter:{Type :2}" >
                            <td class="row-col width10 text-center" ng-bind="faqData.BoardCode"></td>
                            <td class="row-col width64 text-left"  ng-bind="faqData.Subject"></td>
                            <td class="row-col width15 text-center" ng-bind="faqData.WriteDate"></td>
                            <td class="row-col width10 text-center" ng-bind="faqData.ViewCount"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div ng-show="tab.isSet(5)">
            <div class="popup-content">
                <div class="header-box"><?$translate->__('บริการลูกค้า');?></div>
                <div class="header-row-box">
                    <div class="header-title width50 text-left"><span><p><?$translate->__('ยินดีต้อนรับสู่');?> UEFA168 !!</p></span></div>
                    <div class="header-title width20 text-right"><?$translate->__('ผู้เขียน');?> : <strong>UEFA168</strong></div>
                    <div class="header-title width20 text-right"><?$translate->__('เวลา');?> : <strong>2014-05-09</strong></div>
                    <div class="clear"></div>
                </div>

<!--                <div class="preview-box">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                </div>

                <div ng-controller="messageController as msgCtrl">
                    <div class="message-box">
                        <div class="admin-message">
                            <div class="message-name text-left"><strong><?/*$translate->__('ผู้ดูแลระบบ');*/?> - UEFA168</strong></div>
                            <div class="message-time text-right"><em>09:59 &nbsp; 2014-05-09</em></div>
                            <div class="clear"></div>

                            <div class="admin-message-box">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div class="user-message" ng-repeat="message in msgCtrl.reviews">
                            <div class="message-name text-left"><strong><?/*$translate->__('รหัสผู้ใช้');*/?></strong></div>
                            <div class="message-time text-right"><em>{{message.createdOn | date:'HH:mm MM-dd-yyyy'}}</em></div>
                            <div class="clear"></div>

                            <div class="user-message-box">
                                <p>{{message.body}}</p>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="header-title-box">
                        <form name="messageForm" ng-submit="messageForm.$valid && msgCtrl.addChatMsg(message)" novalidate>
                            <textarea rows="8" class="width99" id="textarea"
                                      ng-model="msgCtrl.message.body" placeholder="<?/*$translate->__('พิมพ์ข้อความตรงนี้');*/?>"></textarea>
                            <div class="text-count"><span id="textarea_feedback" class="input-textcount"></span><?/*$translate->__('เหลืออีกกี่ตัวอีกษร');*/?></div>
                            <div class="btn-send"><button class="btn btn-orange btn-send"><?/*$translate->__('ส่ง');*/?></button></div>
                            <div class="clear"></div>
                        </form>
                    </div>
                </div>-->

                <form id="qna-write-form">
                    <div class="row-box">
                        <label>Title</label>
                        <p><input name="Subject" type="text" value="" class="text-left"></p>
                        <div class="clear"></div>
                    </div>
                    <div class="row-box highlight">
                        <label>Description</label>
                        <p><textarea name="Contents" rows="4" cols="20" style="width:520px; height: 185px;" placeholder="Text is limited to 1000 characters."></textarea></p>
                        <div class="clear"></div>
                    </div>
                </form>
                <div class="row-box-last text-center">
                    <button class="btn btn-orange" value="" onclick="write_board('#qna-write-form','4',this)"><?$translate->__('สมบูรณ์');?></button>
                </div>

                <div class="preview-box"></div>

                <div class="header-row-box">
                    <div class="header-title width10 text-center"><?$translate->__('เบอร์');?></div>
                    <div class="header-title width70 text-center"><?$translate->__('หัวข้อ');?></div>
                    <div class="header-title width10 text-center"><?$translate->__('วันที่สร้าง');?></div>
                    <div class="header-title width10 text-center"><?$translate->__('ดู');?></div>
                    <div class="clear"></div>
                </div>

<!--                <div id="notice-container">-->
                    <table cellpadding="0" cellspacing="0" width="100%" ng-controller="noticeController" >
                        <tbody class="board-list"  ng-init="getCustomer()" >
                        <tr ng-repeat="customerData in customerDatas  | filter:{Type :4}" >
                            <td class="row-col width10 text-center" ng-bind="customerData.BoardCode"></td>
                            <td class="row-col width64 text-left"  ng-bind="customerData.Subject"></td>
                            <td class="row-col width15 text-center" ng-bind="customerData.WriteDate"></td>
                            <td class="row-col width10 text-center" ng-bind="customerData.ViewCount"></td>
                        </tr>
                        </tbody>
                    </table>

                    <div class="pagination-container">
                        <div class="pagination pagination-board">
                            <div class="my-navigation">
                                <span class="simple-pagination-previous"></span>
                                <span class="simple-pagination-page-numbers"></span>
                                <span class="simple-pagination-next"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
<!--            </div>-->
        </div>
        <div ng-show="tab.isSet(6)">
            <div class="popup-content">
                <div class="header-box"><?$translate->__('พันธมิตร');?></div>
                <div class="row-box highlight">
                    <p><?$translate->__('ตำราตัวอย่างที่นี่');?>...</p>
                    <div class="clear"></div>
                </div>
                <form id="partner-write-form">
                    <div class="row-box">
                        <label><?$translate->__('หัวข้อ');?></label>
                        <p><input name="Subject" type="text" value="" class="text-left"></p>
                        <div class="clear"></div>
                    </div>
                    <div class="row-box highlight">
                        <label><?$translate->__('ข้อความ');?></label>
                        <p><textarea name="Contents" rows="4" cols="20" style="width:520px; height: 185px;" placeholder="<?$translate->__('ไม่เกิน 1000 ตัวอักษร');?>."></textarea></p>
                        <div class="clear"></div>
                    </div>
                </form>
                <div class="row-box-last text-center">
                    <button class="btn btn-orange" onclick="write_board('#partner-write-form','5',this)" ><?$translate->__('ตกลง');?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="popup-terms">
    <span class="btn-popup-close b-close"></span>
    <!--<h1 class="logo"></h1>-->

    <h2>ข้อตกลงและเงื่อนไข</h2>
    <h4>PLEASE READ CAREFULLY THE FOLLOWING LEGALLY BINDING AGREEMENT BETWEEN UEFA168.com AND YOU.<br />
        MAKE SURE YOU FULLY UNDERSTAND ITS CONTENTS. IF YOU HAVE ANY DOUBTS ABOUT YOUR RIGHTS AND OBLIGATIONS RESULTING FROM THE ACCEPTANCE OF THIS AGREEMENT, <br />PLEASE CONSULT A LEGAL ATTORNEY IN YOUR JURISDICTION.</h4>

    <div class="terms-content">
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
    </div>
</div>

<div class="header-container">
    <div class="container">
        <div class="header-top">

            <div class="jackpot" onclick="openSlotMicro();">
                <p class="title-jackpot"></p>
                <p>฿</p>
                <p class="super-jackpot"></p>
            </div>

            <div class="login-container">
                <?if(!isset($_SESSION['MemberID'])){?>
                    <p><button class="btn btn-gray btn-demo"><?$translate->__('ดูการสาธิต');?></button></p>
                    <p><a href="#" class="link-forgotpassword"><?$translate->__('ลืมรหัสผ่าน');?>?</a></p>
                    <p><button class="btn btn-orange btn-signup"><?$translate->__('ลงทะเบียนที่นี่!');?></button></p>
                <form id="login-form">
                   <!-- <div id="guest" >-->
                        <p><input type="text" name="MemberID" placeholder="<?$translate->__('รหัสผู้ใช้');?>"></p>
                        <p><input type="password" name="MemberPwd" placeholder="<?$translate->__('รหัสผ่าน');?>"></p>
                        <p><button class="btn btn-gray btn-login" onclick="login();"><?$translate->__('เข้าสู่ระบบ');?></button></p>
                        <input type="hidden" id="hiddenLogin" value="LOGIN">
                        <input type="hidden" id="hiddenLang" value="<?php echo $_GET['lang']; ?>">
                    <!--</div>-->
                </form>
                <?}else{?>
  <!--              <div id="user">-->
                    <p><span><?$translate->__('ยินดีต้อนรับ');?>, <strong><?php echo $_SESSION['MemberID']; ?>  <?php echo $_SESSION['MemberPwd']; ?></strong>!</span></p>
                    <p class="box-bal">฿ <strong class="txtAmount all-balance"></strong></p>
                    <p><button class="btn btn-orange btn-wallet"><?$translate->__('กระเป๋าสตางค์');?></button></p>
                    <p><button class="btn btn-gray btn-logout" onclick="logout();" ><?$translate->__('ออกจากระบบ');?></button></p>
            <!--    </div>-->
                <?}?>
                <div class="lang-option">
                    <div id="lang-active">
                        <?if($_GET['lang']=="english"){
                            echo '<i class="icon-lang icon-eng"></i>';
                        }else if($_GET['lang']=="chinese"){
                            echo '<i class="icon-lang icon-chi"></i>';
                        }else{
                            echo '<i class="icon-lang icon-thai"></i>';
                        } ?>
                        <span class="rotate-triangle2"></span>
                    </div>
                    <div id="lang-list">
                        <ul>
                            <li onclick="location.href='/'"><i class="icon-lang icon-thai"></i> ไทย</li>
                            <li onclick="location.href='index.php?lang=english'"><i class="icon-lang icon-eng"></i>ENGLISH</li>
                            <li onclick="location.href='index.php?lang=chinese'"><i class="icon-lang icon-chi"></i> 中國語</li>
                        </ul>
                    </div>
                </div>
                <div class="line-separator"></div>
            </div>



        </div>
        <div class="header-bottom">
            <div class="line-separator"></div>
            <div class="logo" onclick="location.href='/'"></div>

            <div id="nav">
                <div class="nav-items clear">
                    <ul>
                        <li class="item_mobile" id="nav_mobile">
                            <span class="txt"><i class="icon-mobile"></i></span>
                            <span class="rotate-triangle"></span>
                        </li>
                        <li class="item_sports" id="nav_sports" >
                            <span class="txt"><?$translate->__('กีฬา');?></span>
                            <span class="rotate-triangle"></span>
                        </li>
                        <li class="item_casino" id="nav_casino">
                            <span class="txt"><?$translate->__('คาสิโน');?></span>
                            <span class="rotate-triangle"></span>
                        </li>

                        <li class="item_slots" id="nav_slots">
                            <span class="txt"><?$translate->__('เกมส์สลอส');?></span>
                            <span class="rotate-triangle"></span>
                        </li>
                        <li class="item_othergames" id="nav_othergames">
                            <span class="txt"><?$translate->__('เกมอื่น ๆ');?></span>
                            <span class="rotate-triangle"></span>
                        </li>
                        <li class="item"
                            <?php if(isset($_SESSION['MemberID'])){
                                echo 'id="nav_promo" onclick="$(\'#customerPromos\').click();"';
                            }else{
                                echo 'onclick="popupOpenLogin();"';
                            }?>
                             >
                            <span class="icon-nav icon-promo"></span>
                            <span class="txt-nav"><?$translate->__('โปรโมชั่น');?></span>
                        </li>
                        <li class="item" <?php if(isset($_SESSION['MemberID'])){
                                echo 'id="nav_deposit" onclick="$(\'#walletDeposit\').click();"';
                             }else{
                                echo 'onclick="popupOpenLogin();"';
                            }?>  >
                            <span class="icon-nav icon-deposit"></span>
                            <span class="txt-nav"><?$translate->__('ฝาก');?></span>
                        </li>
                        <li class="item" <?php if(isset($_SESSION['MemberID'])){
                            echo 'id="nav_withdraw" onclick="$(\'#walletWithdraw\').click();"';
                        }else{
                            echo 'onclick="popupOpenLogin();"';
                        }?>  >
                            <span class="icon-nav icon-withdraw"></span>
                            <span class="txt-nav"><?$translate->__('ถอน');?></span>
                        </li>

                        <li class="item"
                            <?php if(isset($_SESSION['MemberID'])){
                                echo 'id="nav_customer" onclick="$(\'#customer1on1\').click();"';
                            }else{
                                echo 'onclick="popupOpenLogin();"';
                            }?>
                            >
                            <span class="icon-nav icon-customer"></span>
                            <span class="txt-nav"><?$translate->__('ลูกค้า');?></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="customer-panel">
    <div class="customer-panel-handle">
        <p class="customer-panel-arrow-left"></p>
    </div>

    <div class="panel-content panel-customer">
        <h1><?$translate->__('บริการลูกค้า');?></h1>
        <p><img src="common/images/247memberservice.png" /></p>

        <?if(!isset($_SESSION['MemberID'])){?>
        <button class="btn btn-orange btn-signup"><?$translate->__('สมัครสมาชิกตอนนี้');?>!</button>
        <?}?>

        <div class="btn-livechat" onclick="LC_API.open_chat_window();return false;" >
            <?$translate->__('ห้องสนทนาสด');?>
            <strong><?$translate->__('กด ที่นี้');?></strong>
        </div>
        <div class="customer-details">
            <div class="customer-icon-hotline">
                <?$translate->__('สายด่วน');?>
                <strong>1234-5678-9000</strong>
            </div>
            <div class="customer-icon-lineID">
                <?$translate->__('ไลน์');?>
                <strong>Uefa168LINE</strong>
            </div>
            <div class="customer-icon-skypeID">
                <?$translate->__('สไกด์');?> ID
                <strong>Uefa168SKYPE</strong>
            </div>
            <div class="customer-icon-wechatID">
                <?$translate->__('วีแชต');?> ID
                <strong>Uefa168WECHAT</strong>
            </div>
            <div class="customer-icon-email">
                <?$translate->__('อีเมล');?>
                <strong><a href="mailto:info@uefa168.com">info@uefa168.com</a></strong>
            </div>
        </div>
    </div>
</div>

<div id="wallet-panel">
    <div class="wallet-panel-handle">
        <p class="wallet-panel-arrow-right"></p>
    </div>

    <div class="panel-content my-wallet">
        <h1><?$translate->__('กระเป๋าสตางค์');?><!--<span class="btn btn-refresh">REFRESH</span>--></h1>
        <?if(isset($_SESSION['MemberID'])){?>
        <!--<div class="my-wallet-locked">
            <i class="icon-lock"></i>
            Please login first to access your wallet panel.
        </div>-->
            <!--uncomment for coming soon-->
<!--        <div class="itemWallet">
            <p class="text-right">AFB Sports</p>
            <span><em>฿</em> <strong>1,425,233</strong></span>
        </div>-->
<!--        <div class="itemWallet">
            <p class="text-right"><?/*$translate->__('ผลการแข่งขัน');*/?></p>
            <span><em>฿</em> <strong>3,425,233</strong></span>
        </div>-->
        <!--        <div class="itemWallet">
            <p class="text-right">Playtech</p>
            <span><em>฿</em> <strong>121,235,353</strong></span>
        </div>-->
<!--<div class="itemWallet">
            <p class="text-right">Lotto</p>
            <span><em>฿</em> <strong>121,235,353</strong></span>
        </div>
        <div class="itemWallet">
            <p class="text-right">Stock</p>
            <span><em>฿</em> <strong>121,235,353</strong></span>
        </div>
        <div class="itemWallet">
            <p class="text-right">Sadari</p>
            <span><em>฿</em> <strong>121,235,353</strong></span>
        </div>-->
        <div class="itemWallet">
            <p class="text-right">WFT Sports</p>
            <span><em>฿</em> <strong class="wft-balance balance" ></strong ></span>
        </div>

        <div class="itemWallet">
            <p class="text-right">ASC Sports</p>
            <span><em>฿</em><strong class="asc-balance balance"></strong></span>
        </div>

        <div class="itemWallet">
            <p class="text-right">Microgaming</p>
            <span><em>฿</em><strong class="mg-balance balance"></strong></span>
        </div>

        <div class="itemWallet">
            <p class="text-right">Gameplay</p>
            <span><em>฿</em><strong class="opus-balance balance"></strong></span>
        </div>
        <div class="itemWallet">
            <p class="text-right">Asia Gaming</p>
            <span><em>฿</em><strong class="ag-balance balance"></strong>
            </span>
        </div>

        <div class="itemWallet">
            <p class="text-right">Gold Deluxe</p>
            <span><em>฿</em><strong class="gd-balance balance"></strong></span>
        </div>

        <div class="itemWallet">
            <p class="text-right">Ezugi</p>
            <span><em>฿</em><strong class="ezugi-balance balance"></strong></span>
        </div>

        <div class="itemWallet">
            <p class="text-right">Betsoft</p>
            <span><em>฿</em><strong class="bet-balance balance"></strong></span>
        </div>

        <div class="itemWallet total">
            <p class="text-right"><?$translate->__('ทั้งสิ้น');?></p>
            <span><em>฿</em><strong class="all-balance balance"></strong></span>
            <div class="clear"></div>
        </div>

        <form id="transfer-form-sidebar" role="form">
        <h1><?$translate->__('การโอนเงิน');?></h1>
        <div class="itemWallet" ng-controller="walletController">
            <p class="text-right"><?$translate->__('จาก');?></p>
                <span>
                    <select name="FromWallet">
                        <option><?$translate->__('เลือกเกม');?></option>
<!--                                <option value="">AFB Sports</option>
                                    <option value="">Playtech</option>
                                    <option value=""><?/*$translate->__('ผลการแข่งขัน');*/?></option>-->
                        <? foreach ($variables['gameText'] as $k => $v) { ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                        <? } ?>
                    </select>
                </span>
            <div class="clear"></div>
        </div>
        <div class="itemWallet">
            <p class="text-right"><?$translate->__('จำนวน');?></p>
            <span><em>฿</em> <input type="text" name="Amount" placeholder="0" /></span>
            <div class="clear"></div>
        </div>
        <div class="itemWallet">
            <p class="text-right"><?$translate->__('ถึง');?></p>
                <span>
                    <select name="ToWallet">
                        <option><?$translate->__('เลือกเกม');?></option>
<!--                                <option value="">AFB Sports</option>
                                    <option value="">Playtech</option>
                                    <option value=""><?/*$translate->__('ผลการแข่งขัน');*/?></option>-->
                        <? foreach ($variables['gameText'] as $k => $v) { ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                        <? } ?>
                    </select>
                </span>
            <div class="clear"></div>
        </div>
        <div class="btn-moneytransfer">
            <button class="btn btn-gray" onclick="moneyProcess('#transfer-form-sidebar',this);" ><?$translate->__('โอน');?></button>
        </div>
        </form>




        <p class="text-center">
            <button class="btn btn-gold btn-deposit" onclick="$('#walletDeposit').click();"><i class="icon-btn-deposit"></i>
                <?$translate->__('ฝาก');?>
            </button>

            <button class="btn btn-gold btn-withdraw" onclick="$('#walletWithdraw').click();"> <i class="icon-btn-withdraw"></i>
                <?$translate->__('ถอน');?>
            </button>
        </p>

        <?}else{?>

        <div class="my-wallet-locked">
            <i class="icon-lock"></i>
            <?$translate->__('กรุณาเข้าสู่ระบบในการเข้าถึงแผงกระเป๋าสตางค์ของคุณ');?>.
        </div>
        <?}?>

    </div>
</div>

<div class="nav_hover_wrapper nav_hover_mobile">
    <div class="nav_hover_container" id="nav_hover_cnt_mobile">
        <ul>
            <li class="gamebutton-mobile gamebutton-mobile1">
                <h2><?$translate->__('เร็ว ๆ นี้');?>!</h2>
                <!--<button class="btn btn-orange" onclick="openSport();">เล่นตอนนี้!</button>-->
            </li>
            <!--<li class="gamebutton-mobile gamebutton-mobile2">
                <h2>Mobile 2</h2>
                <button class="btn btn-orange" onclick="openSport();">เล่นตอนนี้!</button>
            </li>
            <li class="gamebutton-mobile gamebutton-mobile3">
                <h2>Mobile 3</h2>
                <button class="btn btn-orange" onclick="openSport();">เล่นตอนนี้!</button>
            </li>
            <li class="gamebutton-mobile gamebutton-mobile4">
                <h2>Mobile 4</h2>
                <button class="btn btn-orange" onclick="openSport();">เล่นตอนนี้!</button>
            </li>-->
        </ul>
    </div>
</div>
<div class="nav_hover_wrapper nav_hover_sports">
    <div class="nav_hover_container" id="nav_hover_cnt_sports">
        <ul>
            <li class="gamebutton-sports gamebutton-sports1" >
                <h2>AFB Sports</h2>
                <button class="btn btn-orange"
                    <?php if($_GET['lang']=='english'){
                        echo 'onclick="javascript:alert(\'Coming Soon!\');"';
                    }else{
                        echo 'onclick="javascript:alert(\'เร็ว ๆ นี้!\');"';
                    } ?>
                    ><?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
            <li class="gamebutton-sports gamebutton-sports2">
                <h2>WFT Sports</h2>
                <button class="btn btn-orange"
                        <?if(isset($_SESSION['MemberID'])){
                            echo 'onclick="openSportWFT();"';
                        }else{
                            echo 'onclick="popupOpenLogin();"';
                        } ?>
                         ><?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
            <li class="gamebutton-sports gamebutton-sports3">
                <h2>ASC Sports</h2>
                <button class="btn btn-orange"
                        <?if(isset($_SESSION['MemberID'])){
                            echo 'onclick="openSportASC();"';
                        }else{
                            echo 'onclick="popupOpenLogin();"';
                        } ?>
                         ><?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
            <li class="gamebutton-sports gamebutton-sports4">
                <h2><?$translate->__('ผลการแข่งขัน');?> (Live Score Feed)</h2>
                <button class="btn btn-orange"
                    <?php if($_GET['lang']=='english'){
                        echo 'onclick="javascript:alert(\'Coming Soon!\');"';
                    }else{
                        echo 'onclick="javascript:alert(\'เร็ว ๆ นี้!\');"';
                    } ?>
                    ><?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
        </ul>
    </div>
</div>
<div class="nav_hover_wrapper nav_hover_casino">
    <div class="nav_hover_container" id="nav_hover_cnt_casino">
        <ul>
            <li class="gamebutton-casino gamebutton-casino1" >
                <h2>Microgaming</h2>
                <button class="btn btn-orange"
                    <?if(isset($_SESSION['MemberID'])){
                        echo 'onclick="playGame(\'1005\',\'live\',\'\',\'\',\'\');"';
                    }?>
                    >
                    <?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
            <li class="gamebutton-casino gamebutton-casino2">
                <h2>Gameplay</h2>
                <button class="btn btn-orange"
                    <?if(isset($_SESSION['MemberID'])){
                        echo 'onclick="playGame(\'1000\',\'live\',\'\',\'\',\'\');"';
                    }?>
                    >
                    <?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
            <li class="gamebutton-casino gamebutton-casino3">
                <h2>Asia Gaming</h2>
                <button class="btn btn-orange"
                    <?if(isset($_SESSION['MemberID'])){
                        echo 'onclick="playGame(\'1012\',\'live\',\'\',\'\',\'\');"';
                    }?>
                    >
                    <?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
            <li class="gamebutton-casino gamebutton-casino4">
                <h2>Gold Deluxe</h2>
                <button class="btn btn-orange"
                    <?if(isset($_SESSION['MemberID'])){
                        echo 'onclick="playGame(\'1002\',\'live\',\'\',\'\',\'\');"';
                    }?>
                    >
                    <?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
            <li class="gamebutton-casino gamebutton-casino5">
                <h2>Ezugi Gaming</h2>
                <button class="btn btn-orange"
                    <?if(isset($_SESSION['MemberID'])){
                        echo 'onclick="playGame(\'1014\',\'live\',\'\',\'\',\'\');"';
                    }?>
                    >
                    <?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
        </ul>
    </div>
</div>
<div class="nav_hover_wrapper nav_hover_slots">
    <div class="nav_hover_container" id="nav_hover_cnt_slots">
        <ul>
            <li class="gamebutton-slots gamebutton-slots1">
                <h3><?$translate->__('แจ๊กพ๊อต');?> ฿ <span>425,230,000.00</span></h3>
                <h2>Microgaming</h2>
                <button class="btn btn-orange"
                    <?if(isset($_SESSION['MemberID'])){
                        echo 'onclick="openSlotMicro();"';
                    }else{
                        //echo 'onclick="popupOpenLogin();"';
                        echo 'onclick="openSlotMicro();"';
                    } ?>

                    ><?$translate->__('เล่นตอนนี้');?>!</button>

            <li class="gamebutton-slots gamebutton-slots2">
                <h2>Gameplay</h2>
                <button class="btn btn-orange"
                        <?if(isset($_SESSION['MemberID'])){
                            echo 'onclick="openSlotGamePlay();"';
                        }else{
                            //echo 'onclick="popupOpenLogin();"';
                            echo 'onclick="openSlotGamePlay();"'; 
                        } ?>
                         ><?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
            <li class="gamebutton-slots gamebutton-slots3">
                <h3><?$translate->__('แจ๊กพ๊อต');?> ฿ <span>425,230,000.00</span></h3>
                <h2>Playtech</h2>
                <button class="btn btn-orange"
                    <?php if($_GET['lang']=='english'){
                        echo 'onclick="javascript:alert(\'Coming Soon!\');"';
                    }else{
                        echo 'onclick="javascript:alert(\'เร็ว ๆ นี้!\');"';
                    } ?>
                    ><?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
            <li class="gamebutton-slots gamebutton-slots4">
                <h2>Betsoft</h2>
                <button class="btn btn-orange"
                        <?if(isset($_SESSION['MemberID'])){
                            echo 'onclick="openSlotBet();"';
                        }else{
                            //echo 'onclick="popupOpenLogin();"';
                            echo 'onclick="openSlotBet();"';
                        } ?>
                        ><?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
        </ul>
    </div>
</div>
<div class="nav_hover_wrapper nav_hover_othergames">
    <div class="nav_hover_container" id="nav_hover_cnt_othergames">
        <ul>
            <li class="gamebutton-othergames gamebutton-othergames1">
                <h2><?$translate->__('เกมพนันชนิดหนึ่ง');?></h2>
                <button class="btn btn-orange"><?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
            <li class="gamebutton-othergames gamebutton-othergames2">
                <h2><?$translate->__('สต็อกสินค้า');?></h2>
                <button class="btn btn-orange"><?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
            <li class="gamebutton-othergames gamebutton-othergames3">
                <h2>Sadari</h2>
                <button class="btn btn-orange"><?$translate->__('เล่นตอนนี้');?>!</button>
            </li>
        </ul>
    </div>
</div>

<div class="wrapper">
    <div class="content-container main-wrapper">
        <div class="container">
            <!-- Main-Container -->
            <div class="main-container">
                <div class="gamebuttons-main">
                    <ul>
                        <li class="gamebutton-main-casino">
                            <strong><?$translate->__('คาสิโน');?></strong>
                            <span>Casino Games</span>
                            <button class="btn btn-orange"
                                <?if(isset($_SESSION['MemberID'])){
                                    echo 'onclick="playGame(\'1005\',\'live\',\'\',\'\',\'\');"';
                                }else{
                                    echo 'onclick="popupOpenLogin();"';
                                } ?>
                                ><?$translate->__('เล่นตอนนี้');?>!</button>
                        </li>
                        <li class="gamebutton-main-sports">
                            <strong><?$translate->__('กีฬา');?></strong>
                            <span>Sports Games</span>
                            <button class="btn btn-orange"
                                <?if(isset($_SESSION['MemberID'])){
                                    echo 'onclick="openSportASC();"';
                                }else{
                                    echo 'onclick="popupOpenLogin();"';
                                } ?>
                                ><?$translate->__('เล่นตอนนี้');?>!</button>
                        </li>
                        <li class="gamebutton-main-slots">
                            <strong><?$translate->__('สลอส');?></strong>
                            <span>Slot Games</span>
                            <button class="btn btn-orange"
                                <?if(isset($_SESSION['MemberID'])){
                                    echo 'onclick="openSlotMicro();"';
                                }else{
                                    echo 'onclick="popupOpenLogin();"';
                                } ?>
                                ><?$translate->__('เล่นตอนนี้');?>!</button>
                        </li>
                        <li class="gamebutton-main-keno">
                            <strong><?$translate->__('คีโน');?></strong>
                            <span>Keno Games</span>
                            <button class="btn btn-orange"
                                <?if(isset($_SESSION['MemberID'])){
                                    echo 'onclick="openSlotBet();"';
                                }else{
                                    echo 'onclick="popupOpenLogin();"';
                                } ?>


                                ><?$translate->__('เล่นตอนนี้');?>!</button>
                        </li>
                    </ul>
                </div>

                <div class="notice-container">
                    <div class="notice-tabs">
                        <h1><?$translate->__('ประกาศ');?> <a href="#" class="btn-more-notice" onclick="$('#customerNotice').click();"><?$translate->__('อื่นๆ');?></a></h1>
                        <div class="notice-tabs-content notice-announcements"  ng-controller="noticeController">
                            <div ng-init="getNotice()">
                                <p ng-repeat="noticeData in noticeDatas | limitTo: 8">
                                    <input type="hidden" ng-value='noticeData.BoardCode'>
                                    <span ng-bind="noticeData.WriteDate | limitTo:10"></span>
                                    <strong ng-bind="noticeData.Subject"
                                            <?if(isset($_SESSION['MemberID'])){
                                                echo 'onclick="indexNotice(1,this)"';
                                            }else{
                                                echo 'onclick="popupOpenLogin();"';
                                            } ?>
                                            class="notice-title"></strong>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="notice-tabs">
                        <h1><?$translate->__('ยอด 8 ถอนเงิน');?></h1>
                        <div ng-controller="transactionController" ng-init="getTransaction()">
                            <div class="notice-tabs-content">
                                <p ng-repeat="transaction in realTimeTransactions | filter:{Type :2} | limitTo:8">
                                    <span ng-bind="transaction.Date | limitTo:16"></span>
                                    <strong ng-bind="transaction.Amount" style="padding-left:10px; text-align: left;"></strong>
                                    <em ng-bind="transaction.Member_id"></em>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="notice-tabs">
                        <h1><?$translate->__('ยอด 8 ฝาก');?></h1>
                        <div ng-controller="transactionController" ng-init="getTransaction()">
                            <div class="notice-tabs-content">
                                <p ng-repeat="transaction in realTimeTransactions | filter:{Type :1} | limitTo:8">
                                    <span ng-bind="transaction.Date | limitTo:16"></span>
                                    <strong ng-bind="transaction.Amount" style="padding-left:10px; text-align: left;"></strong>
                                    <em ng-bind="transaction.Member_id"></em>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <!-- Sports-Container -->
            <div class="sports-container">
                <div class="content-sports-frame">
                    <div class="sport-iframe" style="margin: 0 auto; width: 1024px; height: 800px; background: #000000;">
                        <iframe name="sport-iframe" class="sport-iframe" width="100%" height="100%" style="display: none"></iframe>
                        <iframe name="sportFrame" class="sportFrame" width="100%" height="100%"  style="display: none"></iframe>
                    </div>
                </div>
            </div>

            <!-- Slots-Container -->
            <div class="slots-container">
                <div class="banner-slot slot-microgaming">
                    <button class="btn btn-orange" onclick="playGame('1005','slot','TombRaider','2d');"><?$translate->__('เล่นตอนนี้');?>!</button>
                    <p><?$translate->__('ช่วยลาร่า ครอฟท์ตามหาดาบที่หายไป และรับโบนัสระหว่างการผจญภัยสุดมันส์');?>์</p>
                    <div><img src="common/images/banner-slot-microgaming.png" /></div>
                </div>

                <div class="banner-slot slot-gameplay">
                    <button class="btn btn-orange" onclick="playGame('1000','slot','godoffortune_ld','3d');"onclick="openSlotGamePlay();"><?$translate->__('เล่นตอนนี้');?>!</button>
                    <p><?$translate->__('ช่วยลาร่า ครอฟท์ตามหาดาบที่หายไป และรับโบนัสระหว่างการผจญภัยสุดมันส');?>.</p>
                    <div><img src="common/images/banner-slot-gameplay.png" /></div>
                </div>

                <div class="banner-slot slot-playtech">
                    <button class="btn btn-orange"><?$translate->__('เล่นตอนนี้');?>!</button>
                    <p><?$translate->__('ช่วยลาร่า ครอฟท์ตามหาดาบที่หายไป และรับโบนัสระหว่างการผจญภัยสุดมันส');?>.</p>
                    <div><img src="common/images/banner-slot-playtech.png" /></div>
                </div>

                <div class="banner-slot slot-betsoft">
                    <button class="btn btn-orange" onclick="playGame('1004','slot','426','2d');"><?$translate->__('เล่นตอนนี้');?>!</button>
                    <p><?$translate->__('ช่วยลาร่า ครอฟท์ตามหาดาบที่หายไป และรับโบนัสระหว่างการผจญภัยสุดมันส');?>.</p>
                    <div><img src="common/images/banner-slot-betsoft.png" /></div>
                </div>

                <div class="slot-sub-nav">
                    <div class="slot-close"></div>

                    <div class="slot-tabs-content slot1">
                        <div class="tab-slots-sub">
                            <ul>
<!--                            <li  onclick="loadSlot('1005','TOP25',this);"><?/*$translate->__('25 อันดับสูงสุด');*/?></li>-->
                                <li class="tab-slot1" onclick="loadSlot('1005','Advanced_Slot',this);"><?php $translate->__('สล้อตล่วงหน้า'); ?></li>
                                <li onclick="loadSlot('1005','Bonus_Slot',this);"><?php $translate->__('สล้อตเพิ่ม'); ?></li>
                                <li onclick="loadSlot('1005','Feature_Slot',this);"><?php $translate->__('ลักษณะสลอต'); ?></li>
                                <li onclick="loadSlot('1005','Video_Slot',this);"><?php $translate->__('วิดีโอสล้อต'); ?></li>
                                <li onclick="loadSlot('1005','Slot',this);"><?php $translate->__('สล็อต'); ?></li>
                                <li onclick="loadSlot('1005','Casual_Game',this);"><?php $translate->__('ไม่เป็นทางการ'); ?></li>
                                <li onclick="loadSlot('1005','Scratch_Card',this);"><?php $translate->__('บัตร สเเคร้ชท'); ?></li>
                                <li onclick="loadSlot('1005','Video_Poker',this);"><?php $translate->__('วิดีโอโป้กเกอร์'); ?></li>
                                <li onclick="loadSlot('1005','Table_Games',this);"><?php $translate->__('เกมตาราง'); ?></li>
                            </ul>
                        </div>
                            <div id="1005-slider" class="liquid-slider"></div>
                    </div>

                    <div class="slot-tabs-content slot2">
                        <div class="tab-slots-sub">
                                <ul>
  <!--                                  <li onclick="loadSlot('1000','TOP25',this);"><?/*$translate->__('25 อันดับสูงสุด');*/?></li>-->
                                    <li class="tab-slot2" onclick="loadSlot('1000','Slots_3d',this);"> 3D <?php $translate->__('Slot and Card'); ?></li>
                                    <li onclick="loadSlot('1000','Slots',this);"><?php $translate->__('Slots'); ?></li>
                                    <li onclick="loadSlot('1000','Arcades',this);"><?php $translate->__('Arcade'); ?></li>
                                    <li onclick="loadSlot('1000','Card_Games',this);"><?php $translate->__('Card Game'); ?></li>
                                    <li onclick="loadSlot('1000','Table_Games',this);"><?php $translate->__('Table Game'); ?></li>
                                    <li onclick="loadSlot('1000','Video_Poker',this);"><?php $translate->__('Video Poker'); ?></li>
                                </ul>
                        </div>
                        <div id="1000-slider" class="liquid-slider"></div>
                    </div>

                        <div class="slot-tabs-content slot3">
                            <div class="tab-slots-sub">
                                    <ul>
<!--                                        <li  onclick="loadSlot('1000','TOP25',this);"><?/*$translate->__('25 อันดับสูงสุด');*/?></li>-->
                                        <li class="tab-slot3" onClick="loadSlot('1004','Slots',this);"><?php $translate->__('Slots'); ?></li>
                                        <li onClick="loadSlot('1004','Soft_Games',this);"><?php $translate->__('Soft Game'); ?></li>
                                        <li onClick="loadSlot('1004','Keno',this);"><?php $translate->__('Keno'); ?></li>
                                        <li onClick="loadSlot('1004','Table_Games',this);"><?php $translate->__('Table Game'); ?></li>
                                        <li onClick="loadSlot('1004','Video_Poker',this);"><?php $translate->__('Video Poker'); ?></li>
                                        <li onClick="loadSlot('1004','Multihand_Poker',this);"><?php $translate->__('Multi-hand Poker'); ?></li>
                                        <li onClick="loadSlot('1004','Pyramid_Poker',this);"><?php $translate->__('Pyramid Poker'); ?></li>
                                    </ul>
                                </div>
                            <div id="1004-slider" class="liquid-slider"></div>
                        </div>

                    </div><!--slot-sub-nav-->

            </div>

            <!-- SignUp-Container -->
            <div class="signup-container" >
                <div class="banner-signup">
                    <p><?$translate->__('สมัครสมาชิกวันนี้ และสนุกกับเกมหลากหลาย');?>!</p>
                    <div><img src="common/images/banner-signup.png" /></div>
                </div>

                <div class="signup-form" >
                    <h1><?$translate->__('สร้างบัญชีผู้ใช้');?></h1>
                    <form id="sign-up-form" name="signUpForm" ng-submit="signUp()" novalidate>
                        <div class="row-box highlight">
                            <label><strong class="required">*</strong> <?$translate->__('รหัสผู้ใช้');?></label>
                            <p><input onkeyup="checkid(this);" type="text"
                                      name="MemberID" required />
                            </p>
                            <label class="msg" id="msg_id">4-16 (a-z, 0-9) <?php $translate->__('ตัวอักษร'); ?>.</label>
                            <!--<span ng-show="signUpForm.MemberID.$valid" class="valid"><?//$translate->__('รหัสผู้ใช้สามารถใช้ได้');?>!</span>
                            <span ng-show="signUpForm.MemberID.$error.required && signUpForm.MemberID.$dirty" class="error"><?//$translate->__('ฟิลด์นี้จะต้อง');?></span>
                            <span ng-show="signUpForm.MemberID.$error.hasID" class="error"><?//$translate->__('รหัสที่มีอยู่แล้วในการใช้งาน');?>.</span>
                            <span ng-show="!signUpForm.MemberID.$error.required && (signUpForm.MemberID.$error.minlength || signUpForm.MemberID.$error.maxlength) && signUpForm.MemberID.$dirty" class="error">4-16 (a-z, 0-9) <?//$translate->__('ตัวอักษรเท่านั้น');?></span>-->
                            <div class="clear"></div>
                        </div>
                        <div class="row-box">
                            <label><strong class="required">*</strong> <?$translate->__('รหัสผ่าน');?></label>
                            <p><input type="password"
                                      id="MemberPwd"
                                      name="MemberPwd"
                                      ng-model="signupFormData.MemberPwd"
                                      ng-minlength="6"
                                      ng-maxlength="16"
                                      required />
                            </p>
                            <label class="msg" id="msg_id">6 - 16 <?$translate->__('ตัวอักษร');?></label>
                            <span ng-show="signUpForm.MemberPwd.$error.required && signUpForm.MemberPwd.$dirty" class="error"><?$translate->__('กรุณาใส่รหัสผ่าน');?></span>
                            <span ng-show="!signUpForm.MemberPwd.$error.required && (signUpForm.MemberPwd.$error.minlength || signUpForm.MemberPwd.$error.maxlength) && signUpForm.MemberPwd.$dirty" class="error">6 - 16 <?$translate->__('ตัวอักษรเท่านั้น');?></span>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box highlight">
                            <label><strong class="required">*</strong> <?$translate->__('ยืนยันรหัสผ่าน');?></label>
                            <p><input type="password"
                                      id="MemberValidPwd"
                                      name="MemberValidPwd"
                                      ng-model="signupFormData.MemberValidPwd"
                                      valid-password-c
                                      required />
                            </p>
                            <label class="msg" id="msg_id"><?$translate->__('พิมพ์รหัสผ่าน');?></label>
                            <span ng-show="signUpForm.MemberValidPwd.$valid" class="valid"><?$translate->__('รหัสผ่านที่ตรงกับ');?>!</span>
                            <span ng-show="signUpForm.MemberValidPwd.$error.required && signUpForm.MemberValidPwd.$dirty" class="error"><?$translate->__('โปรดยืนยันรหัสผ่านของคุณ');?></span>
                            <span ng-show="!signUpForm.MemberValidPwd.$error.required && signUpForm.MemberValidPwd.$error.noMatch && signUpForm.MemberValidPwd.$dirty" class="error"><?$translate->__('รหัสผ่านไม่ตรงกัน');?></span>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box">
                            <label><strong class="required">*</strong> <?$translate->__('ชื่อ');?></label>
                            <p><input type="text" maxlength="20"
                                      id="MemberName"
                                      name="MemberName"
                                      ng-model="signupFormData.MemberName"
                                      ng-minlength="4"
                                      ng-maxlength="20"
                                      ng-pattern="/^[A-z][A-z]*$/"
                                      required />
                            </p>
                            <label class="msg" id="msg_id"><?$translate->__('เดียวกันกับบัญชีธนาคาร');?></label>
                            <span ng-show="signUpForm.MemberName.$valid" class="valid"><?$translate->__('ชื่อผู้ใช้สามารถใช้ได้');?>!</span>
                            <span ng-show="signUpForm.MemberName.$error.required && signUpForm.MemberName.$dirty" class="error">*</span>
                            <span ng-show="signUpForm.MemberName.$error.pattern && signUpForm.MemberName.$dirty" class="error">4 - 20 <?$translate->__('ตัวอักษรเท่านั้น');?></span>
                            <span ng-show="!signUpForm.MemberName.$error.required && (signUpForm.MemberName.$error.minlength || signUpForm.MemberName.$error.maxlength) && signUpForm.MemberName.$dirty" class="error">4 - 20 <?$translate->__('ตัวอักษรเท่านั้น');?></span>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box highlight">
                            <label><strong class="required">*</strong> <?$translate->__('หมายเลขโทรศัพท์');?></label>
                            <p><input maxlength="13" type="text" placeholder="000-0000-0000"
                                      id="MemberPhone"
                                      name="MemberPhone"
                                      ng-minlength="8"
                                      ng-maxlength="13"
                                      ng-pattern='/^\d{4}-\d{4}|[0-9]$/'
                                      ng-model="signupFormData.MemberPhone"
                                      required/>
                            </p>
                            <span ng-show="!signUpForm.MemberPhone.$error.required && (signUpForm.MemberPhone.$error.minlength || signUpForm.MemberPhone.$error.maxlength) && signUpForm.MemberPhone.$dirty " class="error"><?$translate->__('หมายเลขโทรศัพท์ที่ไม่ถูกต้อง');?></span>
                            <span ng-show="signUpForm.MemberPhone.$error.pattern && signUpForm.MemberPhone.$dirty" class="error"><?$translate->__('กรุณากรอกตัวเลขเท่านั้น');?></span>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box">
                            <label><strong class="required">*</strong> <?$translate->__('อีเมล');?></label>
                            <p><input type="email"
                                      id="MemberEmail"
                                      name="MemberEmail"
                                      ng-model="signupFormData.MemberEmail"
                                      required />
                            </p>
                            <span ng-show="signUpForm.MemberEmail.$error.email" class="error"><?$translate->__('อีเมลไม่ถูกต้อง');?></span>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box highlight">
                            <label><?$translate->__('สายรหัส');?></label>
                            <p><input type="text"
                                      id="MessengerName"
                                      name="MessengerName"
                                      />
                            </p>
                            <label class="msg" id="msg_id">(<?$translate->__('ไม่จำเป็น');?>)</label>
                            <!--<span ng-show="signUpForm.MessengerName.$error.required && signUpForm.MessengerName.$dirty" class="error">*</span>-->
                            <!--<span ng-show="signUpForm.MessengerName.$error.pattern && signUpForm.MessengerName.$dirty" class="error">6 - 16 <?//$translate->__('ตัวอักษรเท่านั้น');?>.</span>-->
                            <!--<span ng-show="(signUpForm.MessengerName.$error.minlength || signUpForm.MessengerName.$error.maxlength) && signUpForm.MessengerName.$dirty" class="error">6 - 16 <?/*$translate->__('ตัวอักษรเท่านั้น');*/?>.</span>-->
                            <div class="clear"></div>
                        </div>
                        <div class="row-box">
                            <label><?$translate->__('ประเทศ');?></label>
                            <p>
                                <select name="MemberCountry">
                                    <option value="" selected="selected"><?$translate->__('เลือกประเทศ');?></option>
                                    <option value="CN"><?$translate->__('ประเทศจีน');?></option>
                                    <option value="MM"><?$translate->__('พม่า');?></option>
                                    <option value="SG"><?$translate->__('สิงคโปร์');?></option>
                                    <option value="TH"><?$translate->__('ประเทศไทย');?></option>
                                    <option value="US"><?$translate->__('ประเทศสหรัฐอเมริกา');?></option>
                                    <option value="GB"><?$translate->__('สหราชอาณาจักร');?></option>
                                </select>
                            </p>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box highlight">
                            <label><?$translate->__('อายุ');?></label>
                            <p>

                                <select id="ageInYear" name="MemberAge">
                                    <option value="" selected="selected"><?$translate->__('เลือกปี');?></option>
                                    <?php for($i="2000"; $i>="1915";  $i--){
                                        $birthday = $i;
                                        $nowday = date('Y'); //current date
                                        $age = ($nowday - $birthday) ; //age
                                        ?>
                                        <option value="<? echo $age; ?>"><? echo $i; ?></option>
                                    <?}?>
                                </select>
                            </p>
                            <span id="18yrs" class="error"><?$translate->__('อายุ 18 ปีขึ้นไปเท่านั้น');?></span>
                            <div class="clear"></div>
                        </div>
                        <div class="row-box text-center">
                            <div class="signup-terms-box">
                                <i class="icon-terms"></i>
                                <button class="btn btn-lightgray btn-terms" >
                                <?$translate->__('ข้อตกลงและเงื่อนไข');?></button>
                                <button class="btn btn-lightgray btn-over18" >
                                    <?$translate->__('อายุมากกว่า 18 ปี');?></button>
                            </div>
                        </div>
                        <div class="row-box-last text-center">
                            <div class="signup-terms-box">
                                <button type="submit" class="btn btn-orange btn-signup" ng-disabled="signUpForm.$invalid" onclick="signUp();"  ><?$translate->__('สมัครสมาชิก');?></button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="signup-form-promos">
                    <div class="row-box">
                        <img src="common/images/signup-promobanner1.png" />
                    </div>
                    <div class="row-box-last">
                        <img src="common/images/signup-promobanner2.png" />
                    </div>
                </div>
                <div class="clear"></div>
            </div>
    <!--ng-controller-->

            <!--Footer-Container-->
            <div class="footer-container">
                <ul class="footer-logos">
                    <li><img src="common/images/logo_1.png" /></li>
                    <li><img src="common/images/logo_2.png" /></li>
                    <li><img src="common/images/logo_3.png" /></li>
                    <li><img src="common/images/logo_4.png" /></li>
                    <li><img src="common/images/logo_5.png" /></li>
                    <li><img src="common/images/logo_6.png" /></li>
                    <li><img src="common/images/logo_7.png" /></li>
                    <li><img src="common/images/logo_8.png" /></li>
                    <li><img src="common/images/logo_9.png" /></li>
                    <li><img src="common/images/logo_10.png" /></li>
                    <li><img src="common/images/logo_11.png" /></li>
                </ul>
                <div class="clear"></div>
                <div class="footer-details">
                    <div class="customer-care">
                        <h1><?$translate->__('การดูแลลูกค้าของเรา');?></h1>
                        <p>
                            <?$translate->__('เรารู้ซึ้งถึงการดูแลลูกค้าและบริการลูกค้าอย่างดีที่สุด.UEFA168.com มีทีมงานดูแลลูกค้าพร้อมให้บริการท่านตลอด 24 ชั่วโมง และช่วยเหลือตอบคำถาม เพื่อแก้ปัญหาต่างๆและให้ความช่วยเหลือตามที่ท่านต้องการ');?>.
                        </p>
                    </div>
                    <div class="security-game">
                        <h1>Our Security & Game Integrity Promise</h1>
                        <p>
                            <?$translate->__('UEFA168.com รักษามาตรฐานสูงสุดของความปลอดภัยและความสมบูรณ์ของเกม');?>.<br />
                            <?$translate->__('ข้อมูลทั้งหมดมีการป้องกันโดยเทคโนโลยีการเข้ารหัสที่ทันสมัยที่สุดด้วยระบบโดยอัตโนมัติในสถานที่เพื่อตรวจสอบการเล่นเกมและให้แน่ใจว่าทุกด้านของเกมของเราจะดำเนินการในความสนใจที่ดีที่สุดของผู้เล่นของเรา');?>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="copyright">
                    <?$translate->__('ลิขสิทธิ์');?> 2015 <span>Ueaf168.com</span> <?$translate->__('สงวนลิขสิทธิ์');?>.
                </div>
            </div>

        </div>
    </div>
</div>

<div class="masthead">
    <div id="slides-background">
        <div id="slides">
            <div class="slides-container" id="masthead-sports">
                <div><img src="common/images/2.png" /></div>
                <div><img src="common/images/1.png" /></div>
                <div><img src="common/images/3.png" /></div>
            </div>
            <nav class="slides-navigation">
                <div class="containerArrow">
                    <a href="#" class="next"></a>
                    <a href="#" class="prev"></a>
                </div>
            </nav>
        </div>
    </div>
</div>

<form id="frm_game" name="frm_game" method="post" action="/prc/play_game.php">
    <input id="_game" name="_game" type="hidden">
    <input id="_type" name="_type" type="hidden">
    <input id="_code" name="_code" type="hidden">
    <input id="_view" name="_view" type="hidden">
    <input id="_name" name="_name" type="hidden">
</form>

<!--SCRIPTS-->
<script type="text/javascript" src="/common/js/jquery-1.11.1.min.js"></script>
<!--<script type="text/javascript" src="/common/js/jquery-1.11.2.min.js"></script>-->
<script type="text/javascript" src="/common/js/angular.min.js"></script>
<script type="text/javascript" src="/common/js/easing.js"></script>

<script type="text/javascript" src="/common/js/jquery.superslides.js"></script> <!--BG Slider-->
<script type="text/javascript" src="/common/js/jquery.slidereveal.js"></script> <!--Side Panels-->

<script type="text/javascript" src="/common/js/navhover.js"></script><!--Navigation-->
<script type="text/javascript" src="/common/js/jquery.bpopup.js"></script> <!--Popup-->
<script type="text/javascript" src="/common/js/jquery-outline-1.5.js"></script> <!--Text Outline-->
<script type="text/javascript" src="/common/js/jquery-pagination.js"></script> <!--Pagination-->
<script type="text/javascript" src="/common/js/jquery.jodometer.js"></script> <!--Jodometer-->
<script type="text/javascript" src="/common/js/jquery.mCustomScrollbar.js"></script> <!--ScrollBar-->
<script type="text/javascript" src="/common/js/jquery.datetimepicker.js"></script>

<!--SCRIPTS NEED FOR RESTFUL API-->
<script type="text/javascript" src="/common/js/jquery-ui.min.js"></script>
<!--<script type="text/javascript" src="/common/js/jquery.cookie.min.js"></script>-->
<script type="text/javascript" src="/common/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/common/js/custom-angular.js"></script>
<script type="text/javascript" src="/common/js/jOdometer.min.js"></script>
<script type="text/javascript" src="/common/js/jOdometer.custom-jackpot.min.js"></script>
<script type="text/javascript" src="/common/js/popup.min.js"></script>
<script type="text/javascript" src="/common/js/jquery.fade-slider.min.js"></script>
<script type="text/javascript" src="/common/js/jquery-contained-sticky-scroll.min.js"></script>
<script type="text/javascript" src="/common/js/custom.js"></script>
<script type="text/javascript" src="/common/js/custom-module.js"></script>

<script type="text/javascript">
    function load_coupon(page){
        $.getJSON('/prc/member_coupon.php?page='+page, {
            returnformat: 'json'
        }, function(data) {
//        console.log(data);
            var table_data="";
            var page_data="";
            if(data.list.length){
                for(var i = 0 ;i < data.list.length;i++){
                    if(i%2 == 0){
                        table_data += "<tr style='height: 25px;line-height: 25px;padding: 2px 8px'>";
                    }else{
                        table_data += "<tr class='highlight' style='height: 25px;line-height: 25px;padding: 2px 8px'>";
                    }
                    table_data += "<td class=\"row-col width10 text-center\">"+data.list[i].CouponCode + "</td>";
                    table_data += "<td class=\"row-col width28 text-center\">"+data.list[i].CouponName.substr(0,18) + "</td>";
                    table_data += "<td class=\"row-col width10 text-center\">"+data.list[i].CounponAmount +"</td>";
                    table_data += "<td class=\"row-col width12 text-center\">"+data.list[i].CouponExpiredDate.substr(0,10) +"</td>";
                    table_data += "<td class=\"row-col width12 text-center\">"+data.list[i].CouponUsedDate.substr(0,10) +"</td>";

                    if(data.list[i].Status == "G"){
                        table_data += "<td class=\"row-col width28 text-center\">" +
                        "<select class='GameCode' name=\"GameCode\" style='float:none;width:118px;margin-right:5px;height:25px;'>" +
                        "<option value=\"\">Select Game.</option><?foreach ($variables['gameText'] as $k => $v) { ?><option value=\"<?= $k ?>\"><?= $v ?></option><?}?></select>"+
                        "<input type=\"button\" value=\"Claim\" class=\"btn-application\" onclick=\"use_coupon(this,'"+data.list[i].CouponCode+"')\" style='padding: 2px 8px;'></td></tr>";
                    }else{
                        table_data += "<td colspan='2' class=\"row-col width28 text-center\">Used</td></tr>";
                    }
                    table_data += "</tr>";
                }
            }else{
                table_data += "<tr><td colspan='7' class=\"text-center\" style='height:20px;padding-top:20px;'>Non existing data.</td></tr>";
            }

            if(data.pages){
                for(var i=1;i<=data.pages;i++){
                    if(i==data.page){
                        page_data += "<li class='active'>"+i+"</li>";
                    }else {
                        page_data += "<li class=\"pointer link\"><a onclick=\"load_coupon("+i+")\">"+i+"</a></li>";
                    }
                }
            }

            $(".coupon-list").html(table_data);
            $(".pagination-coupon").html(page_data);
        });
    }

    function check_member(type){
        <?if(isset($_SESSION['MemberID'])){?>
        if(type =="sport"){
            playGame('1016','sport','','sport-iframe');
            return true;
        }else if(type=="customer") {
            return true;
        }else{
            return true;
        }
        <?}else{?>
        popupOpenRestricted();
        return false;
        <?}?>

    }

    function openSportASC(){
        var isMember = check_member("sport");
        if(isMember){

            $(".masthead").fadeOut("fast");
            $("body").addClass("bg-sports1");
            $("body").removeClass("bg-sports2");
            $("body").removeClass("bg-sports3");
            $("body").removeClass("bg-sports4");
            $("body").removeClass("bg-casino");
            $("body").removeClass("bg-slots");
            $("body").removeClass("bg-signup");

            $("#nav_mobile, #nav_casino, #nav_slots, #nav_othergames").removeClass('active');
            $("#nav_sports").addClass('active');
            $('.nav_hover_wrapper').hide();

            $(".content-container").removeClass('main-wrapper');
            $(".content-container").removeClass('slots-wrapper');
            $(".content-container").addClass('sports-wrapper');
            $(".content-container").removeClass('signup-wrapper');
            $(".main-container").hide();
            $(".sports-container").show();
            $(".casino-container").hide();
            $(".slots-container").hide();
            $(".signup-container").hide();
            $(".sport-iframe").show();
            $('iframe[name="sportFrame"]').hide();
            $('iframe[name="sport-iframe"]').show();

            playGame('1016','sport','','sport-iframe');

        }else{
            popupOpenRestricted();
        }
    }

    function openSportWFT(){
        var isMember = check_member("sport");
        if(isMember){

            $(".masthead").fadeOut("fast");
            $("body").addClass("bg-sports1");
            $("body").removeClass("bg-sports2");
            $("body").removeClass("bg-sports3");
            $("body").removeClass("bg-sports4");
            $("body").removeClass("bg-casino");
            $("body").removeClass("bg-slots");
            $("body").removeClass("bg-signup");

            $("#nav_mobile, #nav_casino, #nav_slots, #nav_othergames").removeClass('active');
            $("#nav_sports").addClass('active');
            $('.nav_hover_wrapper').hide();

            $(".content-container").removeClass('main-wrapper');
            $(".content-container").removeClass('slots-wrapper');
            $(".content-container").addClass('sports-wrapper');
            $(".content-container").removeClass('signup-wrapper');
            $(".main-container").hide();
            $(".sports-container").show();
            $(".casino-container").hide();
            $(".slots-container").hide();
            $(".signup-container").hide();
            $(".sport-iframe").show();
            $('iframe[name="sport-iframe"]').hide();
            $('iframe[name="sportFrame"]').show();


            playGame('1015','sport','','sportFrame');

        }else{
            popupOpenRestricted();
        }
    }


    function playGame(game,type,code,view,name) {
        <?if(isset($_SESSION['MemberID'])){?>
        if(type=="live" || type=="fun" || type=="slot" || type=="playcheck"){
            var target="";
            if(type == "slot" && game != 1005){
                target=game+Math.random();
            }else{
                target=game;
            }

            if(type=="playcheck"){
                target = 'playcheck';
            }

            if(game==1012){
                var pop = window.open('about:blank',target,'width=1228,height=741,location=0,menubar=0,resizable=1,scrollbars=0,status=0,titlebar=0,toolbar=0').focus();
            }else if(game==1014) {
                var pop = window.open('about:blank', target, 'width=950,height=610,location=0,menubar=0,resizable=1,scrollbars=0,status=0,titlebar=0,toolbar=0').focus();
            }else if(game==1090){
                var pop = window.open('about:blank', target, 'width=1038,height=580,location=0,menubar=0,resizable=1,scrollbars=0,status=0,titlebar=0,toolbar=0').focus();
            }else{
                var pop = window.open('about:blank',target,'width=1024,height=768,location=0,menubar=0,resizable=1,scrollbars=0,status=0,titlebar=0,toolbar=0').focus();
            }

            document.getElementById('frm_game').target = target;
            document.getElementById('_game').value = game;
            document.getElementById('_type').value = type;
            document.getElementById('_code').value = code;
            document.getElementById('_view').value = view;
            if(name  === undefined){

            }else{
                document.getElementById('_name').value = name;
            }
            //    document.getElementById('frm_game').method = 'post';
            document.getElementById('frm_game').submit();
        }else{
            $('.'+view).show();
            document.getElementById('frm_game').target = view;
            document.getElementById('_game').value = game;
            document.getElementById('_type').value = type;
            document.getElementById('_code').value = code;
            document.getElementById('frm_game').submit();
        }
        <?}else{?>

        //Login Modal
        popupOpenLogin();
        <?}?>
    }

    $(document).ready(function(){

        //Date Picker
        $('#datetimepicker').datetimepicker({
            dayOfWeekStart : 1,

        <?php if($_GET['lang'] == "english"){ ?>
            lang:'en',
        <?}else{?>
            lang:'th',
        <?}?>

            theme:'dark',
            step:10
        });

        $('.oneDay').click(function () {
            <?php setcookie('oneDay',$oneDay,time() + (86400 * 7)); // 86400 = 1 day ?>
                $('#popup-notice').hide();
                popupClose();

        });


        //getJackpot();
        <?if(isset($_SESSION['MemberID'])){?>
        setInterval(check_session,5000);
        getAllGameBalance();
        count_copuon();
        <?}?>

        $(window).on('resize', function() {
            if ($(window).width() <= 1600) {
                $(document).scrollLeft(0);
            }
            if ($(window).width() <= 1536) {
                $(document).scrollLeft(40);
            }
            if ($(window).width() <= 1440) {
                $(document).scrollLeft(110);
            }
            if ($(window).width() <= 1360) {
                $(document).scrollLeft(130);
            }
            if ($(window).width() <= 1280) {
                $(document).scrollLeft(170);
            }
            if ($(window).width() <= 1152) {
                $(document).scrollLeft(235);
            }
            if ($(window).width() <= 1024) {
                $(document).scrollLeft(300);
            }
            if ($(window).width() <= 800){
                $(document).scrollLeft(400);
            }
        });

        $(function() {
            if ($(window).width() <= 1600) {
                $(document).scrollLeft(0);
            }
            if ($(window).width() <= 1536) {
                $(document).scrollLeft(40);
            }
            if ($(window).width() <= 1440) {
                $(document).scrollLeft(110);
            }
            if ($(window).width() <= 1360) {
                $(document).scrollLeft(130);
            }
            if ($(window).width() <= 1280) {
                $(document).scrollLeft(170);
            }
            if ($(window).width() <= 1152) {
                $(document).scrollLeft(235);
            }
            if ($(window).width() <= 1024) {
                $(document).scrollLeft(300);
            }
            if ($(window).width() <= 800){
                $(document).scrollLeft(400);
            }
        });

    });
</script>
<script type="text/javascript" src="/common/js/custom-route.js"></script>
<!--Slot Slider-->
<script type="text/javascript" src="/common/js/jquery-easing.js"></script>
<script type="text/javascript" src="/common/js/jquery-touchswipe.js"></script>
<script type="text/javascript" src="/common/js/jquery.liquid-slider.js"></script>
<?if($_SERVER['HTTP_HOST'] == 'grand.isltest.net'){?>

<?}else{?>
    <script type="text/javascript">
        var __lc = {};
        //__lc.license = 5524611;
        __lc.license = 5082911;

        <? if(isset($_SESSION['MemberID'])){?>
        __lc.params = [
            { name: 'MemberInfo', value: '<?=$_SESSION['MemberID']?>' }
        ];

        __lc.visitor = {
            name: '<?=$_SESSION['MemberID']?>',
            email: ''
        };
        <?}else{?>
        __lc.visitor = {
            name: 'Customer',
            email: ''
        };
        <?}?>
        (function() {
            var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
            lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
        })();
    </script>
<?}?>
</body>
</html>