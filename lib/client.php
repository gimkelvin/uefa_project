<?php
session_start();
header('Content-type:text/html; charset=UTF-8');
header('P3P: CP="ALL CURa ADMa DEVa TAIa OUR BUS IND PHY ONL UNI PUR FIN COM NAV INT DEM CNT STA POL HEA PRE LOC OTC"');
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
ini_set('display_errors', 1);

include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/conn.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/game.php";
$lang=$_GET['lang'];
if($lang=='')$lang='thai';
if($lang == 'english'){
    class ReqeustAPI
    {
        public static function call($method,$parameter,$timeout = 6){
            $xmlRequest = self::create_xml($method,$parameter);
    //        var_dump($xmlRequest);
            $ch = curl_init(host());
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlRequest);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $xmlResponse = curl_exec($ch);
            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    //        var_dump($xmlResponse);
            $result = json_decode(str_replace(':{}',':null',json_encode((array) simplexml_load_string($xmlResponse,'SimpleXMLElement', LIBXML_NOCDATA))));
            curl_close($ch);
            $return[] = $code;
            $return[] = $result->Header;
            if(isset($result->Parameter)){
                if(isset($result->Parameter->Record)){
                    if(count($result->Parameter->Record) == 1){
                        $record = array($result->Parameter->Record);

                        $result->Parameter->Record="";
                        $result->Parameter->Record=$record;
                    }
                }else{
                    $result->Parameter->Record="";
                }
                $return[] = $result->Parameter;

            }
            return $return;
        }


        private static $errorCode = array(
            101 => 'Missing Server Code.',
            102 => 'Missing Server Secure Code.',
            103 => 'Invalid Server IP.',
            104 => 'Not Found Server.',
            105 => 'Invalid URL.',
            106 => 'Deny Visiter IP Address.',
            107 => 'Not Found Visiter IP Address.',
            201 => 'Invalid Member ID or Password.',//Invalid Member ID
            202 => 'Missing Parameter',
            203 => 'Incorrect Member ID or Password',//No Found Member ID
            204 => 'Incorrect Member ID or Password',//Incorrect Member Password
            205 => 'Not Enough Member Balance.',
            206 => 'Member ID already exist.',//Member ID already exist
            207 => 'No Found Member ID Or Member Token.',//No Found Member ID Or Member Token
            208 => 'The amount is lower then minimum deposit amount allowed.',
            209 => 'The amount is lower then withdrawal amount allowed.',
            210 => 'Expired Date Coupon.',//Expired Date Coupon
            211 => 'Not Found Coupon Or already Used Coupon.',//Not Found Coupon Or already Used Coupon
            212 => 'Can not find the login information.',//Can not find the login information
            213 => 'Pending approval.',
            214 => 'Not Found TransactionID.',//Not Found TransactionID
            215 => 'Can Not Be Canceled.',
            221 => 'No Found Member ID Or Member Email.',
            301 => 'Not Found Post.',
            302 => 'Game ID Create Error.',
            303 => 'Not Found Aff ID on Payment.',
            304 => 'Trial period has expired.',
            305 => 'Trial game has already been used from this location.',
            311 => 'Incorrect Member ID or Password.',
            312 => 'Invalid name.',
            317 => 'Invalid amount.',
            318 => 'Website error.',
            319 => 'Invaild memo.',
            320 => 'Invalid Bank name.',
            321 => 'Invaild Account number.',
            350 => 'ID is already in use.',
            400 => 'Not Enough Comp Amount.',
            401 => 'Invalid Comp Type.',
            402 => 'Invalid Comp Index.',
            501 => "Game is under system maintenance.",
            811 => 'Failed to create account.',
            999 => 'Unknown Error.',
        );

        public static function parse_http_header($str)
        {
            $lines = explode("\r\n", $str);
            $head  = array(array_shift($lines));
            foreach ($lines as $line) {
                list($key, $val) = explode(':', $line, 2);
                if ($key == 'Set-Cookie') {
                    $head['Set-Cookie'][] = trim($val);
                } else {
                    $head[$key] = trim($val);
                }
            }
            return $head;
        }

        public static function decode_chunked($str) {
            for ($res = ''; !empty($str); $str = trim($str)) {
                $pos = strpos($str, "\r\n");
                $len = hexdec(substr($str, 0, $pos));
                $res.= substr($str, $pos + 2, $len);
                $str = substr($str, $pos + 2 + $len);
            }
            return $res;
        }

        public static function filter_xml($matches) {
            return trim(htmlspecialchars($matches[1]));
        }


        public static function errorCode($rst)
        {
            //ErrorMsg
            $code = $rst;

            if (array_key_exists($code, self::$errorCode)) {
                return self::$errorCode[$code];
            } else {
                return self::$errorCode[$code]."Unknown Error.";
            }
        }

        public static function create_xml($method,$param){
            $xmlBase ="<?xml version='1.0' encoding='UTF-8'?><Request></Request>";
            $xmlRequest = new SimpleXMLElement($xmlBase);
    //        $xmlRequest->addAttribute("Response","res");
            $header = $xmlRequest->addChild("Header");
            $header->addChild("Method",$method);
            $header->addChild("ServerCode",server_code());
            $header->addChild("ServerSecureCode",server_key());
            if(!empty($param)){
                $parameter = $xmlRequest->addChild("Parameter");
                foreach($param as $key=>$value) {
                    if(trim($value) == false){
                        $parameter->addChild($key,"");
                    }else{
                        $parameter->addChild($key,$value);
                    }
                }
            }
            $xmlRequest = $xmlRequest->asXML();
            return $xmlRequest;
        }

        public static function xml_error_check($xml){
            libxml_use_internal_errors(true);
            $doc = simplexml_load_string($xml);
            foreach( libxml_get_errors() as $err ) {
                var_dump($err);
            }
            if ( !is_object($doc) ) {
                var_dump($doc);
            }
        }
    }
}else{ //lang='thai'
    class ReqeustAPI
    {
        public static function call($method,$parameter,$timeout = 6){
            $xmlRequest = self::create_xml($method,$parameter);
            //        var_dump($xmlRequest);
            $ch = curl_init(host());
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlRequest);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $xmlResponse = curl_exec($ch);
            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            //        var_dump($xmlResponse);
            $result = json_decode(str_replace(':{}',':null',json_encode((array) simplexml_load_string($xmlResponse,'SimpleXMLElement', LIBXML_NOCDATA))));
            curl_close($ch);
            $return[] = $code;
            $return[] = $result->Header;
            if(isset($result->Parameter)){
                if(isset($result->Parameter->Record)){
                    if(count($result->Parameter->Record) == 1){
                        $record = array($result->Parameter->Record);

                        $result->Parameter->Record="";
                        $result->Parameter->Record=$record;
                    }
                }else{
                    $result->Parameter->Record="";
                }
                $return[] = $result->Parameter;

            }
            return $return;
        }


        private static $errorCode = array(
            101 => 'รหัสเซิร์ฟเวอร์ที่ขาดหายไป.',
            102 => 'เซิร์ฟเวอร์ที่ขาดหายไปรหัสรักษาความปลอดภัย.',
            103 => 'เซิร์ฟเวอร์ที่ขาดหายไปรหัสรักษาความปลอดภัย.',
            104 => 'เซิร์ฟเวอร์ไม่พบ.',
            105 => 'สมาชิกที่ไม่ถูกต้อง.',
            106 => 'ปฏิเสธที่อยู่ IP ของผู้เข้าชม.',
            107 => 'ที่อยู่ IP ของผู้เข้าชมไม่พบ.',
            201 => 'รหัสสมาชิกที่ไม่ถูกต้องหรือรหัสผ่าน.',//invalid member id
            202 => 'พารามิเตอร์ที่ขาดหายไป',
            203 => 'รหัสสมาชิกรหัสผ่านไม่ถูกต้องหรือ',//no found member id
            204 => 'รหัสสมาชิกรหัสผ่านไม่ถูกต้องหรือ',//incorrect member password
            205 => 'ไม่สมดุลสมาชิกพอ.',
            206 => 'รหัสสมาชิกอยู่แล้ว.',//member id already exist
            207 => 'ไม่พบรหัสสมาชิกหรือโทเค็นสมาชิก.',//no found member id or member token
            208 => 'จำนวนเงินที่ต่ำแล้วจำนวนเงินฝากขั้นต่ำที่ได้รับอนุญาต.',
            209 => 'จำนวนเงินที่ต่ำแล้วจำนวนเงินที่ถอนได้รับอนุญาต.',
            210 => 'คูปองวันที่หมดอายุ.',//expired date coupon
            211 => 'ไม่พบคูปองหรือคูปองใช้แล้ว.',//not found coupon or already used coupon
            212 => 'ไม่สามารถหาข้อมูลเข้าสู่ระบบ.',//can not find the login information
            213 => 'รอการอนุมัติ.',
            214 => 'ไม่พบรหัสการทำธุรกรรม.',//not found transactionid
            215 => 'ไม่สามารถยกเลิก.',
            221 => 'ไม่พบรหัสสมาชิกหรืออีเมล์สมาชิก.',
            301 => 'โพสต์ไม่พบ.',
            302 => 'เกมสร้างรหัสข้อผิดพลาด.',
            303 => 'ไม่พบ AFF รหัสในการชำระเงิน.',
            304 => 'ระยะเวลาการทดลองหมดอายุ.',
            305 => 'เกมการทดลองได้ถูกนำมาใช้จากสถานที่นี้.',
            311 => 'รหัสสมาชิกรหัสผ่านไม่ถูกต้องหรือ.',
            312 => 'ชื่อที่ไม่ถูกต้อง.',
            317 => 'จำนวนเงินที่ไม่ถูกต้อง.',
            318 => 'ข้อผิดพลาดเว็บไซต์.',
            319 => 'บันทึกที่ไม่ถูกต้อง.',
            320 => 'ชื่อธนาคารที่ไม่ถูกต้อง.',
            321 => 'ชื่อธนาคารที่ไม่ถูกต้อง.',
            350 => 'รหัสที่มีอยู่แล้วในการใช้งาน.',
            //400 => 'ไม่เพียงพอจำนวนคอมพ์.',
            400 => 'Not Enough Comp Amount.',
            401 => 'ประเภทคอมพ์ที่ไม่ถูกต้อง.',
            402 => 'ดัชนีคอมพ์ที่ไม่ถูกต้อง.',
            501 => "เกมที่อยู่ภายใต้การบำรุงรักษาระบบ.",
            811 => 'ล้มเหลวในการสร้างบัญชี.',
            999 => 'ข้อผิดพลาดที่ไม่รู้จัก.',
        );

        public static function parse_http_header($str)
        {
            $lines = explode("\r\n", $str);
            $head  = array(array_shift($lines));
            foreach ($lines as $line) {
                list($key, $val) = explode(':', $line, 2);
                if ($key == 'Set-Cookie') {
                    $head['Set-Cookie'][] = trim($val);
                } else {
                    $head[$key] = trim($val);
                }
            }
            return $head;
        }

        public static function decode_chunked($str) {
            for ($res = ''; !empty($str); $str = trim($str)) {
                $pos = strpos($str, "\r\n");
                $len = hexdec(substr($str, 0, $pos));
                $res.= substr($str, $pos + 2, $len);
                $str = substr($str, $pos + 2 + $len);
            }
            return $res;
        }

        public static function filter_xml($matches) {
            return trim(htmlspecialchars($matches[1]));
        }


        public static function errorCode($rst)
        {
            //ErrorMsg
            $code = $rst;

            if (array_key_exists($code, self::$errorCode)) {
                return self::$errorCode[$code];
            } else {
                return self::$errorCode[$code]."Unknown Error.";
            }
        }

        public static function create_xml($method,$param){
            $xmlBase ="<?xml version='1.0' encoding='UTF-8'?><Request></Request>";
            $xmlRequest = new SimpleXMLElement($xmlBase);
            //        $xmlRequest->addAttribute("Response","res");
            $header = $xmlRequest->addChild("Header");
            $header->addChild("Method",$method);
            $header->addChild("ServerCode",server_code());
            $header->addChild("ServerSecureCode",server_key());
            if(!empty($param)){
                $parameter = $xmlRequest->addChild("Parameter");
                foreach($param as $key=>$value) {
                    if(trim($value) == false){
                        $parameter->addChild($key,"");
                    }else{
                        $parameter->addChild($key,$value);
                    }
                }
            }
            $xmlRequest = $xmlRequest->asXML();
            return $xmlRequest;
        }

        public static function xml_error_check($xml){
            libxml_use_internal_errors(true);
            $doc = simplexml_load_string($xml);
            foreach( libxml_get_errors() as $err ) {
                var_dump($err);
            }
            if ( !is_object($doc) ) {
                var_dump($doc);
            }
        }
    }
} //end else


$variables['bank'] = array(
  100 => "Maybank",
  101 => "Public Bank",
  102 => "CIMB bank",
  103 => "Hong Leong Bank",
  104 => "RHB Bank",
);

$variables['help2pay'] = array(
    "MBB" => "Maybank",
    "PBB" => "Public Bank",
    "CIMB" => "CIMB bank",
    "HLB" => "Hong Leong Bank"
);

$variables['help2PayToApi'] = array(
    "MBB" => 100,
    "PBB" => 101,
    "CIMB" => 102,
    "HLB" => 103
);


$variables['status'] = array(
    'D'=>'Cancel',
    'P'=>'Pending',
    'R'=>'Submit',
    'S'=>'Completed'
);


$variables['type'] = array(
    'Deposit'=>'Deposit',
    'Withdrawal'=>'Withdrawal',
    'Transfer' => 'Transfer',
    'NO' => 'Standard',
    'FS' => 'First Deposit',
    'DA' => 'Daily First Deposit',
    'CO' => 'Comp',
    'CP' => 'Coupon',
    'ET' => 'Other'
);

function strlen_utf8($str, $checkmb = false)
{
    preg_match_all('/[\xE0-\xFF][\x80-\xFF]{2}|./', $str, $match); // target for BMP

    $m = $match[0];
    $mlen = count($m); // length of matched characters

    if (!$checkmb) return $mlen;

    $count = 0;
    for ($i = 0; $i < $mlen; $i++) {
        $count += ($checkmb && strlen($m[$i]) > 1) ? 2 : 1;
    }

    return $count;
}

/* reg exp ver=2012.07.16.1 */
function regExp($mode, $str, $min = null, $max = null)
{
    if ($mode == "integer") {
        $regex = "/^\d+$/";
    } else if ($mode == "float") {
        $regex = "/[-+]?[0-9]*\.?[0-9]+/";
    } else if ($mode == "alphanumeric") {
        $regex = "/^[a-zA-Z0-9-_]*$/";
    } else if ($mode == "alphanumericspace") {
        $regex = "/^[a-zA-Z0-9\x20]*$/";
    } else if ($mode == "kor_alpha_num") {
        $regex = "/^([\xEA-\xED][\x80-\xBF]{2}|[a-zA-Z0-9])*$/";
    } else if ($mode == "phone") {
        $regex = "/^[0-9]{11,12}$/";
    } else if ($mode == "phone2") {
        $regex = "/^\d{4}-\d{4}|\d{2,3}-\d{3,4}-\d{4}$/";
    } else if ($mode == "ip") {
        $regex = "/\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/";
    } else if ($mode == "domain") {
        $regex = "/^[a-zA-Z0-9.]*$/";
    } else if ($mode == "bank_account") {
        $regex = "/^[0-9-]*$/";
    } else if ($mode == "all") {
        $regex = "/.*/";
    } else {
        return false;
    }

    if (!is_null($min)) {
        $length = strlen_utf8($str, true);

        if ($length < $min || $length > $max) {
            return false;
        }
    }
    if (!preg_match($regex, $str)) {
        return false;
    }
    return true;
}

function get_microtime()
{
    list($usec, $sec) = explode(" ",microtime());
    return ((float)$usec + (float)$sec);
}


include_once $_SERVER["DOCUMENT_ROOT"] . "/include/incapsula.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/include/Mobile_Detect.php";
$detect = new Mobile_Detect;


