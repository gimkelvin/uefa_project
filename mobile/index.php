<?include_once $_SERVER["DOCUMENT_ROOT"] . "/include/class.translation.php";
if(isset($_GET['lang'])) {
    $translate = new Translator($_GET['lang']);
    include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php?lang=english";

}else {

    $translate = new Translator('thai');
    include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php";
}

include_once $_SERVER["DOCUMENT_ROOT"] . "/prc/check_server.php";
if (isset($_SESSION['MemberID'])) {
    include_once $_SERVER["DOCUMENT_ROOT"] . "/prc/session_extension.php";
}
if(isset($_SESSION["viewDesktop"])){
    unset($_SESSION["viewDesktop"]);
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
    <title>UEFA168.com</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/gif">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/sukhumvit-font.css" rel="stylesheet">
</head>
<body ng-app="uefaApp">
<form id="frm_game" name="frm_game" method="post" action="/prc/play_game.php">
    <input id="_game" name="_game" type="hidden">
    <input id="_type" name="_type" type="hidden">
    <input id="_code" name="_code" type="hidden">
    <input id="_view" name="_view" type="hidden">
    <input id="_name" name="_name" type="hidden">
</form>
    <div id="preloader">
        <div id="status">
            <p class="text-center"><img src="images/logo.png" /></p>
            <p class="text-center"><img src="images/preloader-dotted.gif" /></p>
        </div>
    </div>

    <!--Nav Header-->
    <nav class="nav-bar">
        <div class="nav-header">
            <button type="button" class="btn-menu btn-menu-open"></button>
            <button type="button" class="btn-menu btn-menu-close"></button>

            <div class="logo text-center"><img src="images/logo.png" /></div>

            <button type="button" class="btn-lang">
                <?if($_GET['lang']=="english"){
                    echo '<img src="/mobile/images/flag/eng-active.png" />';
                }else if($_GET['lang']=="chinese"){
                    echo '<img src="/mobile/images/flag/chi-active.png" />';
                }else{
                    echo '<img src="/mobile/images/flag/thai-active.png" />';
                } ?>

            </button>

            <?if(!isset($_SESSION['MemberID'])){?>
                <button type="button" class="btn btn-login"></button>
            <?}else{?>
                <button type="button" class="btn btn-logout" onclick="logout();"></button>
            <?}?>
            <!--<button type="button" class="btn btn-gold btn-wallet">마이 월렛</button>-->
        </div>

        <!--Language Option-->
        <div id="lang-list">
            <ul>
                <li onclick="location.href='/mobile'"><i class="icon-lang icon-thai"></i> ไทย</li>
                <li onclick="location.href='/mobile/index.php?lang=english'"><i class="icon-lang icon-eng"></i> ENGLISH</li>
                <li onclick="location.href='/mobile/index.php?lang=chinese'"><i class="icon-lang icon-chi"></i> 中國語</li>
            </ul>
        </div>
    </nav>

    <section id="top-container"></section> <!--fixed section used for top margin-->

    <!--Login-->
    <div class="login-container">
        <div class="login-content">
            <h5><?$translate->__('เข้าสู่บัญชีของคุณ');?></h5>
            <form id="login-form">
                <input type="text" name="MemberID" placeholder="<?$translate->__('รหัสผู้ใช้');?>" class="input-field">
                <input type="password" name="MemberPwd" placeholder="<?$translate->__('รหัสผ่าน');?>" class="input-field">
                <button class="btn-lg btn-gold btnLogin" onclick="login();"><?$translate->__('เข้าสู่ระบบ');?></button>
                <input type="hidden" id="hiddenLogin" value="LOGIN">
            </form>
            <hr>
            <p>
<!--                <span class="btn-forgotpass">ลืมรหัสผ่านของคุณ?</span>-->
                <button class="btn btn-md btn-info btn-signup"><?$translate->__('ลงทะเบียนที่น');?></button>
            </p>
            <div class="clear"></div>
        </div>
    </div>

    <!--Sign Up-->
    <section id="signup-container" class="page-content">
        <div class="signup-content">
            <h1><?$translate->__('สร้างบัญชีผู้ใช้');?></h1>
            <div class="page-form">
                <form id="cd-form" name="signUpForm" ng-submit="signUp()" novalidate>
                    <div class="row-form">
                        <label><?$translate->__('รหัสผู้ใช้');?></label>
                        <input type="text" onkeyup="checkid(this);" maxlength="20"
                               placeholder="<?$translate->__('รหัสผู้ใช้');?>" class="input-field"
                               id="MemberID"
                               name="MemberID"
                               required />
                        <span class="txt-required">*</span>
                        <span class="msg" id="msg_id"></span>
<!--                        <span ng-show="signUpForm.MemberID.$error.required && signUpForm.MemberID.$dirty" class="error">This field is required</span>
                        <span ng-show="signUpForm.MemberID.$error.hasID" class="error">ID is already in use.</span>
                        <span ng-show="!signUpForm.MemberID.$error.required && (signUpForm.MemberID.$error.minlength || signUpForm.MemberID.$error.maxlength) && signUpForm.MemberID.$dirty" class="error">4-6 (a-z,02-9) ตัวอักษรเท่านั้น</span>-->
                    </div>

                    <div class="row-form">
                        <label><?$translate->__('รหัสผ่าน');?></label>
                        <input type="password" placeholder="<?$translate->__('รหัสผ่าน');?>" class="input-field" maxlength="16"
                               id="signuppassword"
                               name="MemberPwd"
                               ng-model="signupFormData.MemberPwd"
                               ng-minlength="6"
                               ng-maxlength="16"
                               required />
                        <span class="txt-required">*</span>
                        <span ng-show="signUpForm.MemberPwd.$error.required && signUpForm.MemberPwd.$dirty" class="error"><?$translate->__('กรุณาใส่รหัสผ่าน');?></span>
                        <span ng-show="!signUpForm.MemberPwd.$error.required && (signUpForm.MemberPwd.$error.minlength || signUpForm.MemberPwd.$error.maxlength) && signUpForm.MemberPwd.$dirty" class="error">6 - 16 <?$translate->__('ตัวอักษร');?></span>
                    </div>

                    <div class="row-form">
                        <label><?$translate->__('ยืนยันรหัสผ่าน');?></label>
                        <input type="password" placeholder="<?$translate->__('ยืนยันรหัสผ่าน');?>" class="input-field" maxlength="16"
                               id="MemberValidPwd"
                               name="MemberValidPwd"
                               ng-model="signupFormData.MemberValidPwd"
                               valid-password-c
                               required />
                        <span class="txt-required">*</span>
                        <span ng-show="signUpForm.MemberValidPwd.$valid" class="error valid"><?$translate->__('รหัสผ่านที่ตรงกับ');?>!</span>
                        <span ng-show="signUpForm.MemberValidPwd.$error.required && signUpForm.MemberValidPwd.$dirty" class="error"><?$translate->__('โปรดยืนยันรหัสผ่านของคุณ');?></span>
                        <span ng-show="!signUpForm.MemberValidPwd.$error.required && signUpForm.MemberValidPwd.$error.noMatch && signUpForm.MemberValidPwd.$dirty" class="error"><?$translate->__('รหัสผ่านไม่ตรงกัน');?></span>
                    </div>

                    <div class="row-form">
                        <label><?$translate->__('ชื่อ');?></label>
                        <input type="text" placeholder="<?$translate->__('ชื่อ');?>" class="input-field" maxlength="20"
                               id="MemberName"
                               name="MemberName"
                               ng-model="signupFormData.MemberName"
                               ng-minlength="4"
                               ng-maxlength="20"
                               ng-pattern="/^[A-z][A-z]*$/"
                               required />
                        <span class="txt-required">*</span>
                        <span ng-show="signUpForm.MemberName.$error.required && signUpForm.MemberName.$dirty" class="error">*</span>
                        <span ng-show="signUpForm.MemberName.$valid" class="error valid"><?$translate->__('ชื่อผู้ใช้สามารถใช้ได้');?>!</span>
                        <span ng-show="signUpForm.MemberName.$error.required && signUpForm.MemberName.$dirty" class="error">*</span>
                        <span ng-show="signUpForm.MemberName.$error.pattern && signUpForm.MemberName.$dirty" class="error">4 - 20 <?$translate->__('ตัวอักษรเท่านั้น');?></span>
                        <span ng-show="!signUpForm.MemberName.$error.required && (signUpForm.MemberName.$error.minlength || signUpForm.MemberName.$error.maxlength) && signUpForm.MemberName.$dirty" class="error">4 - 20 <?$translate->__('ตัวอักษรเท่านั้น');?></span>
                    </div>

                    <div class="row-form">
                        <label> <?$translate->__('หมายเลขโทรศัพท์');?></label>
                        <input type="text" placeholder="000-0000-0000" class="input-field" maxlength="13"
                               id="MemberPhone"
                               name="MemberPhone"
                               ng-minlength="8"
                               ng-maxlength="13"
                               ng-pattern='/^\d{4}-\d{4}|[0-9]$/'
                               ng-model="signupFormData.MemberPhone"
                               required/>
                        <span class="txt-required">*</span>
                        <span ng-show="!signUpForm.MemberPhone.$error.required && (signUpForm.MemberPhone.$error.minlength || signUpForm.MemberPhone.$error.maxlength) && signUpForm.MemberPhone.$dirty " class="error"><?$translate->__('หมายเลขโทรศัพท์ที่ไม่ถูกต้อง');?></span>
                        <span ng-show="signUpForm.MemberPhone.$error.pattern && signUpForm.MemberPhone.$dirty" class="error"><?$translate->__('กรุณากรอกตัวเลขเท่านั้น');?></span>
                    </div>

                    <div class="row-form">
                        <label>อีเมล</label>
                        <input type="email" placeholder="<?$translate->__('อีเมล');?>" class="input-field"
                               id="MemberEmail"
                               name="MemberEmail"
                               ng-model="signupFormData.MemberEmail"
                               required />
                        <span class="txt-required">*</span>
                        <span ng-show="signUpForm.MemberEmail.$error.email" class="error"><?$translate->__('อีเมลไม่ถูกต้อง');?></span>
                    </div>

                    <div class="row-form">
                        <label><?$translate->__('สายรหัส');?></label>
                        <input type="text" placeholder="<?$translate->__('สายรหัส');?>" class="input-field"
                               id="MessengerName"
                               name="MessengerName"
                               >
                    </div>

                    <div class="row-form">
                        <label><?$translate->__('ประเทศ');?></label>
                        <select name="MemberCountry"
                                id="MemberCountry"
                                ng-model="signupFormData.MemberCountry"
                                required>
                            <option value="" selected="selected"><?$translate->__('เลือกประเทศ');?></option>
                            <option value="CN"><?$translate->__('ประเทศจีน');?></option>
                            <option value="MM"><?$translate->__('พม่า');?></option>
                            <option value="SG"><?$translate->__('สิงคโปร์');?></option>
                            <option value="TH"><?$translate->__('ประเทศไทย');?></option>
                            <option value="US"><?$translate->__('ประเทศสหรัฐอเมริกา');?></option>
                            <option value="GB"><?$translate->__('สหราชอาณาจักร');?></option>
                        </select>
                        <span class="txt-required">*</span>
                        <span ng-show="signUpForm.MemberCountry.$error.required  && signUpForm.MemberCountry.$dirty" class="error"><?$translate->__('ฟิลด์นี้จะต้อง');?></span>
                    </div>

                    <div class="row-form">
                        <label><?$translate->__('อายุ');?><</label>
                        <select id="MemberAge"
                                name="MemberAge"
                                ng-model="signupFormData.MemberAge"
                                required >
                            <option value=""><?$translate->__('เลือกปี');?></option>
                            <?php for($i="1997"; $i>="1915";  $i--){
                                $birthday = $i;
                                $nowday = date('Y'); //current date
                                $age = ($nowday - $birthday) ; //age
                                ?>
                                <option value="<? echo $age; ?>"><? echo $i; ?></option>
                            <?}?>
                        </select>
                        <span class="txt-required">*</span>
                        <span class="error"  ng-show="signUpForm.MemberAge.$error.required  && signUpForm.MemberAge.$dirty">
                            <?$translate->__('อายุ 18 ปีขึ้นไปเท่านั้น');?></span>
                    </div>
<!--                    <div class="row-form">
                        <div class="signup-terms-box">
                            <input type="checkbox"
                                   ng-model="agreedToTerms"
                                   name="agreedToTerms"
                                   id="agreedToTerms"
                                   required
                                   style="display: none;"/>

                            <button class="btn btn-lg btn-info btn-terms"><?/*$translate->__('ข้อตกลงและเงื่อนไข');*/?></button>
                            <button class="btn btn-lg btn-info btn-over18"><?/*$translate->__('อายุมากกว่า 18 ปี');*/?></button>
                        </div>
                    </div>
                    <span class="error-terms" ng-show="signUpForm.agreedToTerms.$error.required && signUpForm.agreedToTerms.$dirty">
                                <?/*$translate->__('คุณต้องยอมรับข้อตกลงและเงื่อนไข');*/?>.
                    </span>-->
                    <button class="btn btn-lg btn-gold" ng-disabled="signUpForm.$invalid" onclick="signUp()"  ><?$translate->__('สมัครสมาชิก');?></button>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>

<!--Forgot Password-->
<!--<form id="forgot-pass-mobile" >
    <div class="forgotpass-container">
    <div class="login-content">
        <h5><?/*$translate->__('ลืมรหัสผ่านของคุณ');*/?></h5>
            <input type="text" name="MemberID" placeholder="<?/*$translate->__('รหัสผู้ใช้');*/?>" class="input-field">
            <input type="email" name="MemberEmail" placeholder="<?/*$translate->__('อีเมล');*/?>" class="input-field">
            <button class="btn-lg btn-gold" onclick="forgot_password()"><?/*$translate->__('ตกลง');*/?></button>
    </div>
</div>
</form>-->

    <!--Deposit-->
    <section id="deposit-container" class="page-content">
        <h1>ฝาก</h1>
        <div class="page-form">
            <form id="deposit-form-help2pay" name="depositForm" novalidate ng-controller="DepositCtrl">
                <div class="row-form">
                    <label><?$translate->__('เลือกเกม');?></label>
                    <select class="input-field"
                        name="Wallet"
                            id="Wallet"
                            ng-model="depositFormdata.Wallet"
                            required>
                        <option><?$translate->__('เลือกเกม');?></option>
                        <!--                                <option value="">AFB Sports</option>
                                    <option value="">Playtech</option>
                                    <option value=""><?/*$translate->__('ผลการแข่งขัน');*/?></option>-->
                        <? foreach ($variables['gameText'] as $k => $v) { ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                        <? } ?>
                    </select>
                    <span ng-show="depositForm.Wallet.$error.required && depositForm.Wallet.$dirty" class="error"><?$translate->__('กรุณาเลือกเกม');?></span>
                </div>

                <div class="row-form">
                    <label><?$translate->__('หมายเลขโทรศัพท์');?></label>
                    <input type="text" placeholder="000-0000-0000" class="input-field" maxlength="13"
                           id="MemberPhone"
                           name="MemberPhone"
                           ng-minlength="8"
                           ng-maxlength="13"
                           ng-pattern='/^\d{4}-\d{4}|[0-9]$/'
                           ng-model="depositFormdata.MemberPhone"
                           required/>
                    <span ng-show="!depositForm.MemberPhone.$error.required && (depositForm.MemberPhone.$error.minlength ||
                                            depositForm.MemberPhone.$error.maxlength) && depositForm.MemberPhone.$dirty "
                          class="error"><?$translate->__('หมายเลขโทรศัพท์ที่ไม่ถูกต้อง');?></span>
                    <span ng-show="depositForm.MemberPhone.$error.pattern && depositForm.MemberPhone.$dirty"
                          class="error"><?$translate->__('ใส่หมายเลขที่ถูกต้องเท่านั้น');?></span>
                </div>

                <div class="row-form">
                    <label><?$translate->__('จำนวนเงินฝาก');?></label>
                    <input type="text" placeholder="0" class="input-field text-right"
                           id="Amount"
                           name="Amount"
                           ng-model="depositFormdata.Amount"
                           required/>
                    <span ng-show="depositForm.Amount.$error.required && depositForm.Amount.$dirty" class="error"><?$translate->__('กรุณากรอกจำนวนเงิน');?></span>
                </div>

                <div class="row-form">
                    <label><?$translate->__('ชื่อของผู้ฝากเงิน');?></label>
                    <input type="text" placeholder="<?$translate->__('2 ถึง 10 อักษร');?>" class="input-field"
                           id="Depositor"
                           name="Depositor"
                           ng-model="depositFormdata.Depositor"
                           ng-minlength="2"
                           ng-maxlength="10"
                           ng-pattern="/^[A-z][A-z]*$/"
                           required />
                    <span ng-show="depositForm.Depositor.$error.required && depositForm.Depositor.$dirty" class="error"><?$translate->__('ฟิลด์นี้จะต้อง');?></span>
                    <span ng-show="depositForm.Depositor.$error.pattern && depositForm.Depositor.$dirty" class="error"><?$translate->__('2 ถึง 10 อักษร');?></span>
                    <span ng-show="depositForm.Depositor.$error.minlength || depositForm.Depositor.$error.maxlength" class="error"><?$translate->__('2 ถึง 10 อักษร');?></span>
                </div>

                <div class="form-notes text-center">
                </div>
                <button class="btn btn-lg btn-gold" ng-disabled="depositForm.$invalid" onclick="moneyProcess('#deposit-form-help2pay',this);"><?$translate->__('ฝาก');?></button>
            </form>
        </div>
    </section>

    <!--Withdraw-->
    <section id="withdraw-container" class="page-content">
        <h1><?$translate->__('ถอน');?></h1>
        <div class="page-form">
            <form id="withdrawal-form" name="withdrawForm" novalidate ng-controller="WithdrawCtrl">
                <div class="row-form">
                    <label><?$translate->__('เลือกเกม');?></label>
                    <select class="input-field"
                            name="Wallet"
                            id="Wallet"
                            ng-model="withdrawFormData.Wallet"
                            required>
                        <option default><?$translate->__('กรุณาเลือกเกม');?></option>
                        <!--                                <option value="">AFB Sports</option>
                                    <option value="">Playtech</option>
                                    <option value=""><?/*$translate->__('ผลการแข่งขัน');*/?></option>-->
                        <? foreach ($variables['gameText'] as $k => $v) { ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                        <? } ?>
                    </select>
                    <span ng-show="withdrawForm.Wallet.$error.required && withdrawForm.Wallet.$dirty" class="error"><?$translate->__('กรุณาเลือกเกม');?></span>
                </div>

                <div class="row-form">
                    <label><?$translate->__('จำนวนเงินที่ถอน');?></label>
                    <input type="text" placeholder="0" class="input-field text-right"
                           id="Amount"
                           name="Amount"
                           ng-model="withdrawFormData.Amount"
                           required />
                    <span ng-show="withdrawForm.Amount.$error.required && withdrawForm.Amount.$dirty" class="error"><?$translate->__('กรุณากรอกจำนวนเงิน');?></span>
                </div>

                <div class="row-form">
                    <label class="lbl-acctno"><?$translate->__('ธนาคาร');?></label>
                    <select name="BankCode" class="input-field">
                        <option value="" default><?$translate->__('กรุณาเลือกธนาคาร');?></option>
                        <? foreach ($variables['bank'] as $k => $v) { ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                        <? } ?>
                    </select>
                    <input type="text" placeholder="<?$translate->__('เลขที่บัญชี');?>" class="input-field input-acctno"
                           id="AccountNumber"
                           name="AccountNumber"
                           ng-model="withdrawFormData.AccountNumber"
                           ng-minlength="2"
                           ng-maxlength="20"
                           ng-pattern="/^[0-9]*$/"
                           required />
                    <span ng-show="!withdrawForm.AccountNumber.$error.required && (withdrawForm.AccountNumber.$error.minlength ||
                                        withdrawForm.AccountNumber.$error.maxlength) && withdrawForm.AccountNumber.$dirty "
                          class="error"><?$translate->__('เลขที่บัญชีไม่ถูกต้อง');?></span>
                    <span ng-show="withdrawForm.AccountNumber.$error.pattern && withdrawForm.AccountNumber.$dirty"
                          class="error"><?$translate->__('ใส่หมายเลขที่ถูกต้องเท่านั้น');?></span>
                    <div class="clear"></div>
                </div>

                <div class="row-form">
                    <label><?$translate->__('ชื่อบัญชี');?></label>
                    <input type="text" class="input-field" placeholder="<?$translate->__('2 ถึง 10 อักษร');?>"
                        id="BankAccount"
                        name="BankAccount"
                        ng-model="withdrawFormData.BankAccount"
                        ng-minlength="4"
                        ng-maxlength="20"
                        required />
                    <span ng-show="withdrawForm.BankAccount.$error.required && withdrawForm.BankAccount.$dirty" class="error"><?$translate->__('ฟิลด์นี้จะต้อง');?></span>
                    <span ng-show="withdrawForm.BankAccount.$error.pattern && withdrawForm.BankAccount.$dirty" class="error"><?$translate->__('4 ถึง 20 อักษร');?></span>
                    <span ng-show="withdrawForm.BankAccount.$error.minlength || withdrawForm.BankAccount.$error.maxlength" class="error"><?$translate->__('4 ถึง 20 อักษร');?></span>
                </div>

                <div class="row-form">
                    <label><?$translate->__('หมายเลขโทรศัพท์');?></label>
                    <input type="text" placeholder="000-0000-0000" maxlength="13"
                           id="MemberPhone"
                           name="MemberPhone"
                           ng-minlength="8"
                           ng-maxlength="13"
                           ng-pattern='/^\d{4}-\d{4}|[0-9]$/'
                           ng-model="withdrawFormData.MemberPhone"
                           required/>
                    <span ng-show="!withdrawForm.MemberPhone.$error.required && (withdrawForm.MemberPhone.$error.minlength ||
                                        withdrawForm.MemberPhone.$error.maxlength) && withdrawForm.MemberPhone.$dirty "
                                        class="error"><?$translate->__('หมายเลขโทรศัพท์ที่ไม่ถูกต้อง');?></span>
                    <span ng-show="withdrawForm.MemberPhone.$error.pattern && withdrawForm.MemberPhone.$dirty"
                                        class="error"><?$translate->__('ใส่หมายเลขที่ถูกต้องเท่านั้น');?></span>
                </div>

                <div class="row-form">
                    <label><?$translate->__('แสดงความคิดเห็น');?></label>
                    <textarea name="UserMemo" placeholder="<?$translate->__('ไม่เกิน 300 ตัวอักษร');?>" class="input-field"></textarea>
                </div>

                <div class="form-notes text-center">
                    <?$translate->__('ในกรณีที่ใช้หลายชื่อในการทำรายการ จะต้องมีการตรวจสอบเพื่อความปลอดภัย');?>.
                </div>
                <button class="btn btn-lg btn-gold" ng-disabled="withdrawForm.$invalid" onclick="moneyProcess('#withdrawal-form',this);"><?$translate->__('ถอน');?></button>
            </form>
        </div>
    </section>

    <!--Change Password-->
    <section id="changepass-container" class="page-content">
        <h1><?$translate->__('เปลี่ยนรหัสผ่าน');?></h1>
        <div class="page-form">
            <form id="settings-form" role="form">
                <div class="row-form">
                    <label><?$translate->__('รหัสผ่านปัจจุบัน');?></label><!--Current Password-->
                    <input type="password" placeholder="" name="CurrentPassword" class="input-field">
                </div>

                <div class="row-form">
                    <label><?$translate->__('รหัสผ่านใหม่');?></label><!--New Password-->
                    <input type="password" placeholder="" name="ChangePassword"  class="input-field">
                </div>

                <div class="row-form">
                    <label><?$translate->__('ยืนยันรหัสผ่านใหม่');?></label><!--Confirm New Password-->
                    <input type="password" placeholder="" name="ValidChangePassword" class="input-field">
                </div>

                <button class="btn btn-lg btn-gold" onclick="moneyProcess('#settings-form',this)"><?$translate->__('เปลี่ยนรหัสผ่าน');?></button>
            </form>
        </div>
    </section>

    <!--Rules-->
    <section id="rules-container" class="page-content">
        <h1><?$translate->__('กฎระเบียบ');?></h1>
        <div class="page-form">
            <div id="accordion-rules">
                <div class="accordion-rules-header">
                    <h3>
                        <strong><?$translate->__('บาคาร่า');?></strong>
                        <span><?$translate->__('บาคาร่า');?></span>
                        <i class="arrow-down"></i>
                    </h3>
                </div>
                <div class="accordion-rules-body">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                    Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros
                    et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum
                    soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.
                </div>
                <div class="accordion-rules-header">
                    <h3>
                        <strong><?$translate->__('กระบอง');?></strong>
                        <span><?$translate->__('กระบอง');?></span>
                        <i class="arrow-down"></i>
                    </h3>
                </div>
                <div class="accordion-rules-body">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                    Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros
                    et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum
                    soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.
                </div>
                <div class="accordion-rules-header">
                    <h3>
                        <strong><?$translate->__('รูเล็ต');?></strong>
                        <span><?$translate->__('รูเล็ต');?></span>
                        <i class="arrow-down"></i>
                    </h3>
                </div>
                <div class="accordion-rules-body">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                    Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros
                    et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum
                    soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.
                </div>
                <div class="accordion-rules-header">
                    <h3>
                        <strong><?$translate->__('สล็อตแมชชีน');?></strong>
                        <span><?$translate->__('สล็อตแมชชีน');?></span>
                        <i class="arrow-down"></i>
                    </h3>
                </div>
                <div class="accordion-rules-body">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                    Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros
                    et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum
                    soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.
                </div>
            </div>
        </div>
    </section>

    <!--Menu Panel-->
<div class="menu">
    <?if(!isset($_SESSION['MemberID'])){?>

        <div class="my-wallet-locked">
            <p><?$translate->__('กรุณาเข้าสู่ระบบในการเข้าถึงแผงกระเป๋าสตางค์ของคุณ');?>.</p>

            <button class="btn-lg btn-gold menu-btn-login"><?$translate->__('เข้าสู่ระบบ');?></button>
            <button class="btn-lg btn-info menu-btn-signup"><?$translate->__('ลงทะเบียนที่นี่');?></button>

            <div class="clear"></div>
        </div>

    <?}else{?>

        <div class="show-username"><?$translate->__('ยินดีต้อนรับสู่');?>, <strong><?=$_SESSION['MemberID']?></strong>!</div>
        <section class="wallet-content">
            <!--<h5>กระเป๋าสตางค์</h5>-->
            <h1><div class="menu-content"><?$translate->__('กระเป๋าสตางค์');?></div></h1>
            <ul>
                <li><p>Microgaming</p>  <span><em>฿</em><strong class="mg-balance balance"></strong></span></li>
                <li><p>Gameplay</p>     <span><em>฿</em><strong class="opus-balance balance"></strong></span></li>
                <li><p>Asia Gaming</p>  <span><em>฿</em><strong class="ag-balance balance"></strong></span></li>
                <li><p>Ezugi</p>     <span><em>฿</em><strong class="ezugi-balance balance"></strong></span></li>
                <li><p>Gold Deluxe</p>  <span><em>฿</em><strong class="gd-balance balance"></strong></span></li>
                <li><p>Bet Soft</p>     <span><em>฿</em><strong class="bet-balance balance"></strong></span></li>
                <li><p>WFT Sports</p>     <span><em>฿</em><strong class="wft-balance balance"></strong></span></li>
                <li><p>ASC Sports</p>     <span><em>฿</em><strong class="asc-balance balance"></strong></span></li>
                <li class="sidebar-wallet-total">
                    <p><?$translate->__('ทั้งสิ้น');?></p>
                    <span class="total-balance"><em>฿</em><strong class="all-balance balance"></strong></span>
                </li>
            </ul>
            <div class="clear"></div>
        </section>
        <section id="transfer">
            <h1><div class="menu-content"><?$translate->__('การโอนเงิน');?></div></h1>
            <div class="transfer-form">

                <form id="transfer-form" role="form">
                    <div class="row-form">
                        <label><?$translate->__('จาก');?></label>
                        <select class="input-field" name="FromWallet">
                            <option><?$translate->__('เลือกเกม');?></option>
                            <!--                                <option value="">AFB Sports</option>
                                    <option value="">Playtech</option>
                                    <option value=""><?/*$translate->__('ผลการแข่งขัน');*/?></option>-->
                            <? foreach ($variables['gameText'] as $k => $v) { ?>
                                <option value="<?= $k ?>"><?= $v ?></option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="row-form">
                        <label><?$translate->__('จำนวน');?></label>
                        <input type="text"  name="Amount" placeholder="0" class="input-field text-right">
                    </div>

                    <div class="row-form">
                        <label><?$translate->__('ถึง');?></label>
                        <select class="input-field" name="ToWallet">
                            <option><?$translate->__('เลือกเกม');?></option>
                            <!--                                <option value="">AFB Sports</option>
                                    <option value="">Playtech</option>
                                    <option value=""><?/*$translate->__('ผลการแข่งขัน');*/?></option>-->
                            <? foreach ($variables['gameText'] as $k => $v) { ?>
                                <option value="<?= $k ?>"><?= $v ?></option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="row-form">
                        <button class="btn btn-md btn-gold" onclick="moneyProcess('#transfer-form',this);"><?$translate->__('การโอนเงิน');?></button>
                    </div>
                </form>
            </div>
        </section>
        <div class="clear"></div>

        <section id="links">
            <ul>
                <li class="link-deposit"><?$translate->__('ฝาก');?></li>
                <li class="link-withdraw"><?$translate->__('ถอน');?></li>
                <li class="link-rules"><?$translate->__('กฎระเบียบ');?></li>
                <li class="link-changepassword"><?$translate->__('เปลี่ยนรหัสผ่าน');?></li>
                <li class="link-logout btn-info" onclick="logout();"><?$translate->__('ออกจากระบบ');?></li>
            </ul>
        </section>
    <?}?>
    <div class="clear"></div>
</div>


    <!--Banner-->
    <section id="banner"></section>

    <!--Sign Up Button-->
    <section id="promo-banner">
        <p><?php $translate->__('ฝากผ่านระบบออนไลน์หน้าเว็บ รับเพิ่ม 5% สูงสุด 500 บาท'); ?>.</p>
        <!--<button class="btn-signup">빠른 회원가입</button>-->
    </section>


    <!--Main Container-->
    <div class="main-container">
        <!--Categories-->
        <section class="main-nav" id="main-categories">
            <ul>
                <li id="livecasino" onclick="openCasino();" class="active"><?$translate->__('คาสิโน');?></li>
                <li id="slotgames" onclick="openSlot();"  ><?$translate->__('เกมส์สลอส');?></li>
                <li id="sportsbook" onclick="openSport();"><?$translate->__('กีฬา');?></li>
            </ul>
        </section>
        <?if($detect->isAndroidOS()){?>
        <!--Live Casino-->
        <section class="game-content" id="livecasino-content">
            <div class="game-buttons"
                 <?if(!isset($_SESSION['MemberID'])){?>
                 onclick="window.open('http://mobile-resigner.valueactive.eu/launch88livedealer/apk?btag1=72918431&btag3=280','_blank');"
                 <?}else{?>
                <?}?>
                >
                <h2>Microgaming</h2>
                <p><?$translate->__('คำอธิบายคาสิโนมือถือที่นี่');?></p>
                <img src="images/btn-gamebutton1.png" />
            </div>
            <div class="game-buttons" onclick="location.href='https://12462d174bcda1002bbc-8c0924250c581678c5d572d3d1df2dbf.ssl.cf6.rackcdn.com/app-citi280-release.apk'" >
                <h2>Gameplay</h2>
                <p><?$translate->__('คำอธิบายคาสิโนมือถือที่นี่');?></p>
                <img src="images/btn-gamebutton2.png"  />
            </div>
            <div class="game-buttons" onclick="window.open('http://agin.cc','_blank');">
                <h2>Asia Gaming</h2>
                <p><?$translate->__('คำอธิบายคาสิโนมือถือที่นี่');?></p>
                <img src="images/btn-gamebutton3.png" />
            </div>
            <div class="game-buttons" onclick="playGame('1002','live','','');" >
                <h2>Gold Deluxe</h2>
                <p><?$translate->__('คำอธิบายคาสิโนมือถือที่นี่');?></p>
                <img src="images/btn-gamebutton4.png" />
            </div>
            <div class="clear"></div>
        </section>
        <?}else if($detect->isiOS()){?>
            <!--Live Casino-->
            <section class="game-content" id="livecasino-content">
                <div class="game-buttons" onclick="window.open('http://agin.cc','_blank');">
                    <h2>Asia Gaming</h2>
                    <img src="images/btn-gamebutton3.png" />
                </div>
                <div class="game-buttons" onclick="playGame('1002','live','','');">
                    <h2>Gold Deluxe</h2>
                    <img src="images/btn-gamebutton4.png" />
                </div>
                <div class="clear"></div>
            </section>
        <?}?>
        <!--Slot Games-->
        <section class="game-content" id="slots-content">
            <div class="cd-tabs">
                <nav>
                    <ul class="cd-tabs-navigation">
                        <li><a  class="tab-but tab-but-1 tab-active" href="#">Microgaming</a></li>
<!--                        <li><a data-content="slot-asiagaming" href="#0">Asia Gaming</a></li>
                        <li><a data-content="slot-golddeluxe" href="#0">Gold Deluxe</a></li>-->
                        <li><a class="tab-but tab-but-2" href="#">Gameplay</a></li>
                        <li><a class="tab-but tab-but-3" href="#">BetSoft</a></li>
                    </ul>
                </nav>

                              <ul class="cd-tabs-content">
                                    <li class="tab-content-1">

                                    </li>
                </ul>
                <div class="clear"></div>
            </div>
        </section>

        <!--Sportsbook-->
        <section class="game-content" id="sports-content">
            <div class="text-center">
                <?$translate->__('เร็ว ๆ นี้');?>!
            </div>
        </section>

        <!--Footer-->
        <footer>
            <section id="copyright">
                <div class="container text-center">
                    <?$translate->__('ลิขสิทธิ์');?> &copy; 2015. <strong>UEFA168.com</strong> <span><?$translate->__('สงวนลิขสิทธิ์');?>.</span>
                </div>
            </section>
        </footer>
    </div>

    <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="js/angular.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>

    <script type="text/javascript" src="js/custom-angular.js"></script>
    <script type="text/javascript" src="js/modernizr.js"></script> <!--Modernizr - Slot Sub Horizontal Menu Scrollbar-->
    <script type="text/javascript" src="js/jquery-outline-1.5.js"></script> <!--Text Outline-->
    <script type="text/javascript" src="js/custom.js"></script>

<script type="text/javascript">
        //jQuery.noConflict();

        //Tabs
/*       var tabs=new ddtabcontent("login");
        tabs.setpersist(true);
        tabs.setselectedClassTarget("link");
        tabs.init();*/
        function signUp(){
            var url="/prc/sign_up.php";
            jQuery.post(url,jQuery('#cd-form').serialize(),function(data){
                if(data.result == 1){
                    alert(data.message);
                    document.location="/mobile/";
                }else{
                    alert(data.message);
                }
            },"json");
        }

        var try_login = false;
        function login() {
            var beforeText = $("#hiddenLogin").val();

            if(beforeText=="LOGIN") {
                if (!try_login) {
                    $(".btnLogin").val("<?$translate->__('อยู่ระหว่างดำเนินการ');?>");
                    try_login = true;
                    var url = "/prc/login.php";
                    var login_form = "#login-form";
                    $.post(url, $(login_form).serialize(), function (data) {
//                        alert("로그인시도");
                        if (data.result != 0) {
                            try_login = false;
                            location.href = "/mobile/";
                        } else {
                            try_login = false;
                            alert(data.message);
                        }
                    }, "json").done(function(){
                        $(".btnLogin").val(beforeText);
                    });
                }
            }else{
                alert("<?$translate->__('ที่รอดำเนินการขั้นตอน');?>.");
            }
        }

        function check_session(){
            $.getJSON('/prc/get_member_info.php', {
                returnformat: 'json'
            },function(data){
                if(data.result != 1){
                    if(data.result == 207){
                        var url="/prc/logout.php";
                        $.post(url,function(data){
                            if(data.result == 1){
                                alert("<?$translate->__('บัญชีของคุณได้รับการออกจากระบบ');?>.");
                                window.location.href="/";
                            }else{
                                alert(data.message);
                            }
                        },"json");
                    }else{
                        alert(data.message);
                        window.location.href="/";
                    }
                }
            });
        }

        function getAllGameBalance(){
            $(".opus-balance").text("<?$translate->__('โหลด');?>..");
            $(".gd-balance").text("<?$translate->__('โหลด');?>..");
            $(".ag-balance").text("<?$translate->__('โหลด');?>..");
            $(".mg-balance").text("<?$translate->__('โหลด');?>..");
            $(".bet-balance").text("<?$translate->__('โหลด');?>..");
            $(".moon-balance").text("<?$translate->__('โหลด');?>..");
            $(".ho-balance").text("<?$translate->__('โหลด');?>..");
            $(".ezugi-balance").text("<?$translate->__('โหลด');?>..");
            $(".asc-balance").text("<?$translate->__('โหลด');?>..");
            $(".wft-balance").text("<?$translate->__('โหลด');?>..");
            $(".xtd-balance").text("<?$translate->__('โหลด');?>..");
            $(".all-balance").text("<?$translate->__('โหลด');?>..");

            $.getJSON('/prc/check_member_balance_parall.php', {
                returnformat: 'json'
            }, function(data) {

                $(".mg-balance").text(data.list[1005].Balance);
                $(".opus-balance").text(data.list[1000].Balance);
                $(".gd-balance").text(data.list[1002].Balance);
                $(".ag-balance").text(data.list[1012].Balance);
                $(".bet-balance").text(data.list[1004].Balance);
                $(".ezugi-balance").text(data.list[1014].Balance);
                $(".asc-balance").text(data.list[1016].Balance);
                $(".wft-balance").text(data.list[1015].Balance);
//        $(".xtd-balance").text(data.list[1009].Balance);
//        $(".moon-balance").text(data.list[1003].Balance);
//        $(".ho-balance").text(data.list[1013].Balance);
                $(".all-balance").text(data.list['All'].Balance);

            });
        }

        function moneyProcess(form,self){

            var url="";
            var beforeValueName;
            var message="";

            if($(self).val() == "Processing"){
                alert("<?$translate->__('ที่รอดำเนินการขั้นตอน');?>");
                return 0;
            }

            beforeValueName = $(self).val();
            $(self).val("<?$translate->__('การประมวลผล');?>");
            //alert($(self).val()); exit;

            if(form == "#transfer-form" || form == "#transfer-form-sidebar"){
                url="/prc/transfer.php";
            }else if(form == "#deposit-form" || form == "#deposit-form-help2pay"){
                url="/prc/deposit.php";
            }else if(form == "#withdrawal-form"){
                url="/prc/withdrawal.php";
            }else if(form == "#comp-form"){
                url="/prc/use_comp.php";
            }else if(form == "#coupon-form"){
                url="/prc/use_coupon.php";
            }else if(form == "#settings-form"){
                url="/prc/change_password.php";
            }

            var money_result=0;
            $.post(url,$(form).serialize(),function(data){
                message = data.message;
                money_result = data.result;
                if(money_result == 1){
                    alert(message);
                    resetForm();
                }else{
                    alert(message);
                }
            },"json").done(function(){
                $(self).val(beforeValueName);
                if(money_result){
                    getAllGameBalance();
                    load_comp();
                }
            });
        }

        function checkid(obj) {
            var pattern = /(^[ㄱ-ㅎ가-힣]*$)/;
            if(pattern.test($(obj).val()) && $(obj).val().length >= 1){
                alert('Please enter alphabets only.');
                $(obj).val('');
                return 0;
            }

            if ($(obj).val().length > 3) {

              var url = "/prc/check_client.php";

                $.post(url,$('#cd-form').serialize(),function(data){
                    var result = data.result;
                    var message = data.message;
                    var msg_id = data.message_id;
                    if (result == "1") {
                        $("#msg_id").addClass("valid");
                    }else{
                        $("#msg_id").removeClass("valid");
                        $("#msg_id").addClass("error");
                    }
                    $("#msg_id").html(message);
                },"json");

            }

        }

        function resetForm(){
            $('select').find('option:first').attr('selected', 'selected');
            $('textarea').val('');
            $(".game-balance").text("Please select game.");
            $("input").not('.btn-form,:input[type=submit],:input[type=button]').val('');
            $('input[type=text]').val('');
        }

        function playGame(game,type,code,view,name) {
            <?if(isset($_SESSION['MemberID'])){
                if($detect->isiOS()){
                   if ($detect->is('Chrome')){
                   ?>
            alert('Please use safari web browser to launch the game');
            return 0;
            <?}
        }
        ?>
            if(type=="live" || type=="fun" || type=="slot" || type=="playcheck"){
                var target;
                if(type == "slot" && game != 1005){
                    target=game+Math.random();
                }else{
                    target=game;
                }

                if(type=="playcheck"){
                    target = 'playcheck';
                }

                if(game==1012){
                    var pop = window.open('about:blank',target,'width=1228,height=741,location=0,menubar=0,resizable=1,scrollbars=0,status=0,titlebar=0,toolbar=0').focus();
                }else if(game==1014){
                    var pop = window.open('about:blank',target,'width=950,height=610,location=0,menubar=0,resizable=1,scrollbars=0,status=0,titlebar=0,toolbar=0').focus();
                }else{
                    var pop = window.open('about:blank',target,'width=1024,height=768,location=0,menubar=0,resizable=1,scrollbars=0,status=0,titlebar=0,toolbar=0').focus();
                }

                document.getElementById('frm_game').target = target;
                document.getElementById('_game').value = game;
                document.getElementById('_type').value = type;
                document.getElementById('_code').value = code;
                document.getElementById('_view').value = view;
                if(name  === undefined){

                }else{
                    document.getElementById('_name').value = name;
                }
                //    document.getElementById('frm_game').method = 'post';
                document.getElementById('frm_game').submit();
            }else{
                jQuery('.'+view).show();
                document.getElementById('frm_game').target = view;
                document.getElementById('_game').value = game;
                document.getElementById('_type').value = type;
                document.getElementById('_code').value = code;
                document.getElementById('frm_game').submit();
            }
            <?}else{?>
            alert('Please Login.');
            return false;
            <?}?>
        }



        jQuery(document).ready(function($){
//       console.log(slot);


            $(".view-desktop").click(function(e){
                e.preventDefault();
                var url="/prc/select_view.php?view=desktop";
                $.post(url,function(){{document.location="/";}});
            });


            $(".view-desktop").click(function(e){
                e.preventDefault();
                var url="/prc/select_view.php?view=desktop";
                $.post(url,function(){{document.location="/";}});
            });

            $(".btnLogin").click(function(e){
                e.preventDefault();
                login();
            });


            $(".btnDeposit,.linkDeposit").click(function(e){
                e.preventDefault();

                $(".depositForm").show();
                $(".withdrawForm").hide();
                $(".index").hide();
            });


            $(".btnWithdraw,.linkWithdraw").click(function(e){
                e.preventDefault();
                $(".withdrawForm").show();
                $(".depositForm").hide();
                $(".index").hide();
            });

            $("a.closeForm").on("click",function(e){
                if($(".withdrawForm").is(":visible")){
                    $(".withdrawForm").hide();
                }else if($(".depositForm").is(":visible")){
                    $(".depositForm").hide();
                }
                $(".index").show();
            });


            <?if(isset($_SESSION["MemberID"])){?>
            setInterval(check_session,5000);
            getAllGameBalance();
            <?}?>
            $(".linkDeposit").click(function(){
                $("#msg-accessdenied").css("display", "block");
            });

        });

    </script>
</body>
</html>
