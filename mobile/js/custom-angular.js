var app = angular.module('uefaApp', []);

app.controller('SignUpController', ['$scope', function($scope) {
    $scope.submitted = false;
    $scope.submit = function() {
        if ($scope.user_form.$valid) {
            //Submit as normal
        } else {
            //don't submit
        }
    };
}]);

app.controller('DepositCtrl', ['$scope', function($scope) {
    $scope.submitted = false;
    $scope.submit = function() {
        if ($scope.user_form.$valid) {
            //Submit as normal
        } else {
            //don't submit
        }
    };
}]);

app.controller('WithdrawCtrl', ['$scope', function($scope) {
    $scope.submitted = false;
    $scope.submit = function() {
        if ($scope.user_form.$valid) {
            //Submit as normal
        } else {
            //don't submit
        }
    };
}]);

//Signup
app.directive('validPasswordC', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            var original;
            ctrl.$formatters.unshift(function (modelValue) {
                original = modelValue;
                return modelValue;
            });

            ctrl.$parsers.push(function(viewValue){
                var noMatch = viewValue != scope.signUpForm.MemberPwd.$viewValue;
                console.log(noMatch);
                ctrl.$setValidity('noMatch', !noMatch);

                return viewValue;
            });
        }
    }
});

//Tabs
app.controller("TabController", function() {
    this.tab = 1;

    this.isSet = function(checkTab) {
        return this.tab === checkTab;
    };

    this.setTab = function(setTab) {
        this.tab = setTab;
    };
});