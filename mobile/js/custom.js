//Preloader
$(window).load(function() {
    $("#status").fadeOut();
    $("#preloader").delay(400).fadeOut("slow");
});


//Categories
function openCasino(){
    $("#livecasino-content").show();
    $("#slots-content").hide();
    $("#sports-content").hide();

    $("#main-categories li#livecasino").addClass("active");
    $("#main-categories li#slotgames").removeClass("active");
    $("#main-categories li#sportsbook").removeClass("active");
}

var slot="";
function openSlot(){
    $("#livecasino-content").hide();
    $("#sports-content").hide();
    $("#slots-content").show();

    $("#main-categories li#livecasino").removeClass("active");
    $("#main-categories li#slotgames").addClass("active");
    $("#main-categories li#sportsbook").removeClass("active");

    if(slot === ""){

        $(".cd-tabs-content").show();
        $(".tab-content-1").show();

        $.get("/prc/mobile_slot.php",function(data) {
            slot = $.parseJSON(data);
        }).done(function(){
            loadSlot(1005);
        });

    }

}

function openSport(){
    $("#livecasino-content").hide();
    $("#slots-content").hide();
    $("#sports-content").show();

    $("#main-categories li#livecasino").removeClass("active");
    $("#main-categories li#slotgames").removeClass("active");
    $("#main-categories li#sportsbook").addClass("active");
}

function logout(){
    var url="/prc/logout.php";
    $.post(url,function(data){
        if(data.result == 1){
            location.href="/mobile/";
        }else{
            alert(data.message);
        }
    },"json");
}

$(document).ready(function() {

    function loadSlot(game){
        $(".tab-content-1").html("");
        var slot_list ="";
        var i=0;
        slot_list+="<div id=\"slotItems\">";
        for(i = 0 ;i<slot[game].length;i++) {
            if(game==1000){
                slot_list+=	"<a class=\"slotItemBox\" onclick=\"playGame('"+game+"','slot','"+slot[game][i].GameCode+"','Mobile')\">"+
                "<img src='"+slot[game][i].image+"'  alt='"+slot[game][i].GameName+"'>";
            }else if(game==1005){
                slot_list+=	"<a class=\"slotItemBox\" onclick=\"playGame('"+game+"','slot','"+slot[game][i].GameCode+"','Mobile')\">"+
                "<img src='"+slot[game][i].image+"' style='width:200%' alt='"+slot[game][i].GameName+"'>";
            }else if(game==1004){
                slot_list+=	"<a class=\"slotItemBox betsoft-item\"  onclick=\"playGame('"+game+"','slot','"+slot[game][i].GameCode+"','2d')\">"+
                "<img src='"+slot[game][i].image+"' alt='"+slot[game][i].GameName+"'>";
            }
            slot_list+="<span class=\"slotTitle\">"+slot[game][i].GameName+"</span>"+
            "</a>";
        }
        slot_list+="<div class='clear'></div>";
        slot_list+="</div>";

        $(".tab-content-1").html(slot_list);

    }

/*
    $("#MemberAge").change(function(){
        if ($("#MemberAge option:selected").text() <= 1997) {
            $('.btn-over18').addClass('btn-green');
        }else{
            $('.btn-over18').removeClass('btn-green');
        }
    });
*/

    //Signup Form Terms
/*    $('.btn-terms').on('click touchstart', function () {
        $('.error-terms .ng-hide').addClass('btn-green');
        //$('.error-terms .ng-hide').toggleClass('btn-green');
    });*/

    $('#slotgames').click(function(){
        $('.tab-but-1').click();
    });

    $('.tab-but-1').click(function(){
        $('.tab-but').removeClass('tab-active');
        $('.tab-but-1').addClass('tab-active');
        /*$('footer #copyright').hide();*/
        loadSlot(1005);
        return false;
    });

    $('.tab-but-2').click(function(){
        $('.tab-but').removeClass('tab-active');
        $('.tab-but-2').addClass('tab-active');
       /* $('footer #copyright').hide();*/
        loadSlot(1000);
        return false;
    });

    $('.tab-but-3').click(function(){
        $('.tab-but').removeClass('tab-active');
        $('.tab-but-3').addClass('tab-active');
        $('.tab-content').hide(100);

        /*$('footer #copyright').hide();*/
        loadSlot(1004);
        return false;
    });

    $(".btn-login").show();
    $(".btn-wallet").hide();

    $(".game-buttons h2").outlineLetters({color: '#000000', size: 2});
    $(".game-buttons p").outlineLetters({color: '#000000', size: 1});

    //Language Option
    $(".btn-lang").click(function(){
        $("#lang-list").slideToggle('fast');
        $(this).toggleClass('lang-active-selected');
    });

    //Login
    $('.btn-login, .menu-btn-login').click(function(){
        $(".menu, #banner, #main-categories, .main-container, #promo-banner, #signup-container, .forgotpass-container, #deposit-container, #withdraw-container, #changepass-container").hide();
        $(".login-container").show();
        $(".btn-menu-open").hide();
        $(".btn-menu-close").show();
    });

    //Signup
    $('.menu-btn-signup, .btn-signup').click(function(){
        $(".menu, #banner, #main-categories, .main-container, #promo-banner, .login-container, .forgotpass-container, #deposit-container, #withdraw-container, #changepass-container").hide();
        $("#signup-container").show();
        $(".btn-menu-open").hide();
        $(".btn-menu-close").show();
    });

    //Forgot Password
    $('.btn-forgotpass').click(function(){
        $(".menu, #banner, #main-categories, .main-container, #promo-banner, .login-container, #signup-container, #deposit-container, #withdraw-container, #changepass-container").hide();
        $(".forgotpass-container").show();
        $(".btn-menu-open").hide();
        $(".btn-menu-close").show();
    });

    //Home
    $('.logo img').click(function() {
        $("#banner, #main-categories, .main-container, #promo-banner").show();
        $(".login-container, #signup-container, .forgotpass-container, #deposit-container, #withdraw-container, #changepass-container, #rules-container, .menu").hide();
        $("body").css("background-color","#0f0f0f");
        $(".btn-menu-open").show();
        $(".btn-menu-close").hide();
        $('.btn-menu-open').removeClass('btn-menu-close-bg');
    });
    $('.btn-menu-close').click(function() {
        $("#banner, #main-categories, .main-container, #promo-banner").show();
        $(".login-container, #signup-container, .forgotpass-container, #deposit-container, #withdraw-container, #changepass-container, #rules-container, .menu").hide();
        $("body").css("background-color","#0f0f0f");
        $(".btn-menu-open").show();
        $(".btn-menu-close").hide();
        $('.btn-menu-open').removeClass('btn-menu-close-bg');
    });

    //Menu
    $('.btn-menu-open').click(function() {
        $(".menu").show();
        $("#banner, #main-categories, .main-container, #promo-banner").hide();
        $(".login-container, #signup-container, .forgotpass-container, #deposit-container, #withdraw-container, #changepass-container, #rules-container").hide();
        $("body").css("background-color","#0f0f0f");
        $(".btn-menu-close").show();
        $(".btn-menu-open").hide();
        //$(".btn-menu-close").addClass('btn-menu-close-bg');
    });

    //Deposit
    $('.link-deposit').click(function() {
        $(".menu, #banner, #main-categories, .main-container, #promo-banner").hide();
        $("#deposit-container").show();
        $("body").css("background-color","#0f0f0f");
        $(".btn-menu-open").hide();
        $(".btn-menu-close").show();
        $('.btn-menu-open').toggleClass('btn-menu-close-bg');
    });

    //Withdraw
    $('.link-withdraw').click(function() {
        $(".menu, #banner, #main-categories, .main-container, #promo-banner").hide();
        $("#withdraw-container").show();
        $("body").css("background-color","#0f0f0f");
        $(".btn-menu-open").hide();
        $(".btn-menu-close").show();
        $('.btn-menu-open').toggleClass('btn-menu-close-bg');
    });

    //Change Password
    $('.link-changepassword').click(function() {
        $(".menu, #banner, #main-categories, .main-container, #promo-banner").hide();
        $("#changepass-container").show();
        $("body").css("background-color","#0f0f0f");
        $(".btn-menu-open").hide();
        $(".btn-menu-close").show();
        $('.btn-menu-open').toggleClass('btn-menu-close-bg');
    });

    //Rules
    $('.link-rules').click(function() {
        $(".menu, #banner, #main-categories, .main-container, #promo-banner").hide();
        $("#rules-container").show();
        $("body").css("background-color","#0f0f0f");
        $(".btn-menu-open").hide();
        $(".btn-menu-close").show();
        $('.btn-menu-open').toggleClass('btn-menu-close-bg');
    });

    /*$('.btn-login').click(function(){
     $('.login-container').show();
     $('.login-container').animate({top: 0}, 800);
     });
     $('.btn-wallet').click(function(){
     $('.wallet-container').show();
     $('.wallet-container').animate({top: 0}, 800);
     });
     $('.slide-content-close').click(function(){
     $('.login-container').animate({top: -315}, 800);
     $('.wallet-container').animate({top: -435}, 800);
     });*/

    $("#livecasino-content").show();
    $("#slots-content").hide();
    $("#sports-content").hide();

    //Signup Form Terms
    $('.btn-terms').click(function(){
        $(this).toggleClass('btn-green');
        if ($('.btn-terms').hasClass('btn-green')) {
            $('#MemberTerms').text('Y');
        }else{
            $('#MemberTerms').text('N');
        }
    });

/*    var myselect = document.getElementById("ageInYear");
    for (i=2000; i>=1915;i--){
        myselect.add(new Option(i),null);
    }*/

/*    $('#18yrs').hide();
    $("#ageInYear").change(function(){
        if ($("#ageInYear option:selected").text() <= "1997") {
            $('.btn-over18').addClass('btn-green');
            $('#18yrs').hide();
        }
        else if ($("#ageInYear option:selected").text() == "Select Year") {
            $('#18yrs').hide();
        }
        else {
            $('.btn-over18').removeClass('btn-green');
            $('#18yrs').show();
        }
    });*/

/*
    $('.btn-over18').click(function(){
        $(this).toggleClass('btn-green');
        if ($('.btn-over18').hasClass('btn-green')) {
            $('#MemberOlder18').text('Y');
        }else{
            $('#MemberOlder18').text('N');
        }

        if ($("#ageInYear option:selected").text() >= "1997") {
            $('#18yrs').show();
            $('.btn-over18').removeClass('btn-green');
        }
    });
*/

    //Accordion - Rules Page
    $("#accordion-rules").accordion({
        heightStyle: "content",
        active: 0,
        collapsible: true,
        header: "div.accordion-rules-header"
    });

    //Scrollable Menu
    var tabItems = $('.cd-tabs-navigation a'),
        tabContentWrapper = $('.cd-tabs-content');

    tabItems.on('click', function(event){
        event.preventDefault();
        var selectedItem = $(this);
        if( !selectedItem.hasClass('selected') ) {
            var selectedTab = selectedItem.data('content'),
                selectedContent = tabContentWrapper.find('li[data-content="'+selectedTab+'"]'),
                slectedContentHeight = selectedContent.innerHeight();

            tabItems.removeClass('selected');
            selectedItem.addClass('selected');
            selectedContent.addClass('selected').siblings('li').removeClass('selected');
            //animate tabContentWrapper height when content changes
            tabContentWrapper.animate({
                'height': slectedContentHeight
            }, 200);
        }
    });

    //hide the .cd-tabs::after element when tabbed navigation has scrolled to the end (mobile version)
    checkScrolling($('.cd-tabs nav'));
    $(window).on('resize', function(){
        checkScrolling($('.cd-tabs nav'));
        tabContentWrapper.css('height', 'auto');
    });
    $('.cd-tabs nav').on('scroll', function(){
        checkScrolling($(this));
    });

    function checkScrolling(tabs){
        var totalTabWidth = parseInt(tabs.children('.cd-tabs-navigation').width()),
            tabsViewport = parseInt(tabs.width());
        if( tabs.scrollLeft() >= totalTabWidth - tabsViewport) {
            tabs.parent('.cd-tabs').addClass('is-ended');
        } else {
            tabs.parent('.cd-tabs').removeClass('is-ended');
        }
    }

});
