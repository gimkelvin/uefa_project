<?include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php"; ?>
<?
//2. 데이터 GET

$CurrentPassword = trim($_POST["CurrentPassword"]);
$ChangePassword = trim($_POST["ChangePassword"]);
$ValidChangePassword = trim($_POST["ValidChangePassword"]);

//3. 데이터 CHECK
if (!regExp("all", $CurrentPassword, 6, 16)) {
    echo json_encode(array("result"=>0,"message"=>"Please enter current password.","message_id"=>"msg_password1"));
    exit;
}

if (!regExp("all", $ChangePassword, 6, 16)) {
    echo json_encode(array("result"=>0,"message"=>"Please enter 6~16 characters.","message_id"=>"msg_password2"));
    exit;
}

if (!regExp("all", $ValidChangePassword, 6, 16)) {
    echo json_encode(array("result"=>0,"message"=>"Please enter 6~16 characters..","message_id"=>"msg_password3"));
    exit;
}

if ($ChangePassword != $ValidChangePassword) {
    echo json_encode(array("result"=>0,"message"=>"Please confirm your password.","message_id"=>"msg_password3"));
    exit;
}

if ($CurrentPassword == $ChangePassword) {
    echo json_encode(array("result"=>0,"message"=>"New password is identical as the old password.","message_id"=>"msg_password2"));
    exit;
}

$param = array(
    "MemberID"=>$_SESSION["MemberID"],
    "MemberToken"=>$_SESSION["MemberToken"],
    "CurrentPassword"=>hash('sha256',$CurrentPassword),
    "ChangePassword"=>hash('sha256',$ChangePassword),
    "MemberIP"=>$_SERVER['REMOTE_ADDR'],
    'VisiterURL' => $_SERVER['HTTP_HOST']

);


$rst=ReqeustAPI::call("ChangePassword",$param, null);

if ($rst[0] == 200) {
    if($rst[1]->ErrorCode != 0){
        $result = 0;
        $message = ReqeustAPI::errorCode($rst[1]->ErrorCode);
    }else{
        $result = 1;
        $message = "Password has been changed successfully.";
    }
} else{
    $result = 0;
    $message = ReqeustAPI::errorCode($rst[0]);
}

echo json_encode(array("result"=>$result,"message"=>$message,"message_id"=>"alert"));

