<?include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php";

//echo $_POST["MemberID"];
$param = array('MemberID' => $_POST["MemberID"]);
$rst = ReqeustAPI::call('CheckClient', $param, null);

if ($rst[0] == 200) {
    if($rst[1]->ErrorCode != 0){
        $result = 0;
        $message = ReqeustAPI::errorCode($rst[1]->ErrorCode);

        if($_GET['lang'] == "english"){
            echo json_encode(array("result"=>0,"message"=>"This ID is already in use.","message_id"=>"msg_id"));
        }else{
            echo json_encode(array("result"=>0,"message"=>"ID นี้มีอยู่แล้วในการใช้งาน.","message_id"=>"msg_id"));
        }

        exit;

    }else{

        if($_GET['lang'] == "english"){
            echo json_encode(array("result"=>1,"message"=>"ID is available.","message_id"=>"msg_id"));
        }else{
            echo json_encode(array("result"=>1,"message"=>"ID สามารถใช้ได้.","message_id"=>"msg_id"));
        }

        exit;
    }
} else{
    $result = 0;
    $message = ReqeustAPI::errorCode($rst[0]);
}

echo json_encode(array("result"=>$result,"message"=>$message,"message_id"=>"alert"));


