<?include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php";

$Wallet= trim($_POST["Wallet"]);
$MemberPhone = str_replace("-","",trim($_POST["MemberPhone"]));
$Amount =  trim($_POST["Amount"]);
$UserMemo =  htmlspecialchars(trim($_POST["UserMemo"]));
$Depositor =  htmlspecialchars(trim($_POST["Depositor"]));
$DepositType =  htmlspecialchars(trim($_POST["DepositType"]));
$DepositDate =  htmlspecialchars(trim($_POST["DepositDate"]));

//3. 데이터 CHECK
if($Wallet == ""){

    echo json_encode(array("result"=>0,"message"=>"Please select game.","message_id"=>"msg_game"));
    exit;
}

if ($MemberPhone == "") {
    echo json_encode(array("result"=>0,"message"=>"Please enter phone number.","message_id"=>"msg_phone"));
    exit;
}

if ($Amount == "") {
    echo json_encode(array("result"=>0,"message"=>"Please enter the amount.","message_id"=>"msg_amount"));
    exit;
}

$Amount = str_replace(",", "", $Amount);

if (!regExp("integer", $Amount)) {
    echo json_encode(array("result"=>0,"message"=>"Please enter numeric value for the amount.","message_id"=>"msg_amount"));
    exit;
}

if ($Amount <= 0) {
    echo json_encode(array("result"=>0,"message"=>"Please enter the correct amount.","message_id"=>"msg_amount"));
    exit;
}

if (!regExp("kor_alpha_num", $_POST["Depositor"], 2, 10)) {
    echo json_encode(array("result"=>0,"message"=>"Please enter the name of Depositor between 2 to 10 characters only.","message_id"=>"msg_name"));
    exit;
}

if (!regExp("all", $UserMemo, 0, 600)) {
    echo json_encode(array("result"=>0,"message"=>"Text is limited to 300 characters.","message_id"=>"msg_comment"));
    exit;
}

$param = array(
    "MemberID"=>$_SESSION["MemberID"],
    "MemberToken"=>$_SESSION["MemberToken"],
    "MemberIP"=>$_SERVER['REMOTE_ADDR'],
    "Depositor"=>$Depositor,
    "MemberPhone"=>$MemberPhone,
    "Amount"=>$Amount,
    "Wallet"=>$Wallet,
    "UserMemo"=>$UserMemo,
    "DepositType"=>$DepositType,
    "DepositDate"=>$DepositDate
);

$rst=ReqeustAPI::call("cDeposit",$param, null);
//var_dump($rst);
if ($rst[0] == 200) {
    if($rst[1]->ErrorCode != 0){
        $result = 0;
        $message = ReqeustAPI::errorCode($rst[1]->ErrorCode);
    }else{
        $result = 1;
        $message = "Deposit request has been submitted.";
    }
} else{
    $result = 0;
    $message = ReqeustAPI::errorCode($rst[0]);
}

echo json_encode(array("result"=>$result,"message"=>$message,"message_id"=>"alert"));

