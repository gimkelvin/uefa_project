<?include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php"; ?>
<?

//if (!preg_match("/http:\/\/(.*?)\/pages\/deposit/", $_SERVER['HTTP_REFERER'], $match)) {
//    exit;
//}
//$domain = $match[1];

//2. 데이터 GET
$Payment = trim($_POST["Payment"]);
$Amount =  trim($_POST["Amount"]);
$BankAccount = trim($_POST["BankAccount"]);
$BankCode = trim($_POST["BankCode"]);
$UserMemo =  htmlspecialchars(trim($_POST["UserMemo"]));
$Wallet= trim($_POST["Wallet"]);

if($Payment != "1001"){
    echo json_encode(array("result"=>0,"message"=>"Please select payment.","message_id"=>"msg_game"));
    exit;
}
//3. 데이터 CHECK
if($Wallet == ""){
    echo json_encode(array("result"=>0,"message"=>"Please select game.","message_id"=>"msg_game"));
    exit;
}

if ($Amount == "") {
    echo json_encode(array("result"=>0,"message"=>"Please enter the amount.","message_id"=>"msg_amount"));
    exit;
}

$Amount = str_replace(",", "", $Amount);

if (!regExp("integer", $Amount)) {
    echo json_encode(array("result"=>0,"message"=>"Please enter numeric value for the amount.","message_id"=>"msg_amount"));
    exit;
}

if ($Amount <= 0) {
    echo json_encode(array("result"=>0,"message"=>"Please enter the correct amount.","message_id"=>"msg_amount"));
    exit;
}

if($BankCode == ""){
    echo json_encode(array("result"=>0,"message"=>"Please select bank.","message_id"=>"msg_game"));
    exit;
}

if (!regExp("kor_alpha_num", $_POST["BankAccount"], 4, 30)) {
    echo json_encode(array("result"=>0,"message"=>"Please enter the name of Depositor between 2~10 characters.","message_id"=>"msg_name"));
    exit;
}

if (!regExp("all", $UserMemo, 0, 600)) {
    echo json_encode(array("result"=>0,"message"=>"Text is limited to 300 characters.","message_id"=>"msg_comment"));
    exit;
}

echo json_encode(array("result"=>1,"message"=>$message,"message_id"=>"alert"));

