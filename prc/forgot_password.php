<?php
include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/include/mailer/PHPMailerAutoload.php";
function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

$password = randomPassword();
$MemberID = $_POST['MemberID'];
$MemberEmail = $_POST['MemberEmail'];


if (!regExp('alphanumeric',"$MemberID",4,16 )) {
    echo json_encode(array("result"=>0,"message"=>"Please confirm username."));
    exit;
}

if (!filter_var($MemberEmail, FILTER_VALIDATE_EMAIL)) {
    echo json_encode(array("result"=>0,"message"=>"Please confirm email."));
    exit;
}


$ValidateKey = sha1(mt_rand(10000,99999).time().$Email);

$param = array("MemberID"=>$MemberID,"MemberEmail"=>$MemberEmail,"ValidateKey"=>$ValidateKey,"Mode"=>"I");
$rst=ReqeustAPI::call("MemberValidate",$param, null);

if($rst[1]->ErrorCode != 0){
    $result = 0;
    $message = ReqeustAPI::errorCode($rst[1]->ErrorCode);
    echo json_encode(array("result"=>0,"message"=>$rst[1]->ErrorCode.$message));
    exit;
}else{
    $param = array("MemberID"=>$MemberID,"ValidateKey"=>$ValidateKey,"ChangePassword"=>hash('sha256',$password));
    $rst=ReqeustAPI::call("vChangePassword",$param, null);

    if($rst[1]->ErrorCode != 0){
        $result = 0;
        $message = ReqeustAPI::errorCode($rst[1]->ErrorCode);
        echo json_encode(array("result"=>0,"message"=>"$message"));
        exit;
    }
}

$mail = new PHPMailer(true);
$mail->IsSMTP();
$mail->Host = "smtp.gmail.com";    // 구글의 서버
$mail->SMTPSecure = 'tls';
$mail->SMTPAuth = true;
$mail->Port = 465;
$mail->SMTPSecure = "ssl";
$mail->Username   = "noreply@UEFA168.com";    // 구글 계정 아이디
$mail->Password   = "noreply8899";          // 구글 계정 암호
//$mail->SMTPDebug  = 2;
$mail->SetFrom('noreply@UEFA168.com', 'UEFA168');
//$mail->CharSet = "euc-"; /// 문자셋
$message  = "
<!doctype html>
<html lang=\"us\">
<head>
	<meta charset=\"utf-8\">
	<style>
		body {font: normal 14px Arial;color: #ffffff;}
		a {color: #ffd052;}
		a:hover {color: #ffe371; text-decoration: underline;}
	</style>
</head>
<body>
<div style=\"width: 100%; min-width: 680px; padding: 10px; margin: 0 auto; background: #0f0f0f;\">
	<div style=\"width: 100%; min-height: 500px; padding: 20px 0; margin: 0 auto; color: #ffffff; background: url(http://5a64792eec13049c7d45-f76bf7a5c7d2a973361c63941f8f11a1.r5.cf6.rackcdn.com/bg-email2.jpg) top center no-repeat #000000; text-align: justify;\">
		<h1 style=\"text-align: center; margin: 0 0 40px 0; padding: 0 0 10px 0; display: block; border-bottom: 1px dotted #383838;\"><img src=\"../common/images/logo-uefa168.png\" /></h1>
		<p style=\"margin-bottom: 20px; font-size: 18px; padding: 0 40px;\">
			<strong>Password.</strong>
		</p>
		<p style=\"margin-bottom: 20px; line-height: 20px; padding: 0 40px;\">

        <br/>
        <br/>
        account ".$MemberID." password : ".$password."
        <br/>
        <br/>
        UEFA168.com
		</p>
	</div>
	<div style=\"text-align: center; margin: 5px 0 0 0; padding: 8px; display: block; border-top: 1px dotted #383838; font-size: 14px;\">
		<a href=\"http://www.UEFA168.com\" style=\"text-decoration: none;color: #ffd052;\">UEFA168.com</a>
	</div>
</div>
</body>
</html>
";


$mail->AddAddress($MemberEmail);
$mail->Subject = 'forgot password from www.UEFA168.com';
$mail->MsgHTML($message);

//$mail->PreSend();
//echo $mail->GetSentMIMEMessage();
if($mail->Send()){
    echo json_encode(array("result"=>1,"message"=>"Email sent. Please check your email account.",));
}else{
    echo json_encode(array("result"=>0,"message"=>"Failed to send email.Please try again",));
}