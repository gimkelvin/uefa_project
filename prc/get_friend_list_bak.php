<?include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php"; ?>
<?
$page=$_GET['page'];
$pages="";
$param = array("MemberID"=>$_SESSION['MemberID'],"MemberToken"=>$_SESSION['MemberToken']);
$rst=ReqeustAPI::call("GetMemberInfo",$param, null);

//var_dump($rst);
if ($rst[0] == 200) {
    if($rst[1]->ErrorCode != 0){
        $result = 0;
        $message = ReqeustAPI::errorCode($rst[1]->ErrorCode);
    }else{
//        var_dump($rst[2]->RefererList);
        $total = count($rst[2]->RefererList);
        $list = $rst[2]->RefererList;
        $content = array();
        $pages = ceil($total / 10);
        $pages_lists = array();
        $start = 10 * ($page - 1);
        $end = min($total, 10 * $page);

        usort($list, function ($a, $b) {
            $ad = new DateTime($a->RegisterDate);
            $bd = new DateTime($b->RegisterDate);

            if ($ad == $bd) {
                return 0;
            }

            return $ad < $bd ? 1 : -1;

        });

        for($i=0;$i<$total;$i++){
            $content[$i] = new stdClass();
            $content[$i] = $list[$i];
        }


        for($i=$start;$i<$end;$i++){
            $pages_list[$i] = new stdClass();
            $pages_list[$i]->num = ($total - $i);
            $pages_list[$i]->RefererID=$content[$i]->RefererID;
            $pages_list[$i]->RegisterDate=$content[$i]->RegisterDate;
            $pages_lists[]=$pages_list[$i];
        }
    }
}else{
    $result = 0;
    $message = ReqeustAPI::errorCode($rst[0]);
}

echo json_encode(array("page"=>$page,"pages"=>$pages,"list"=>$pages_lists));

