<? include $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php"; ?>
<?
//if(!isset($_SESSION["XPRO"])){
    $param = array("MemberID"=>$_SESSION['MemberID'],"MemberToken"=>$_SESSION["MemberToken"],"GameCode"=>1004,"MemberIP"=>$_SERVER['REMOTE_ADDR'],"GameProvider"=>"LiveGame","GameID"=>1,"Language"=>"en");
    $rst=ReqeustAPI::call("XproPlayGame",$param, null);
    if ($rst[0] == 200) {
        $_SESSION["XPRO"] = $rst[2]->gamesList->game;
    }else{
        unset($_SESSION["XPRO"]);
    }
//}

$currency="";



if(isset($_SESSION["XPRO"])) {
    $games = $_SESSION["XPRO"];
//    var_dump($games);
    $game["xpro"] = array(
        0 => "Baccarat",
        1 => "Roulette",
        2 => "Blackjack",
        3 => "SicBo",
        4 => "Tiger",
        5 => "Holdem",
        6 => "Poker",
    );


    foreach ($games as $key => $row) {
        $volume[$key] = $row->gameName;
    }


    $array_lowercase = array_map('strtolower', $volume);

    array_multisort($array_lowercase, SORT_ASC, SORT_STRING, $games);

    foreach ($games as $game) {
        if (strpos($game->gameName, "Baccarat")) {
            $type = "Baccarat";
        } else if (strpos($game->gameName, "Roulette")) {
            $type = "Roulette";
        } else if (strpos($game->gameName, "Blackjack")) {
            $type = "Blackjack";
        } else if (strpos($game->gameName, "Sic Bo")) {
            $type = "SicBo";
        } else if (strpos($game->gameName, "Tiger")) {
            $type = "Tiger";
        } else if (strpos($game->gameName, "Holdem")) {
            $type = "Holdem";
        } else if (strpos($game->gameName, "Poker")) {
            $type = "Poker";
        }


        if (sizeof($game->limitSetList->limitSet) > 1) {
            for ($i = 0; $i < sizeof($game->limitSetList->limitSet); $i++) {
                $row = new stdClass();
                $row->gameID = (string)$game->gameID;
                $row->gameType = (string)$game->gameType;
                $row->limitSetID = (string)$game->limitSetList->limitSet[$i]->limitSetID;
                $row->minBet = (string)$currency.number_format($game->limitSetList->limitSet[$i]->minBet,0);
                $row->maxBet = (string)$currency.number_format($game->limitSetList->limitSet[$i]->maxBet,0);
                $row->gameName = (string)$game->gameName;
                $row->dealerName = (string)$game->dealerName;
                $row->dealerImageUrl = (string)$game->dealerImageUrl;
                $row->connectionUrl = (string)$game->connectionUrl;
                $row->winParams = (string)$game->winParams;
                $row->isOpen = (string)$game->isOpen;
                $row->openHour = (string)$game->openHour;
                $row->closeHour = (string)$game->closeHour;

                $json[$type][] = $row;
            }
        } else {

            $row = new stdClass();
            $row->gameID = (string)$game->gameID;
            $row->gameType = (string)$game->gameType;
            $row->limitSetID = (string)$game->limitSetList->limitSet->limitSetID;
            $row->minBet = (string)$currency.number_format($game->limitSetList->limitSet->minBet,0);
            $row->maxBet = (string)$currency.number_format($game->limitSetList->limitSet->maxBet,0);
            $row->gameName = (string)$game->gameName;
            $row->dealerName = (string)$game->dealerName;
            $row->dealerImageUrl = (string)$game->dealerImageUrl;
            $row->connectionUrl = (string)$game->connectionUrl;
            $row->winParams = (string)$game->winParams;
            $row->isOpen = (string)$game->isOpen;
            $row->openHour = (string)$game->openHour;
            $row->closeHour = (string)$game->closeHour;

            $json[$type][] = $row;
        }
    }

//var_dump($json);


    echo json_encode($json);
}else{
    echo "error";
}