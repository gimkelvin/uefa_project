<?
include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php";

$MemberID = $_POST["MemberID"];
$MemberPwd = $_POST["MemberPwd"];

if ($MemberID == "" || $MemberPwd == "") {//)
    $result = 0;

    if($_GET['lang']=='english'){
        $message = "Please enter ID and Password.";
    }else{
        $message = "กรุณากรอก ID และรหัสผ่าน.";
    }


    echo json_encode(array("result"=>$result,"message"=>$message));
    exit;
}

$param = array(
    "MemberID"=>$MemberID,
    "MemberPwd"=>hash('sha256',$_POST["MemberPwd"]),
    "MemberIP"=>$_SERVER['REMOTE_ADDR'],
    'VisiterURL' => $_SERVER['HTTP_HOST']
);

$rst=ReqeustAPI::call("Login",$param, null);

if ($rst[0] == 200) {
    if($rst[1]->ErrorCode != 0){
        $result = 0;
        $message = ReqeustAPI::errorCode($rst[1]->ErrorCode);
    }else{
        session_unset();

        $result = 1;

        if($_GET['lang']=='english'){
            $message = "Login complete";
        }else{
            $message = "เข้าสู่ระบบที่สมบูรณ์";
        }


       session_regenerate_id(true);
        $_SESSION['MemberID'] = $rst[2]->MemberID;
        $_SESSION['MemberToken'] = $rst[2]->MemberToken;

        if(isset($rst[2]->LastLoginDate)){
            $_SESSION['LastLoginDate'] = substr($rst[2]->LastLoginDate,0,16);
        }else{

            if($_GET['lang']=='english'){
                $_SESSION['LastLoginDate'] = "First Login";
            }else{
                $_SESSION['LastLoginDate'] = "เข้าสู่ระบบครั้งแรก";
            }


        }
    }
}else{
    $result = 0;
    $message = ReqeustAPI::errorCode($rst[0]);
}

echo json_encode(array("result"=>$result,"message"=>$message));

