<?include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php"; ?>
<?
$page = $_GET['page'];
if ($page == "") $page = 1;
$param = array(
    "MemberID"=>$_SESSION["MemberID"],
    "MemberToken"=>$_SESSION["MemberToken"]
);

$rst=ReqeustAPI::call("MemberTransaction",$param, null);
//var_dump($rst);
if ($rst[0] == 200) {
    if($rst[1]->ErrorCode != 0){
        $result = 0;
        $message = ReqeustAPI::errorCode($rst[1]->ErrorCode);
    }else{
//        var_dump($rst[2]);
        $total = $rst[2]->TotalRecord;
        $list = $rst[2]->Record;
        $content = array();
        $pages = ceil($total / 10);
        $pages_lists = array();
        $start = 10 * ($page - 1);
        $end = min($total, 10 * $page);


        for($i=0;$i<$total;$i++){
            $content[$i] = new stdClass();
            $content[$i] = $list[$i];
        }


        for($i=$start;$i<$end;$i++){
            $pages_list[$i] = new stdClass();
            $pages_list[$i]->num = ($total - $i);

            $pages_list[$i]->TransactionType=$variables['type'][$content[$i]->TransactionType];
            if($content[$i]->TransactionType == "Transfer"){
                $pages_list[$i]->Wallet=$variables['gameText'][$content[$i]->FromWallet]." => ".$variables['gameText'][$content[$i]->Wallet];
            }else{
                $pages_list[$i]->Wallet=$variables['gameText'][$content[$i]->Wallet];
            }
            if(isset($content[$i]->ServiceDescription)){
                $pages_list[$i]->ServiceDescription=$variables['type'][$content[$i]->ServiceDescription];
            }else{
                $pages_list[$i]->ServiceDescription="Normal";
            }

            $pages_list[$i]->Amount=number_format($content[$i]->Amount);
            $pages_list[$i]->ServiceAmount=number_format($content[$i]->ServiceAmount);
            $pages_list[$i]->Status=$variables['status'][$content[$i]->Status];
            $pages_list[$i]->Date=substr($content[$i]->Date,0,16);
            $pages_lists[]=$pages_list[$i];
        }
    }
}
echo json_encode(array("page"=>$page,"pages"=>$pages,"list"=>$pages_lists));
