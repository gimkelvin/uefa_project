<?php
if(isset($_SESSION['MemberToken'])){
    $param = array('MemberID' => $_SESSION["MemberID"],"MemberToken"=>$_SESSION['MemberToken'],"URL"=>$_SERVER["HTTP_HOST"],"MemberIP"=>$_SERVER["REMOTE_ADDR"]);
    $rst = ReqeustAPI::call('SessionExtension', $param, null);

    if($rst[0]==200){
        if($rst[1]->ErrorCode != 0){
            session_unset($_SESSION['MemberID']);
            session_unset($_SESSION['MemberToken']);
            session_unset($_SESSION['LastLoginDate']);
            setcookie(
                "PHPSESSID",
                session_id(),
                time() - 3600/*ini_get('session.cookie_lifetime'),*/,
                ini_get('session.cookie_path'),
                ini_get('session.cookie_domain'),
                ini_get('session.cookie_secure'),
                ini_get('session.cookie_httponly')
            );
            session_regenerate_id(true);
        }else{
            setcookie(
                "PHPSESSID",
                session_id(),
                time() + 3600/*ini_get('session.cookie_lifetime'),*/,
                ini_get('session.cookie_path'),
                ini_get('session.cookie_domain'),
                ini_get('session.cookie_secure'),
                ini_get('session.cookie_httponly')
            );
        }
    }else{
        session_destroy();
    }
}
