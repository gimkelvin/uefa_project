<?include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php";

$MemberID = trim($_POST["MemberID"]);
$MemberName = trim($_POST["MemberName"]);
$MemberPwd = trim($_POST["MemberPwd"]);
$MemberValidPwd = trim($_POST["MemberValidPwd"]);
$MemberEmail = trim($_POST["MemberEmail"]);
$MemberPhone = str_replace("-","",trim($_POST["MemberPhone"]));
$MessengerName = trim($_POST["MessengerName"]);
$MemberAge = trim($_POST["MemberAge"]);
$MemberCountry = trim($_POST["MemberCountry"]);


if (!regExp('alphanumeric',"$MemberID",4,16 )) {

    if($_GET['lang']=='english'){
        echo json_encode(array("result"=>0,"message"=>"Alphanumeric between 4~16 characters.","message_id"=>"msg_id"));
    }else{
        echo json_encode(array("result"=>0,"message"=>"ตัวเลขและตัวอักษรระหว่างวันที่ 4 ~ 16 ตัวอักษร.","message_id"=>"msg_id"));
    }
    exit;
}

if (!regExp('alphanumeric',$MemberPwd,6,16 )) {

    if($_GET['lang']=='english'){
        echo json_encode(array("result"=>0,"message"=>"Please enter password between 6~16 characters.","message_id"=>"msg_pw"));
    }else{
        echo json_encode(array("result"=>0,"message"=>"กรุณาใส่รหัสผ่านระหว่างวันที่ 6 ~ 16 ตัวอักษร.","message_id"=>"msg_id"));
    }
    exit;
}

if ($MemberPwd != $MemberValidPwd) {
    if($_GET['lang']=='english'){
        echo json_encode(array("result"=>0,"message"=>"Please confirm your password.","message_id"=>"msg_pwc"));
    }else{
        echo json_encode(array("result"=>0,"message"=>"โปรดยืนยันรหัสผ่านของคุณ.","message_id"=>"msg_pwc"));
    }

    exit;
}


if (!regExp("kor_alpha_num", $_POST["MemberName"], 4, 20)) {
    if($_GET['lang']=='english'){
        echo json_encode(array("result"=>0,"message"=>"Please enter between 4-20 characters.","message_id"=>"msg_name"));
    }else{
        echo json_encode(array("result"=>0,"message"=>"กรุณากรอกตัวอักษรระหว่าง 4-20.","message_id"=>"msg_pwc"));
    }

    exit;
}

if (!filter_var($MemberEmail, FILTER_VALIDATE_EMAIL)) {
    if($_GET['lang']=='english'){
        echo json_encode(array("result"=>0,"message"=>"Please confirm email.","message_id"=>"msg_email"));
    }else{
        echo json_encode(array("result"=>0,"message"=>"โปรดยืนยันอีเมล.","message_id"=>"msg_pwc"));
    }

    exit;
}

if ($MemberCountry == "") {
    if($_GET['lang']=='english'){
        echo json_encode(array("result"=>0,"message"=>"Please select country.","message_id"=>"msg_email"));
    }else{
        echo json_encode(array("result"=>0,"message"=>"โปรดเลือกประเทศ.","message_id"=>"msg_pwc"));
    }

    exit;
}

$param = array(
    "MemberID"=>$MemberID,
    "MemberName"=>$MemberName,
    "MemberPwd"=> hash('sha256',$MemberPwd),
    "RegisterIP"=>$_SERVER['REMOTE_ADDR'],
    'RegisterURL' => $_SERVER['HTTP_HOST'],
    "MemberPhone"=>$MemberPhone,
    "MemberEmail"=>$MemberEmail,
    "Age"=>$MemberAge,
    "MessengerName"=>$MessengerName,
    "Country"=>$MemberCountry,
    "Currency"=>"USD",
    "Language"=>"en-us"
);

$rst=ReqeustAPI::call("Signup",$param, null);

if ($rst[0] == 200) {
    if($rst[1]->ErrorCode != 0){
        $result = 0;
        echo json_encode(array("result"=>0,"message"=>$rst[1]->ErrorMsg,"message_id"=>"alert"));
        exit;
    }else{
        $param = array(
            "MemberID"=>$MemberID,
            "MemberPwd"=> hash('sha256',$MemberPwd),
            "MemberIP"=>$_SERVER['REMOTE_ADDR'],
            'VisiterURL' => $_SERVER['HTTP_HOST']
        );

        $rst=ReqeustAPI::call("Login",$param, null);
        if ($rst[0] == 200) {
            if($rst[1]->ErrorCode != 0){
                $result = 0;
                $message = $rst[1]->ErrorMsg.ReqeustAPI::errorCode($rst[1]->ErrorCode);
            }else{
                session_unset();
                $result = 1;
                if($_GET['lang']=='english'){
                    $message = "Sign Up Complete.";
                }else{
                    $message = "ลงทะเบียนเสร็จสมบูรณ์.";
                }

                session_regenerate_id(true);
                $_SESSION['MemberID'] = $rst[2]->MemberID;
                $_SESSION['MemberToken'] = $rst[2]->MemberToken;

                if(isset($rst[2]->LastLoginDate)){
                    $_SESSION['LastLoginDate'] = substr($rst[2]->LastLoginDate,0,16);
                }else{
                    $_SESSION['LastLoginDate'] = "First Login";
                }
            }
        }
    }
} else{
    var_dump($param);
    $result = 0;
    $message = ReqeustAPI::errorCode($rst[0]);
    echo json_encode(array("result"=>0,"message"=>$rst[1]->ErrorMsg,"message_id"=>"alert"));
}

echo json_encode(array("result"=>$result,"message"=>$message));
