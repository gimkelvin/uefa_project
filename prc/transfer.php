<?include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php"; ?>
<?

$Amount =  trim($_POST["Amount"]);
$FromWallet= trim($_POST["FromWallet"]);
$ToWallet= trim($_POST["ToWallet"]);

//3. 데이터 CHECK
if($FromWallet == ""){
    if($_GET['lang'] == 'english') {
        echo json_encode(array("result" => 0, "message" => "Please select game.", "message_id" => "msg_game"));
    }else{
        echo json_encode(array("result" => 0, "message" => "กรุณาเลือกเกม.", "message_id" => "msg_game"));
    }
    exit;
}


if ($Amount == "") {
    if($_GET['lang'] == 'english') {
        echo json_encode(array("result" => 0, "message" => "Please enter amount.", "message_id" => "msg_amount"));
    }else{
        echo json_encode(array("result" => 0, "message" => "กรุณาใส่จำนวน.", "message_id" => "msg_amount"));
    }
        exit;
}

$Amount = str_replace(",", "", $Amount);

if (!regExp("integer", $Amount)) {
    if($_GET['lang'] == 'english') {
        echo json_encode(array("result" => 0, "message" => "Please enter numeric value for the amount.", "message_id" => "msg_amount"));
    }else{
        echo json_encode(array("result" => 0, "message" => "กรุณากรอกค่าตัวเลขจำนวนเงิน.", "message_id" => "msg_amount"));
    }
        exit;
}

if ($Amount <= 0) {
    if($_GET['lang'] == 'english') {
        echo json_encode(array("result" => 0, "message"=>"Please enter a valid amount.","message_id"=>"msg_amount"));
    }else{
        echo json_encode(array("result" => 0, "message" => "กรุณากรอกจำนวนเงินที่ถูกต้อง.", "message_id" => "msg_amount"));
    }
        exit;
}

if($ToWallet == ""){
    if($_GET['lang'] == 'english') {
        echo json_encode(array("result" => 0, "message" => "Please select game.","message_id"=>"msg_game2"));
    }else{
        echo json_encode(array("result" => 0, "message" => "กรุณาเลือกเกม.", "message_id" => "msg_game2"));
    }
        exit;
}

if($FromWallet == $ToWallet){
    if($_GET['lang'] == 'english') {
        echo json_encode(array("result" => 0, "message" => "Please select a different game.","message_id"=>"msg_game2"));
    }else{
        echo json_encode(array("result" => 0, "message" => "กรุณาเลือกเกมที่แตกต่าง.", "message_id" => "msg_game2"));
    }
        exit;
}



$param = array("MemberID"=>$_SESSION['MemberID'],"MemberToken"=>$_SESSION['MemberToken'],"Wallet"=>$FromWallet);
$rst=ReqeustAPI::call("CheckMemberBalance2",$param, null);

if ($rst[0] == 200) {
//    var_dump($rst);
    if($rst[1]->ErrorCode != 0 && $rst[1]->ErrorCode != 999){
        if($_GET['lang'] == 'english') {
            echo json_encode(array("result"=>0,"message"=>"Fund transfer failed.","message_id"=>"msg_amount"));
        }else{
            echo json_encode(array("result"=>0,"message"=>"โอนเงินล้มเหลว.","message_id"=>"msg_amount"));
        }
        exit;
    }else{
        if($rst[2]->Record[0]->Balance < $Amount){
            if($_GET['lang'] == 'english') {
                echo json_encode(array("result"=>0,"message"=>"Insufficient balance.","message_id"=>"msg_amount"));
            }else{
                echo json_encode(array("result"=>0,"message"=>"ยอดเงินไม่เพียงพอ.","message_id"=>"msg_amount"));
            }
            exit;
        }
    }
}else{
    $result = 0;
    $message = ReqeustAPI::errorCode($rst[0]);

}

$param = array(
    "MemberID"=>$_SESSION["MemberID"],
    "MemberToken"=>$_SESSION["MemberToken"],
    "Amount"=>$Amount,
    "FromWallet"=>$FromWallet,
    "ToWallet"=>$ToWallet,
    "MemberIP"=>$_SERVER['REMOTE_ADDR']
);

//var_dump($param);

$rst=ReqeustAPI::call("TransferBalance",$param, null);

if ($rst[0] == 200) {
//    var_dump($rst);
    if($rst[1]->ErrorCode != 0){
        $result = 0;
        $message = ReqeustAPI::errorCode($rst[1]->ErrorCode);
    }else{
        $result = 1;
        if($_GET['lang'] == 'english') {
            $message = "Fund transfer has been completed.";
        }else{
            $message = "โอนเงินเสร็จเรียบร้อยแล้ว.";
        }
    }
} else{
    $result = 0;
    $message = ReqeustAPI::errorCode($rst[0]);
}

echo json_encode(array("result"=>$result,"message"=>$message,"message_id"=>"alert"));

