<?include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php"; ?>
<?

//if (!preg_match("/http:\/\/(.*?)\/pages\/deposit/", $_SERVER['HTTP_REFERER'], $match)) {
//    exit;
//}
//$domain = $match[1];

//2. 데이터 GET

$CompAmount =  trim($_POST["CompAmount"]);
$GameCode= trim($_POST["GameCode"]);

//3. 데이터 CHECK
if($GameCode == ""){
    echo json_encode(array("result"=>0,"message"=>"Please select game.","message_id"=>"msg_game"));
    exit;
}

if ($CompAmount == "") {
    echo json_encode(array("result"=>0,"message"=>"Please enter amount.","message_id"=>"msg_amount"));
    exit;
}

$CompAmount = str_replace(",", "", $CompAmount);

if (!regExp("integer", $CompAmount)) {
    echo json_encode(array("result"=>0,"message"=>"Please enter numeric value for the amount.","message_id"=>"msg_amount"));
    exit;
}

if ($CompAmount <= 0) {
    echo json_encode(array("result"=>0,"message"=>"Please enter a valid amount.","message_id"=>"msg_amount"));
    exit;
}


$param = array(
    "MemberID"=>$_SESSION["MemberID"],
    "MemberToken"=>$_SESSION["MemberToken"],
    "CompAmount"=>$CompAmount,
    "GameCode"=>$GameCode,
    "MemberIP"=>$_SERVER['REMOTE_ADDR'],
);



$rst=ReqeustAPI::call("useComp",$param, null);

if ($rst[0] == 200) {
    if($rst[1]->ErrorCode != 0){
        $result = 0;
        $message = ReqeustAPI::errorCode($rst[1]->ErrorCode);
    }else{
        $result = 1;
        $message = "Comp points have been processed successfully.";
    }
} else{
    $result = 0;
    $message = ReqeustAPI::errorCode($rst[0]);
}

echo json_encode(array("result"=>$result,"message"=>$message,"message_id"=>"alert"));

