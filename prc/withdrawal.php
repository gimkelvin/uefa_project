<?include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php";

$Wallet= trim($_POST["Wallet"]);
$Amount =  trim($_POST["Amount"]);
$BankCode =  trim($_POST["BankCode"]);
$AccountNumber =  str_replace("-","",trim($_POST["AccountNumber"]));
$BankAccount =  trim($_POST["BankAccount"]);
$MemberPhone = str_replace("-","",trim($_POST["MemberPhone"]));
$UserMemo =  htmlspecialchars(trim($_POST["UserMemo"]));

if($Wallet == ""){
    echo json_encode(array("result"=>0,"message"=>"Please select game.","message_id"=>"msg_game"));
    exit;
}


if ($Amount == "") {
    echo json_encode(array("result"=>0,"message"=>"Please enter the amount.","message_id"=>"msg_amount"));
    exit;
}

$Amount = str_replace(",", "", $Amount);

if (!regExp("integer", $Amount)) {
    echo json_encode(array("result"=>0,"message"=>"Please enter numeric value for the amount.","message_id"=>"msg_amount"));
    exit;
}

if ($Amount <= 0) {
    echo json_encode(array("result"=>0,"message"=>"Please enter a valid amount.","message_id"=>"msg_amount"));
    exit;
}

if($BankCode == ""){
    echo json_encode(array("result"=>0,"message"=>"Please select a bank.","message_id"=>"msg_code"));
    exit;
}

if (!regExp("kor_alpha_num", $_POST["BankAccount"], 4, 20)) {
    echo json_encode(array("result"=>0,"message"=>"Please enter the name of depositor within 3 to 21 characters.","message_id"=>"msg_name"));
    exit;
}

if (!regExp("integer", $AccountNumber)) {
    echo json_encode(array("result"=>0,"message"=>"Please enter numeric values only.","message_id"=>"msg_bank_account"));
    exit;
}

if ($MemberPhone == "") {
    echo json_encode(array("result"=>0,"message"=>"Please enter a valid mobile number.","message_id"=>"msg_phone"));
    exit;
}

if (!regExp("all", $UserMemo, 0, 600)) {
    echo json_encode(array("result"=>0,"message"=>"Text is limited to 3000 characters.","message_id"=>"msg_comment"));
    exit;
}

$param = array("MemberID"=>$_SESSION['MemberID'],"MemberToken"=>$_SESSION['MemberToken'],"Wallet"=>$Wallet);
$rst=ReqeustAPI::call("CheckMemberBalance2",$param, null);

if ($rst[0] == 200) {
    if($rst[1]->ErrorCode != 0 && $rst[1]->ErrorCode != 999){
        echo json_encode(array("result"=>0,"message"=>"Failed: Withdrawal request.","message_id"=>"msg_amount"));
        exit;
    }else{
        if($rst[2]->Record[0]->Balance < $Amount){
            echo json_encode(array("result"=>0,"message"=>"Insufficient balance.","message_id"=>"msg_amount"));
            exit;
        }
    }
}else{
    $result = 0;
    $message = ReqeustAPI::errorCode($rst[0]);
}

$param = array(
    "MemberID"=>$_SESSION["MemberID"],
    "MemberToken"=>$_SESSION["MemberToken"],
    "BankAccount"=>$BankAccount,
    "BankCode"=>$BankCode,
    "AccountNumber"=>$AccountNumber,
    "MemberPhone"=>$MemberPhone,
    "Amount"=>$Amount,
    "Wallet"=>$Wallet,
    "MemberIP"=>$_SERVER['REMOTE_ADDR'],
    "UserMemo"=>$UserMemo
);

$rst=ReqeustAPI::call("Withdrawal",$param, null);

if ($rst[0] == 200) {
    if($rst[1]->ErrorCode != 0){
        $result = 0;
        $message = ReqeustAPI::errorCode($rst[1]->ErrorCode);
    }else{
        $result = 1;
        $message = "Withdrawal request has been submitted.";
    }
} else{
    $result = 0;
    $message = ReqeustAPI::errorCode($rst[0]);
}

echo json_encode(array("result"=>$result,"message"=>$message));

