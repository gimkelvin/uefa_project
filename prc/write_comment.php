<?include_once $_SERVER["DOCUMENT_ROOT"] . "/lib/client.php"; ?>
<?

$type = $_GET["type"];
$type = $_GET["type"];

if($type == 4){
    $BoardID="customer";
}else if($type == 5){
    $BoardID="affiliate";
}


$Contents =  htmlspecialchars(trim($_POST["Contents"]));
$BoardCode = trim($_GET["code"]);
//3. 데이터 CHECK

if($Contents == ""){
    echo json_encode(array("result"=>0,"message"=>"Please enter the message.","message_id"=>"alert"));
    exit;
}

if (!regExp("all", $Contents, 0, 2000)) {
    echo json_encode(array("result"=>0,"message"=>"Text is limited to 1000 characters.","message_id"=>"alert"));
    exit;
}

$param = array(
    "MemberID"=>$_SESSION["MemberID"],
    "MemberToken"=>$_SESSION["MemberToken"],
    "BoardID"=>$BoardID,
    "BoardCode"=>$BoardCode,
    "Contents"=>$Contents
);


$rst=ReqeustAPI::call("WriteComment",$param, null);

if ($rst[0] == 200) {
    if($rst[1]->ErrorCode != 0){
        $result = 0;
        $message = ReqeustAPI::errorCode($rst[1]->ErrorCode);
    }else{
        $result = 1;
        $message = "Reply has been submitted.";
    }
} else{
    $result = 0;
    $message = ReqeustAPI::errorCode($rst[0]);
}

echo json_encode(array("result"=>$result,"message"=>$message,"message_id"=>"alert"));